/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <wordexp.h>

#include "src/core/cfgdata.h"

#include "executor.h"

Executor::Executor(const QByteArray& cmdLine, const CfgShell& shell) :
    m_parseError(false)
{
    // Parse either the command line directly, or the shell execution command.
    const bool hasShell = !shell.exec.isEmpty();
    const QByteArray& command = hasShell ? shell.exec : cmdLine;

    wordexp_t params;

    if (wordexp(command, &params, WRDE_NOCMD) != 0) {
        m_parseError = true;
    } else {
        for (unsigned arg = 0; arg < params.we_wordc; ++arg) {
            if (hasShell) {
                QByteArray argStr = params.we_wordv[arg];
                argStr.replace("%s", cmdLine);
                m_argv.push_back(QByteArray());
                m_argv.back().swap(argStr);
            } else {
                m_argv.push_back(params.we_wordv[arg]);
            }
        }

        m_progname = params.we_wordv[0];

        wordfree(&params);
    }
}
