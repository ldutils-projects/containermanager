/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "detailpane.h"
#include "ui_detailpane.h"
#include "src/ui/windows/mainwindow.h"
#include <src/util/roles.h>
#include "src/util/util.h"
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "src/core/containercontrol.h"

DetailPane::DetailPane(MainWindow& mainWindow, QWidget *parent) :
    Pane(mainWindow, PaneClass::Detail, parent),
    ui(new Ui::DetailsPane),
    uSecDelegate(this),
    sizeDelegate(20, 42, this),
    cpuSharesDelegate(this),
    detailsHeader(Qt::Horizontal, this),
    subtreeFilter(this),
    flattenFilter(this, [](const QAbstractItemModel* model, const QModelIndex& idx) {
        return model->rowCount(idx) == 0;
    }),
    detailsFilter(this),
    resizeTimer(this),
    initialExpandTimer(this)
{
    ui->setupUi(this);

    setPaneFilterBar(ui->filterCtrl);
    setupDetailsView();
    setupContextMenus();
    setupSignals();
    setupDelegates();
    setupTimers();
    Util::SetupWhatsThis(this);

    viewAsTree(true);
}

DetailPane::~DetailPane()
{
    deleteUI(ui);
}

void DetailPane::setupSignals()
{
    // Notify ourselves and the filter about active container changes
    connect(&mainWindow(), &MainWindow::paneCurrentContainerChanged, this, &DetailPane::currentContainerChanged);

    // Filter text changes.
    connect(ui->filterDetails, &QLineEdit::textChanged, this, &DetailPane::filterTextChanged);
}

void DetailPane::setupDelegates()
{
    QTreeView& view = *ui->viewDetails;

    // Alas, this can't be in the constructors, since it requires ui->setupUi above.
    uSecDelegate.setSelector(selectionModel());
    sizeDelegate.setSelector(selectionModel());
    cpuSharesDelegate.setSelector(selectionModel());

    // QTreeView doesn't allow specification of delegates at nesting levels: so this will
    // set the delegate for all Nth subnodes of every node.  Most are not editable however,
    // so we're getting away with it.
    view.setItemDelegateForRow(detailsFilter.dataRow(ContainerModel::CfsQuota),    &uSecDelegate);
    view.setItemDelegateForRow(detailsFilter.dataRow(ContainerModel::CfsPeriod),   &uSecDelegate);
    view.setItemDelegateForRow(detailsFilter.dataRow(ContainerModel::CpuShares),   &cpuSharesDelegate);
    view.setItemDelegateForRow(detailsFilter.dataRow(ContainerModel::MemoryLimit), &sizeDelegate);
}

void DetailPane::setupTimers()
{
    // Wait a little bit after the last active container change, and then resize columns.
    resizeTimer.setSingleShot(true);
    connect(&resizeTimer, &QTimer::timeout, this, &DetailPane::resizeDeferredHook);

    // We can't expand the tree in the constructor.  This is a kludge to fake it.
    initialExpandTimer.setSingleShot(true);
    connect(&initialExpandTimer, &QTimer::timeout, this, &DetailPane::reexpandTree);
    initialExpandTimer.start(0);  // expand the tree after a little bit of idle time
}

void DetailPane::setupContextMenus()
{
    // View context menus
    {
        QTreeView& view = *ui->viewDetails;

        mainWindow().setupContainerActionContextMenu(paneMenu);
        // container action context menu
        view.setContextMenuPolicy(Qt::CustomContextMenu);
        connect(&view, &QTreeView::customContextMenuRequested, this, &DetailPane::showActionContextMenu);
    }

    // Pane header menus
    setupActionContextMenu(paneMenu);
}

void DetailPane::filterTextChanged(const QString& regex)
{
    subtreeFilter.setQueryString(regex);

    if (regex.size() == 0)  // if filter is cleared, re-show everything
        showAll();
}

QModelIndexList DetailPane::getSelections() const
{
    if (!hasSelection())
        return { };

    return { Util::MapDown(detailsFilter.getSelection()) };
}

bool DetailPane::hasSelection() const
{
    return detailsFilter.hasSelection();
}

bool DetailPane::isSelected(const QModelIndex &idx) const
{
    return hasSelection() && detailsFilter.getSelection() == idx;
}

void DetailPane::showActionContextMenu(const QPoint& pos)
{
    paneMenu.exec(ui->viewDetails->mapToGlobal(pos));
}

void DetailPane::setupDetailsView()
{
    QTreeView& view = *ui->viewDetails;

    detailsFilter.setSourceModel(&containerModel);
    flattenFilter.setSourceModel(&detailsFilter);
    subtreeFilter.setSourceModel(&detailsFilter);  // start out with tree view

    view.setModel(&subtreeFilter);
    view.setHeader(&detailsHeader);
    view.setAlternatingRowColors(true);
    view.setSelectionMode(QAbstractItemView::ExtendedSelection);
    view.setSelectionBehavior(QAbstractItemView::SelectRows);
    view.setSortingEnabled(false);
    view.setUniformRowHeights(true);

    subtreeFilter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());
    subtreeFilter.setDynamicSortFilter(false);

    detailsHeader.setSectionResizeMode(QHeaderView::Interactive);
    detailsHeader.setDefaultAlignment(Qt::AlignLeft);

    setFocusProxy(ui->viewDetails);
}

void DetailPane::showAll()
{
    ui->filterDetails->clear();
    expandAll();
}

void DetailPane::expandAll()
{
    ui->viewDetails->expandAll();
}

void DetailPane::collapseAll()
{
    ui->viewDetails->collapseAll();
}

QItemSelectionModel* DetailPane::selectionModel() const
{
    return ui->viewDetails->selectionModel();
}

void DetailPane::copySelected() const
{
    subtreeFilter.copyToClipboard(*ui->viewDetails,
                                  subtreeFilter.sortedSelection(selectionModel()),
                                  cfgData().rowSeparator,
                                  cfgData().colSeparator,
                                  Util::CopyRole);
}

void DetailPane::viewAsTree(bool asTree)
{
    if (asTree)
        subtreeFilter.setSourceModel(&detailsFilter);
    else
        subtreeFilter.setSourceModel(&flattenFilter);
}


bool DetailPane::viewIsTree() const
{
    return subtreeFilter.sourceModel() != &flattenFilter;
}

void DetailPane::selectAll()
{
    ui->viewDetails->selectAll();
}

void DetailPane::selectNone()
{
    ui->viewDetails->clearSelection();
}

void DetailPane::newConfig()
{
    subtreeFilter.setFilterCaseSensitivity(cfgData().getFilterCaseSensitivity());
}

void DetailPane::resizeToFit(int defer)
{
    if (defer < 0)
        resizeDeferredHook();
    else
        resizeTimer.start(defer);
}

void DetailPane::resizeDeferredHook()
{
    Util::ResizeViewForData(*ui->viewDetails);
}

void DetailPane::currentContainerChanged(const QModelIndex& current)
{
    // If lock button is selected, keep current selection.
    if (ui->lockToCurrentButton->isChecked())
        return;

    detailsFilter.currentItemChanged(current);

    // After a small delay, resize columns (so we don't spam the resize each time)
    resizeToFit(500);  // expand the tree after a little bit of idle time
}


void DetailPane::on_lockToCurrentButton_toggled(bool checked)
{
    ui->lockToCurrentButton->setIcon(checked ? Icons::get("object-locked") :
                                               Icons::get("object-unlocked"));
}

// Annoyingly, we cannot expand tree nodes during construction or loading (the viewer
// widget simply ignores the request), so we defer it here and trigger this by a timer.
// Ungainly.
void DetailPane::reexpandTree()
{
    if (expandState.empty()) {
        expandAll();
        return;
    }

    for (auto& [key, expanded] : expandState)
        if (const QModelIndex headerIdx = detailsFilter.headerIndex(key); headerIdx.isValid())
            ui->viewDetails->setExpanded(Util::MapUp(&subtreeFilter, headerIdx), expanded);

    expandState.clear();
}

void DetailPane::save(QSettings& settings) const
{
    Pane::save(settings);

    SL::Save(settings, "filterDetails", ui->filterDetails->text());

    // Save expansion state for each header node in the detail view.
    settings.beginGroup("Expanded"); {
        Util::Recurse(detailsFilter, [this, &settings](const QModelIndex& idx) {
            if (detailsFilter.hasChildren(idx)) {
                const bool isExpanded = ui->viewDetails->isExpanded(Util::MapUp(&subtreeFilter, idx));
                const QString headerName = detailsFilter.data(idx).toString();

                SL::Save(settings, headerName, isExpanded);
            }
            return true;
        });
    } settings.endGroup();

    // TODO: for locked state, save which container we're locked to, and restore that on load.
}

void DetailPane::load(QSettings& settings)
{
    Pane::load(settings);

    ui->filterDetails->setText(SL::Load<QString>(settings, "filterDetails"));

    // Restore expansion state for each header node in the detail view.  We can't do the
    // expansion here: see comment in reexpandHeaders.
    settings.beginGroup("Expanded"); {
        const QStringList keys = settings.childKeys();

        for (const auto& key : keys) {
            bool expanded = true;
            SL::Load(settings, key, expanded);
            expandState[key] = expanded;
        }
    } settings.endGroup();
}
