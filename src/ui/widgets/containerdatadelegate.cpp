/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QComboBox>

#include <src/util/util.h>
#include "src/core/containermodel.h"
#include "containerdatadelegate.h"

ContainerDataDelegate::ContainerDataDelegate(QObject *parent) : ComboBoxDelegate(parent)
{
    for (ModelType mt = ContainerModel::_First; mt < ContainerModel::_Count; ++mt)
        addItem(ContainerModel::mdName(mt));
}
