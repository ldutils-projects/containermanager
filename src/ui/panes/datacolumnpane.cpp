/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/ui/windows/mainwindow.h"
#include "datacolumnpane.h"

DataColumnPane::DataColumnPane(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent) :
    DataColumnPaneBase(mainWindow, PaneClass_t(int(paneClass)), parent)
{
}

// This is needed to make the MOC hapy
DataColumnPane::~DataColumnPane()
{
}

const MainWindow& DataColumnPane::mainWindow() const
{
    return static_cast<const MainWindow&>(DataColumnPaneBase::mainWindow());
}

MainWindow& DataColumnPane::mainWindow()
{
    return static_cast<MainWindow&>(DataColumnPaneBase::mainWindow());
}
