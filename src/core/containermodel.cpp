/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cctype>
#include <QStringList>

#include "containermodel.h"
#include "containerclass.h"
#include "containerprivate.h"
#include "containerstate.h"
#include "containercontrol.h"
#include "src/ui/windows/appconfig.h"
#include <src/util/roles.h>
#include "src/core/app.h"
#include "src/util/util.h"

ContainerModel::ContainerModel(QObject *parent) :
    TreeModel(new ContainerItem(), parent)
{
    setupHeaders();
}

// This is impractical to do with constructor delegation.
void ContainerModel::setupHeaders()
{
    QStringList header;

    for (ModelType mt = ContainerModel::_First; mt < ContainerModel::_Count; ++mt)
        header << ContainerModel::mdName(mt);

    setHorizontalHeaderLabels(header);
}

inline ContainerItem* ContainerModel::getItem(const QModelIndex &idx) const
{
    return static_cast<ContainerItem*>(TreeModel::getItem(idx));
}

// update container data
void ContainerModel::updateData(bool force)
{
    applyToPrivate([&](ContainerPrivate& privData, const QModelIndex& idx) {
        privData.updateData(verify(idx), force);
        return true;
    });
}

QString ContainerModel::mdName(ModelType mt)
{
    switch (mt) {
    case ContainerModel::Name:         return QObject::tr("Name");
    case ContainerModel::Class:        return QObject::tr("Class");
    case ContainerModel::State:        return QObject::tr("State");
    case ContainerModel::Uptime:       return QObject::tr("Uptime");
    case ContainerModel::CpuUse:       return QObject::tr("CPU Use");
    case ContainerModel::CpuShares:    return QObject::tr("CPU Shares");
    case ContainerModel::CfsQuota:     return QObject::tr("CFS Quota");
    case ContainerModel::CfsPeriod:    return QObject::tr("CFS Period");
    case ContainerModel::Load:         return QObject::tr("Load");
    case ContainerModel::Memory:       return QObject::tr("Memory");
    case ContainerModel::MemoryLimit:  return QObject::tr("MemoryLimit");
    case ContainerModel::MemPct:       return QObject::tr("Mem %");
    case ContainerModel::KMem:         return QObject::tr("KMem");
    case ContainerModel::Unprivileged: return QObject::tr("Unprivileged");
    case ContainerModel::Ephemeral:    return QObject::tr("Ephemeral");
    case ContainerModel::Processes:    return QObject::tr("Processes");
    case ContainerModel::Control:      return QObject::tr("Control");
    case ContainerModel::HLink:        return QObject::tr("Host Link");
    case ContainerModel::CLink:        return QObject::tr("Container Link");
    case ContainerModel::IPV4:         return QObject::tr("IPV4");
    case ContainerModel::IPV6:         return QObject::tr("IPV6");
    case ContainerModel::TxBytes:      return QObject::tr("TX Bytes");
    case ContainerModel::RxBytes:      return QObject::tr("RX Bytes");
    case ContainerModel::TxRxBytes:    return QObject::tr("TX+RX Bytes");
    case ContainerModel::TxSpeed:      return QObject::tr("TX Speed");
    case ContainerModel::RxSpeed:      return QObject::tr("RX Speed");
    case ContainerModel::TxRxSpeed:    return QObject::tr("TX+RX Speed");
    case ContainerModel::HWAddress:    return QObject::tr("HW Address");
#if USEIO
    case ContainerModel::ReadBytes:    return QObject::tr("Read Bytes");
    case ContainerModel::WriteBytes:   return QObject::tr("Write Bytes");
    case ContainerModel::ReadSpeed:    return QObject::tr("Read Speed");
    case ContainerModel::WriteSpeed:   return QObject::tr("Write Speed");
#endif
    case ContainerModel::OS:           return QObject::tr("OS");
    case ContainerModel::Kernel:       return QObject::tr("Kernel");
    case ContainerModel::Machine:      return QObject::tr("Machine");
    case ContainerModel::Distro:       return QObject::tr("Distro");
    case ContainerModel::Release:      return QObject::tr("Release");
    case ContainerModel::Codename:     return QObject::tr("Codename");
    case ContainerModel::ConfigFile:   return QObject::tr("Config File");
    case ContainerModel::_Count:       break;

    }

    assert(0 && "Unknown ContainerData value");
    return "";
}

// Obtain cgroup controller for this data, if any, or nullptr.
const char* ContainerModel::cgroupController(ModelType mt)
{
    switch (mt) {
    case ContainerModel::CpuUse:       [[fallthrough]];
    case ContainerModel::CpuShares:    [[fallthrough]];
    case ContainerModel::CfsQuota:     [[fallthrough]];
    case ContainerModel::CfsPeriod:    return "cpu,cpuacct";

    case ContainerModel::Memory:       [[fallthrough]];
    case ContainerModel::MemoryLimit:  [[fallthrough]];
    case ContainerModel::KMem:         [[fallthrough]];
    case ContainerModel::Processes:    return "memory";

#if USEIO
    case ContainerModel::ReadBytes:    [[fallthrough]];
    case ContainerModel::WriteBytes:   [[fallthrough]];
    case ContainerModel::ReadSpeed:    [[fallthrough]];
    case ContainerModel::WriteSpeed:   return "blkio";
#endif

    default:                          return nullptr;
    }
}

const char* ContainerModel::cgroupFile(ModelType mt)
{
    switch (mt) {
    case ContainerModel::CpuUse:       return "cpuacct.usage";
    case ContainerModel::CpuShares:    return "cpu.shares";
    case ContainerModel::CfsQuota:     return "cpu.cfs_quota_us";
    case ContainerModel::CfsPeriod:    return "cpu.cfs_period_us";
    case ContainerModel::Memory:       return "memory.usage_in_bytes";
    case ContainerModel::MemoryLimit:  return "memory.limit_in_bytes";
    case ContainerModel::KMem:         return "memory.kmem.usage_in_bytes";
#if USEIO
    case ContainerModel::ReadBytes:    [[fallthrough]];
    case ContainerModel::WriteBytes:   [[fallthrough]];
    case ContainerModel::ReadSpeed:    [[fallthrough]];
    case ContainerModel::WriteSpeed:   return "blkio.io_service_bytes";
#endif
    default:                          return nullptr;
    }
}

bool ContainerModel::mdIsEditable(ModelType mt)
{
    switch (mt) {
    case ContainerModel::CpuShares:    [[fallthrough]];
    case ContainerModel::CfsQuota:     [[fallthrough]];
    case ContainerModel::CfsPeriod:    [[fallthrough]];
    case ContainerModel::MemoryLimit:  return true;
    // TODO: allow editing network up/down, and machine state

    default:                          return false;
    }
}

// Return true if this data can be placed in a chart
bool ContainerModel::mdIsChartable(ModelType mt)
{
    switch (mt) {
    case ContainerModel::Uptime:       [[fallthrough]];
    case ContainerModel::CpuUse:       [[fallthrough]];
    case ContainerModel::CpuShares:    [[fallthrough]];
    case ContainerModel::CfsQuota:     [[fallthrough]];
    case ContainerModel::CfsPeriod:    [[fallthrough]];
    case ContainerModel::Load:         [[fallthrough]];
    case ContainerModel::Memory:       [[fallthrough]];
    case ContainerModel::MemoryLimit:  [[fallthrough]];
    case ContainerModel::MemPct:       [[fallthrough]];
    case ContainerModel::KMem:         [[fallthrough]];
    case ContainerModel::Processes:    [[fallthrough]];
    case ContainerModel::TxBytes:      [[fallthrough]];
    case ContainerModel::RxBytes:      [[fallthrough]];
    case ContainerModel::TxRxBytes:    [[fallthrough]];
    case ContainerModel::TxSpeed:      [[fallthrough]];
    case ContainerModel::RxSpeed:      [[fallthrough]];
    case ContainerModel::TxRxSpeed:    return true;
#if USEIO
    case ContainerModel::ReadBytes:    [[fallthrough]];
    case ContainerModel::WriteBytes:   [[fallthrough]];
    case ContainerModel::ReadSpeed:    [[fallthrough]];
    case ContainerModel::WriteSpeed:   return true;
#endif

    default:                          return false;
    }
}

QString ContainerModel::makeTooltip(const QString& description, bool editable, TTInfo info)
{
    QString toolTip = ModelMetaData::makeTooltip(description, editable);

    if (info == TTInfo::CgroupCpu) {
        toolTip += makeTooltipHeader("Cgroup", "cpushares") +
                   tr("This data requires the <tt><i>cpushares</i></tt> "
                      "cgroup controller be available.  On some systems it is not, by default.  See "
                      "<p style=\"text-indent: 20px\"><tt><a href=/etc/pam.d/common-session>"
                      "/etc/pam.d/common-session</a></tt>"
                      "</p>and"
                      "<p style=\"text-indent: 20px\"><tt><a href=/etc/pam.d/common-session-noninteractive>"
                      "/etc/pam.d/common-session-noninteractive</a></tt>");
    }

    if (info == TTInfo::CgroupMem) {
        toolTip += makeTooltipHeader("Cgroup", "memory") +
                   tr("This data requires the <tt><i>memory</i></tt> cgroup controller be available.</p>");
    }

#if USEIO
    if (info == TTInfo::CgroupBlkIo) {
        toolTip += makeTooltipHeader("Cgroup", "blkio") +
                   tr("This data requires the <tt><i>blkio</i></tt> "
                      "cgroup controller be available.  On some systems it is not, by default.  See "
                      "<p style=\"text-indent: 20px\"><tt><a href=/etc/pam.d/common-session>"
                      "/etc/pam.d/common-session</a></tt>"
                      "</p>and"
                      "<p style=\"text-indent: 20px\"><tt><a href=/etc/pam.d/common-session-noninteractive>"
                      "/etc/pam.d/common-session-noninteractive</a></tt>");

    }
#endif // USEIO

    if (info == TTInfo::AsyncGet) {
        toolTip += makeTooltipHeader("Asynchronous") +
                   tr("This property is queried in a background task for most container types "
                      "to avoid blocking the UI, and will <i>latch</i> once set. Use the refresh "
                      "button to update it.");
    }

    return toolTip;
}


// tooltip for container column header
QString ContainerModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case ContainerModel::Name:
        return makeTooltip(tr("Descriptive container name.<i></i>."), editable);
    case ContainerModel::Class:
        return makeTooltip(tr("The type of the container.  E.g, Lxc, or VirtualBox.<i></i>."), editable);
    case ContainerModel::State:
        return makeTooltip(tr("The current container state.<i></i>."), editable);
    case ContainerModel::Uptime:
        return makeTooltip(tr("The time since the container was started.<i></i>."), editable);
    case ContainerModel::CpuUse:
        return makeTooltip(tr("The total CPU time used by the container.<i></i>."), editable);
    case ContainerModel::CpuShares:
        return makeTooltip(tr("This controls the ratio of CPU time allotted to various containers. "
                              "All CPU shares are added together, and each container will recieve "
                              "this ratio of the total.  For example, if one container is given 512 "
                              "CPU shares, and another 1024, then the first gets 512/(512+1024) of "
                              "the total, or 33%, while the second gets 1024/(512+1024) of the total, "
                              "or 66%."), 
                           editable, CgroupCpu);
    case ContainerModel::CfsQuota: [[fallthrough]];
    case ContainerModel::CfsPeriod:
        return makeTooltip(tr("This is an absolute limit on the CPU time usable by the container. "
                              "out of each <i>CFS Period</i> nanoseconds of CPU time, the container can "
                              "use up to <i>CFS Quota</i> nanoseconds.  For example, to limit a container "
                              "to no more than about half of one CPU HW thread, use <i>CFSQuota</i>=50000 and "
                              "<i>CFS Period</i>=100000.  To limit it to two HW threads, set "
                              "<i>CFS Quota</i> to 200000."),
                           editable, CgroupCpu);
    case ContainerModel::Load:
        return makeTooltip(tr("This is a rough measure of the CPU load of the container. It is not "
                              "possible to measure this directly, so this is an indirect indicator. "
                              "It is the ratio of the container's CPU use per second of real time. "
                              "For example, a container fully using two CPU cores will have a load of "
                              "about 2.0."),
                           editable, CgroupCpu);
    case ContainerModel::Memory:
        return makeTooltip(tr("This is the total memory used by the container.<p>"),
                           editable, CgroupMem);
    case ContainerModel::MemoryLimit:
        return makeTooltip(tr("This is system memory limit for the container.  It may use no more "
                              "than this amount."),
                           editable, CgroupMem);
    case ContainerModel::MemPct:
        return makeTooltip(tr("This is the ratio of the memory use of the container to the amount "
                              "of memory it is permitted to use in total."),
                           editable, CgroupMem);
    case ContainerModel::KMem:
        return makeTooltip(tr("This is the kernel memory used by the container."),
                           editable, CgroupMem);
    case ContainerModel::Unprivileged:
        return makeTooltip(tr("This is true for unprivileged containers.  This may not apply to "
                              "all container types.<i></i>"), editable);
    case ContainerModel::Ephemeral:
        return makeTooltip(tr("This is true for emphemeral containers.  These will disappear "
                              "once they shut down.<i></i>"), editable);
    case ContainerModel::Processes:
        return makeTooltip(tr("This is number of processes in the container.  It may not be available "
                              "on all types of containers."), editable, CgroupMem);
    case ContainerModel::Control:
        return makeTooltip(tr("This is true if the current user has permission to control the "
                              "container.  It may be possible to see containers you do not have "
                              "permissions to control.<i></i>"), editable);
    case ContainerModel::HLink:
        return makeTooltip(tr("This is a list of host-side network interfaces used by the container.<i></i>"),
                           editable);
    case ContainerModel::CLink:
        return makeTooltip(tr("This is a list of container-side network interfaces.<i></i>"), editable);
    case ContainerModel::IPV4:
        return makeTooltip(tr("This is a list of IPV4 addresses used by the container.<i></i>"), editable);
    case ContainerModel::IPV6:
        return makeTooltip(tr("This is a list of IPV6 addresses used by the container.<i></i>"), editable);
    case ContainerModel::TxBytes:
        return makeTooltip(tr("This is the total number of bytes transmitted by the container, "
                              "per network interface.<i></i>"), editable);
    case ContainerModel::RxBytes:
        return makeTooltip(tr("This is the total number of bytes received by the container, "
                              "per network interface.<i></i>"), editable);
    case ContainerModel::TxRxBytes:
        return makeTooltip(tr("This is the total number of bytes sent or received by the container, "
                              "per network interface.<i></i>"), editable);
    case ContainerModel::TxSpeed:
        return makeTooltip(tr("This is the transmit speed of the container network in bytes per second.<i></i>"),
                           editable);
    case ContainerModel::RxSpeed:
        return makeTooltip(tr("This is the receive speed of the container network in bytes per second.<i></i>"),
                           editable);
    case ContainerModel::TxRxSpeed:
        return makeTooltip(tr("This is the total speed (transmit+receive) of the container network in bytes per second.<i></i>"),
                           editable);
    case ContainerModel::HWAddress:
        return makeTooltip(tr("This is the hardware interface (MAC address) for the container.<i></i>"),
                           editable);
#if USEIO
    case ContainerModel::ReadBytes:
        return makeTooltip(tr("This is the number of block device (usually disk) bytes read by the container.<i></i>"),
                           editable, CgroupBlkIo);
    case ContainerModel::WriteBytes:
        return makeTooltip(tr("This is the number of block device (usually disk) bytes written by the container.<i></i>"),
                           editable, CgroupBlkIo);
    case ContainerModel::ReadSpeed:
        return makeTooltip(tr("This is the read speed of the container from block devices (usually disks).<i></i>"),
                           editable, CgroupBlkIo);
    case ContainerModel::WriteSpeed:
        return makeTooltip(tr("This is the writeread speed of the container to block devices (usually disks).<i></i>"),
                           editable, CgroupBlkIo);
#endif
    case ContainerModel::OS:
        return makeTooltip(tr("This is the OS running on the container, if available.\n"
                              "For example, <i>GNU/Linux</i>"),
                           editable, AsyncGet);
    case ContainerModel::Kernel:
        return makeTooltip(tr("This is the kernel running on the container, if available.<p>"
                              "For example, <i>4.15.0-24-generic</i><p>"
                              "For paravirtualized container types, it will be identical to "
                              "the host kernel."),
                           editable, AsyncGet);
    case ContainerModel::Machine:
        return makeTooltip(tr("This is the machine type of the OS on the container, if available.<p>"
                              "For example, <i>x86_64</i>"),
                           editable, AsyncGet);

    case ContainerModel::Distro:
        return makeTooltip(tr("This is the distribution running on the container, if available.<p>"
                              "For example, <i>Debian</i>"),
                           editable, AsyncGet);
    case ContainerModel::Release:
        return makeTooltip(tr("This is the distribution running on the container, if available.<p>"
                              "For example, a Debian container may return <i>9.4</i>"),
                           editable, AsyncGet);
    case ContainerModel::Codename:
        return makeTooltip(tr("This is the codename for the OS release running on the container.<p>"
                              "For example, a Debian container may return <i>stretch</i>"),
                           editable, AsyncGet);
    case ContainerModel::ConfigFile:
        return makeTooltip(tr("This is the configuration file used to control the container's "
                              "properties.<i></i>"),
                           editable);
    default:
        assert(0 && "Unknown ContainerData value");
        return { };
    }
}

// tooltip for container column header
QString ContainerModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment ContainerModel::mdAlignment(ModelType mt)
{
    switch (mt) {
#if USEIO
    case ContainerModel::ReadBytes:    [[fallthrough]];
    case ContainerModel::WriteBytes:   [[fallthrough]];
    case ContainerModel::ReadSpeed:    [[fallthrough]];
    case ContainerModel::WriteSpeed:   [[fallthrough]];
#endif
    case ContainerModel::Uptime:       [[fallthrough]];
    case ContainerModel::CpuUse:       [[fallthrough]];
    case ContainerModel::CpuShares:    [[fallthrough]];
    case ContainerModel::CfsQuota:     [[fallthrough]];
    case ContainerModel::CfsPeriod:    [[fallthrough]];
    case ContainerModel::Load:         [[fallthrough]];
    case ContainerModel::Memory:       [[fallthrough]];
    case ContainerModel::MemoryLimit:  [[fallthrough]];
    case ContainerModel::MemPct:       [[fallthrough]];
    case ContainerModel::KMem:         [[fallthrough]];
    case ContainerModel::Processes:    [[fallthrough]];
    case ContainerModel::TxBytes:      [[fallthrough]];
    case ContainerModel::RxBytes:      [[fallthrough]];
    case ContainerModel::TxRxBytes:    [[fallthrough]];
    case ContainerModel::TxSpeed:      [[fallthrough]];
    case ContainerModel::RxSpeed:      [[fallthrough]];
    case ContainerModel::TxRxSpeed:    return Qt::AlignRight   | Qt::AlignVCenter;

    case ContainerModel::Unprivileged: [[fallthrough]];
    case ContainerModel::Ephemeral:    [[fallthrough]];
    case ContainerModel::Control:      return Qt::AlignHCenter | Qt::AlignVCenter;

    default:                           return Qt::AlignLeft    | Qt::AlignVCenter;
    }
}

bool ContainerModel::mdIsRate(ModelType mt)
{
    switch (mt) {
    case ContainerModel::RxSpeed:    [[fallthrough]];
    case ContainerModel::TxSpeed:    [[fallthrough]];
    case ContainerModel::TxRxSpeed:  return true;
#if USEIO
    case ContainerModel::ReadSpeed:  [[fallthrough]];
    case ContainerModel::WriteSpeed: return true;
#endif
    default: return false;
    }
}

const QString& ContainerModel::mdRateSuffix(ModelType mt)
{
    static const QString empty;
    static const QString perS("/s");

    return mdIsRate(mt) ? perS : empty;
}

// Work out which units configuration setting to use, given ContainerData.
// For some purposes, such as graph Y-axes, we can't easily display the
// normal pretty format, and fall back on something simpler: in that case,
// autoTime = true.
const Units& ContainerModel::mdUnits(ModelType mt, bool autoTime)
{
    static const Units unitsStr(Format::String);
    static const Units unitsRaw(Format::PctFloat, 2);
    static const Units unitsLinearTime(Format::AutoTime, 2);

    switch (mt) {
    case ContainerModel::Uptime:    return autoTime ? unitsLinearTime : cfgData().unitsUptime;
    case ContainerModel::CpuUse:    return autoTime ? unitsLinearTime : cfgData().unitsCpuUse;
    case ContainerModel::Processes: [[fallthrough]];
    case ContainerModel::CpuShares: [[fallthrough]];
    case ContainerModel::CfsPeriod: [[fallthrough]];
    case ContainerModel::CfsQuota:  [[fallthrough]];
    case ContainerModel::Load:      return unitsRaw;

    case ContainerModel::MemPct:    return cfgData().unitsPercent;

#if USEIO
    case ContainerModel::ReadBytes:    [[fallthrough]];
    case ContainerModel::WriteBytes:   [[fallthrough]];
    case ContainerModel::ReadSpeed:    [[fallthrough]];
    case ContainerModel::WriteSpeed:   [[fallthrough]];
#endif
    case ContainerModel::Memory:       [[fallthrough]];
    case ContainerModel::KMem:         [[fallthrough]];
    case ContainerModel::MemoryLimit:  return cfgData().unitsMemory;

    case ContainerModel::RxBytes:      [[fallthrough]];
    case ContainerModel::TxBytes:      [[fallthrough]];
    case ContainerModel::TxRxBytes:    [[fallthrough]];
    case ContainerModel::RxSpeed:      [[fallthrough]];
    case ContainerModel::TxSpeed:      [[fallthrough]];
    case ContainerModel::TxRxSpeed:    return cfgData().unitsNetwork;

    default:                           return unitsStr;
    }
}

// update container list
void ContainerModel::updateList()
{
    // TODO: don't update if any outstanding BG threads

    QSet<const ContainerPrivate*> updated; // track which ones got updated, so we can remove cruft.

    for (const auto& cfgDir : cfgData().directories()) {
        // Let container class update list of containers
        if (const ContainerClassBase* cclass = ContainerClassBase::find(cfgDir.containerClass))
            cclass->updateList(*this, QModelIndex(), cfgDir, updated);
    }

    // Remove cruft.
    removeRows([&](const QModelIndex& item) {
            return item.isValid() && privateData(item) != nullptr &&
                   !updated.contains(privateData(item));
        });
}

// Convenience wrapper to covert ContainerData to int
QModelIndex ContainerModel::findRow(const QModelIndex &parent, const QVariant &data, ModelType column, int role, int maxDepth) const
{
    return TreeModel::findRow(parent, data, column, role, maxDepth);
}

bool ContainerModel::setPrivateData(const QModelIndex &index, ContainerPrivate *data)
{
    const bool result = getItem(verify(index))->setPrivateData(data);

    emit dataChanged(index, index, { Util::PrivateDataRole });

    return result;
}

const ContainerPrivate* ContainerModel::privateConstData(const QModelIndex &index) const
{
    return getItem(verify(index))->privateData();
}

ContainerPrivate* ContainerModel::privateData(const QModelIndex &index) const
{
    return getItem(verify(index))->privateData();
}

Qt::ItemFlags ContainerModel::flags(const QModelIndex &index) const
{
    // Custom flags per column
    const Qt::ItemFlags baseFlags = TreeModel::flags(verify(index));

    // We can edit some columns directly
    if (mdIsEditable(index.column()))
        return baseFlags | Qt::ItemIsEditable;

    return baseFlags;
}

QVariant ContainerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        // Handle column header tooltips.
        if (role == Qt::ToolTipRole)
            return mdTooltip(section);

        if (role == Qt::WhatsThisRole)
            return mdWhatsthis(section);
    }

    return TreeModel::headerData(section, orientation, role);
}

const Units& ContainerModel::units(const QModelIndex& idx) const
{
    return mdUnits(idx.column());
}

bool ContainerModel::isContainer(const QModelIndex &idx) const
{
    return static_cast<const ContainerItem*>(getItem(verify(idx)))->isContainer();
}

bool ContainerModel::isState(const QModelIndex& idx, ContainerState cs) const
{
    const QVariant& val = data(ContainerModel::State, verify(idx), Util::RawDataRole);

    return !val.isNull() && ContainerState(val.toInt()) == cs;
}

QVariant ContainerModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return { };

    if (const QVariant val = getItem(idx)->data(idx.column(), role); val.isValid())
        return cfgData().containerColorizer.maybeUse(val, idx, role);

    return cfgData().containerColorizer.colorize(idx, role);
}

void ContainerModel::applyToPrivate(const std::function<bool (ContainerPrivate&, const QModelIndex& idx)>& fn,
                                    const QModelIndex &parent) const
{
    apply([&](const QModelIndex& idx) {
        ContainerPrivate* privData = privateData(idx);
        if (privData != nullptr)
            return fn(*privData, idx);
        return true;  // keep recursing if no private data
    }, parent);
}
