/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <QVector>
#include <QByteArray>

class CfgShell;

// Handle creation of commands to execute in containers.
class Executor
{
public:
    Executor(const QByteArray& cmdLine, const CfgShell& shell);

    const QByteArray& progname() const { return m_progname; }
    const QVector<QByteArray>& argv() const { return m_argv; }
    bool parseError() const { return m_parseError; }

protected:
    QByteArray          m_progname;
    QVector<QByteArray> m_argv;
    bool                m_parseError;
};

#endif // EXECUTOR_H
