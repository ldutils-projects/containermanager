/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CGROUPS_H
#define CGROUPS_H

#include <cassert>
#include <sys/types.h>
#include <QList>
#include <QByteArray>

#include "src/util/util.h"

class CGroups
{
public:
    CGroups();

    template <typename T>
    T read(pid_t pid, const char* key, int buflen = 1024, const char* useController = nullptr) const;

    template <typename T>
    bool write(pid_t pid, const char* key, const T& value, const char* useController = nullptr) const;

    int lineCount(pid_t pid, const char* key, const char* useController = nullptr) const;

private:
    bool open(pid_t pid, const char* key, const char* useController, QFile& file, QIODevice::OpenMode mode) const;

    CGroups(const CGroups&) = delete;
    CGroups& operator=(const CGroups&) = delete;

    using Controllers = QList<QByteArray>;

    mutable Controllers controllers;  // controllers cache
    mutable Controllers single;       // single controller cache

    const   bool containerOnly;       // true to skip non-container specific controllers

    const decltype(controllers)& readControllers(const QByteArray& root, const char* useController) const;
    const decltype(controllers)& readControllers(pid_t pid, const char* useController) const;
};

template <typename T>
inline T CGroups::read(pid_t pid, const char* key, const int buflen, const char* useController) const
{
    QFile file;

    if (!open(pid, key, useController, file, QIODevice::ReadOnly | QIODevice::Text))
        return T();

    return Util::ReadFile<T>(file, buflen);
}


template <typename T>
inline bool CGroups::write(pid_t pid, const char* key, const T& value, const char* useController) const 
{
    QFile file;

    if (!open(pid, key, useController, file, QIODevice::WriteOnly | QIODevice::Text))
        return false;

    return file.write(value) == value.size();
}

#endif // CGROUPS_H
