/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>

#include <src/util/roles.h>
#include "src/util/util.h"
#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "src/core/containerstate.h"
#include "src/ui/windows/mainwindow.h"

#include "mappedbase.h"

MappedBase::MappedBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    Pane(mainWindow, paneClass, parent),
    m_queryColumn(-1),
    m_model(nullptr),
    m_queryRoot(new Query::All())
{
    setModel(&app().containerModel());
}

void MappedBase::setModel(const QAbstractItemModel* model)
{
    if (m_model != nullptr) {
        disconnect(m_model, &QAbstractItemModel::dataChanged, this, &MappedBase::processDataChanged);
        disconnect(m_model, &QAbstractItemModel::rowsInserted, this, &MappedBase::processRowsInserted);
        disconnect(m_model, &QAbstractItemModel::rowsAboutToBeRemoved, this, &MappedBase::processRowsAboutToBeRemoved);
    }

    m_model = model;
    m_queryCtx.setModel(m_model, cfgData().getFilterCaseSensitivity());

    if (m_model != nullptr) {
        // data change/insertion/deletion signals
        connect(m_model, &QAbstractItemModel::dataChanged, this, &MappedBase::processDataChanged);
        connect(m_model, &QAbstractItemModel::rowsInserted, this, &MappedBase::processRowsInserted);
        connect(m_model, &QAbstractItemModel::rowsAboutToBeRemoved, this, &MappedBase::processRowsAboutToBeRemoved);
    }
}

void MappedBase::showAll()
{
    m_queryRoot = std::make_unique<Query::All>();
    invalidateFilter();
}

bool MappedBase::accept(const QModelIndex& idx) const
{
    // Reject if not a container, or not running
    return containerModel.isContainer(idx) && containerModel.isState(idx, ContainerState::Running);
}

void MappedBase::newConfig()
{
    Pane::newConfig();
    setQueryColumn(m_queryColumn);
    m_queryCtx.setCaseSensitivity(cfgData().getFilterCaseSensitivity());
}

bool MappedBase::filterUpdate(const QModelIndex& idx, int& pos, bool accepted,
                              void (MappedBase::*insert)(const QModelIndex&, int),
                              void (MappedBase::*remove)(const QModelIndex&),
                              bool (MappedBase::*contains)(const QModelIndex&) const)
{
    const bool contained = (this->*contains)(idx);
    bool changed = false;

    if (accepted) {
        if (!contained) {
            changed = true;
            (this->*insert)(idx, pos);
        }

        if (pos >= 0)
            ++pos;
    } else {
        if (contained) {
            changed = true;
            (this->*remove)(idx);
        }
    }

    return changed;
}

bool MappedBase::filterUpdate(const QModelIndex& idx, int& pos)
{
    bool changed = false;

    // If the sub-chart can't hide things (only add/remove), use the acceptance filter.
    // If it can hide, just add everything that's running.
    if (filterUpdate(idx, pos, canHide() ? accept(idx) : filterAcceptsRow(idx),
                     &MappedBase::insert, &MappedBase::remove, &MappedBase::contains))
        changed = true;

    if (canHide()) {
        int showPos = pos;
        if (contains(idx))
            if (filterUpdate(idx, showPos, filterAcceptsRow(idx),
                             &MappedBase::show, &MappedBase::hide, &MappedBase::shown))
                changed = true;
    }

    return changed;
}

void MappedBase::processModelReset()
{
    clear();

    if (m_model == nullptr)
        return;

    int pos = 0;

    Util::Recurse(*m_model, [this, &pos](const QModelIndex& idx) {
         filterUpdate(idx, pos);
         return true;
    });
}

void MappedBase::processRowsInserted(const QModelIndex& parent, int start, int end)
{
    if (m_model == nullptr)
        return;

    int pos = -1; // TODO: ... insert where it should be.
    for (int row = start; row <= end; ++row) {
        const QModelIndex index = m_model->index(row, 0, parent);
        filterUpdate(index, pos);
    }
}

void MappedBase::processRowsAboutToBeRemoved(const QModelIndex& parent, int start, int end)
{
    if (m_model == nullptr)
        return;

    for (int row = start; row <= end; ++row)
        remove(m_model->index(row, 0, parent));
}


bool MappedBase::dataChangedUpdate(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                                   const QVector<int>& /*roles*/,
                                   bool& spansState, bool& spansPlot)
{
    if (m_model == nullptr)
        return false;

    spansState = spans(ContainerModel::State, topLeft, bottomRight);
    spansPlot  = m_queryColumn < 0 || spans(m_queryColumn, topLeft, bottomRight);

    if (!spansState && !spansPlot)
        return false;

    bool changed = false;
    int pos = -1;

    // Add and remove to reflect current filter:
    if (spansState) {
        for (int row = topLeft.row(); row <= bottomRight.row(); ++row) {
            const QModelIndex idx = m_model->index(row, 0, topLeft.parent());
            if (filterUpdate(idx, pos)) // TODO: get proper insertion point
                changed = true;
        }
    }

    return changed;
}


void MappedBase::processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                                    const QVector<int>& roles)
{
    bool spansState, spansPlot;

    dataChangedUpdate(topLeft, bottomRight, roles, spansState, spansPlot);
}

void MappedBase::setQueryColumn(int column)
{
    if (m_queryColumn == column)
        return;

    m_queryColumn = column;

    // Don't do if invoked from baseclass (i.e, during construction)
    if (typeid(*this) != typeid(MappedBase))
        invalidateFilter();
}

void MappedBase::setQuery(const QString& query)
{
    m_queryRoot = m_queryCtx.parse(m_queryText = query);
    invalidateFilter();
}

bool MappedBase::invalidateFilter()
{
    bool changed  = false;
    int  pos = 0;

    if (m_model == nullptr)
        return false;

    Util::Recurse(*m_model, [&](const QModelIndex& idx) {
        if (filterUpdate(idx, pos, filterAcceptsRow(idx),
                         &MappedBase::show, &MappedBase::hide, &MappedBase::shown))
            changed = true;
        return true;
    });

    return changed;
}

bool MappedBase::filterAcceptsRow(const QModelIndex& idx) const
{
    if (!accept(idx) || m_model == nullptr)
        return false;

    // accept if query matches it.  -1 to handle "All".
    return m_queryRoot->matchIdx(idx, m_queryColumn);
}

void MappedBase::save(QSettings& settings) const
{
    Pane::save(settings);

    MemberSave(settings, m_queryColumn);
    MemberSave(settings, m_queryText);
}

void MappedBase::load(QSettings& settings)
{
    Pane::load(settings);

    MemberLoad(settings, m_queryColumn);
    MemberLoad(settings, m_queryText);

    setQueryColumn(m_queryColumn);
    setQuery(m_queryText);
}
