/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>

#include <QtGlobal>
#include <QMessageBox>
#include <QSet>
#include <QIcon>
#include <QAction>
#include <QMenu>
#include <QInputDialog>
#include <QWhatsThis>
#include <QSignalBlocker>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <src/fwddeclbase.h>
#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/roles.h>
#include <src/util/icons.h>
#include <src/util/units.h>
#include <src/util/cmdlinebase.h>
#include <src/util/resources.h>
#include <src/ui/widgets/tabwidget.h>
#include "src/version.h"
#include "src/ui/panes/consolepane.h"
#include "src/core/containerstate.h"
#include "src/core/iconcache.h"
#include "src/core/containercontrol.h"
#include "src/core/containerprivate.h"
#include "src/core/app.h"
#include "src/util/cmdline.h"

MainWindow::MainWindow(QWidget *parent, const CmdLine* cmdLineOverride) :
    MainWindowBase(::Apptitle, cmdLineOverride != nullptr ? *cmdLineOverride : app().cmdLine(), parent),
    ui(new Ui::MainWindow),
    m_iconCache(":art/logos"),
    initStatic(this),
    m_appConfig(this),
    m_aboutDialog(this),
    autosaveTimer(this),
    listUpdateTimer(this),
    dataUpdateTimer(this),
    bgTaskTimer(this),
    eggTimer(this),
    systemTrayIcon(this),
    bgTaskCount(0),
    bgTaskDone(0),
    bgTaskFailed(0)
{
    ui->setupUi(this);

    ui->centralWidget->layout()->addWidget(new TabWidget(*this));

    setupSession();          // session management
    setupMenus();            // menu slot connections
    setupActionTooltips();   // from parent class: set WhatsThis to match ToolTip
    resetEnvironment();      // remove unwanted env vars
    setupTimers();           // update timers
    setupStatus();           // create status bar widgets
    updateAll();             // seed initial data
    setupAppConfig();        // application name, etc
    setupSystemTray();       // system tray icon
    setupSignals();          // connect signals/slots
    setupAutosave();           // initialize timers
    setupActionIcons();      // default icons if there's no theme icon
    Util::SetupWhatsThis(this);

    sessionRestore();        // restore any prior session
    setupDefaultPanels();    // setup default panel config (after loading session)
    recentSessionsChanged(); // virtual method can't be invoked from parent constructor
}

MainWindow::~MainWindow()
{
    app().haltThreads();  // do before deleting UI
    cleanup();

    delete ui;
    ui = nullptr;
}

void MainWindow::setupSystemTray()
{
//    systemTrayIcon.setContextMenu(trayIconMenu);
//    systemTrayIcon.setIcon(windowIcon());
    //    systemTrayIcon.show();
}

void MainWindow::setupSignals()
{
    MainWindowBase::setupSignals();

    // Undo signals update dirty flag, and actions.  The view undoer doesn't update dirty.
    connect(&app().undoMgr(), &UndoMgr::dirtyStateChanged, this, &MainWindow::dirtyStateChanged);

    connect(&undoMgr(), &UndoMgr::undoAdded, this, &MainWindow::updateActions);
    connect(&undoMgr(), &UndoMgr::changeApplied, this, &MainWindow::postUndoActions);
}

void MainWindow::setupAutosave()
{
    if (cfgData().dataAutosaveInterval > 0) {
        autosaveTimer.setSingleShot(true);
        connect(&autosaveTimer, &QTimer::timeout, this, &MainWindow::sessionSave, Qt::UniqueConnection);
    } else {
        disconnect(&autosaveTimer, &QTimer::timeout, this, &MainWindow::sessionSave);
    }
}

// Restore status color, or warning/etc colors stick for status tips
void MainWindow::statusChanged(const QString& /*txt*/) const
{
    changeStatusBarColor(QPalette::Text);
}

void MainWindow::startedBGTask(ThreadBase*)
{
    ++bgTaskCount;
    updateProgress(bgTaskDone, bgTaskCount, bgTaskFailed);
}

void MainWindow::finishedBGTask(int rc)
{
    ++bgTaskDone;
    if (rc != 0)
        ++bgTaskFailed;

    updateProgress(bgTaskDone, bgTaskCount, bgTaskFailed);

    // We do this after a small delay, to mitigate weird behaviors where thread-start
    // spam overlaps with initial threads finishing and resets the progress bar.
    bgTaskTimer.start();
}

// Default panel config
void MainWindow::setupDefaultPanels()
{
    TabWidget* tabs = mainWindowTabs();

    const QSignalBlocker bts(tabs);

    if (tabs->count() > 1)  // skip if the session loaded panels already.
        return;

    auto* hGroup        = containerFactory();
    auto* containerPane = paneFactory(PaneClass::Container);
    auto* detailPane    = paneFactory(PaneClass::Detail);

    hGroup->setOrientation(Qt::Horizontal);

    tabs->addTab("Containers", hGroup);

    addPane(containerPane, hGroup);
    addPane(detailPane, hGroup);

    containerPane->setFocus();

    tabs->setCurrentIndex(0);

    resizeColumnsAllPanes();

    QList<int> sizes = { 100, 30 };

    hGroup->setSizes(sizes);
    hGroup->setStretchFactor(0, 80);
    hGroup->setStretchFactor(1, 20);
}

void MainWindow::egg()
{
    const auto eggs = Util::ReadFile<QList<QByteArray>>(":/text/eggs.txt");

    if (!eggs.empty()) {
        statusMessage(UiType::Normal, eggs[rand() % eggs.size()]);
        eggTimer.start();
    }
}

QAction* MainWindow::getControlAction(ContainerControl cc) const
{
    if (ui == nullptr) // shutting down
        return nullptr;

    switch (cc) {
    case ContainerControl::Start:     return ui->action_Start_Container;
    case ContainerControl::Stop:      return ui->action_Stop_Container;
    case ContainerControl::Freeze:    return ui->action_Freeze_Container;
    case ContainerControl::Thaw:      return ui->action_Thaw_Container;
    case ContainerControl::ExecuteIn: return ui->action_ExecuteIn_Container;
    case ContainerControl::Reboot:    return ui->action_Reboot_Container;
    case ContainerControl::Create:    return ui->action_Create_Container;
    case ContainerControl::Remove:    return ui->action_Remove_Container;

    default:                          return nullptr;
    }
}

QAction* MainWindow::getPaneAction(PaneAction cc) const
{
    if (ui == nullptr)
        return nullptr;

    switch (cc) {
    case PaneAction::ShowAll:             return ui->action_View_Show_All;
    case PaneAction::ExpandAll:           return ui->action_View_Expand_All;
    case PaneAction::CollapseAll:         return ui->action_View_Collapse_All;
    case PaneAction::SelectAll:           return ui->action_View_Select_All;
    case PaneAction::SelectNone:          return ui->action_View_Select_None;
    case PaneAction::ResizeToFit:         return ui->action_Size_Cols;
    case PaneAction::SetFiltersVisible:   return ui->action_Show_Filters;
    case PaneAction::CopySelected:        return ui->action_Copy_Selected;
    case PaneAction::ViewAsTree:          return ui->action_View_AsTree;
    case PaneAction::PaneClose:           return ui->action_Pane_Close;
    case PaneAction::PaneAdd:             return nullptr; // it has sub-menus
    case PaneAction::PaneReplace:         return nullptr; // it has sub-menus
    case PaneAction::PaneSplitH:          return ui->action_Pane_Split_Horizontal;
    case PaneAction::PaneSplitV:          return ui->action_Pane_Split_Vertical;
    case PaneAction::PaneLeft:            return ui->action_Pane_Left;
    case PaneAction::PaneRight:           return ui->action_Pane_Right;
    case PaneAction::PaneRowUp:           return ui->action_Pane_Group_Left;
    case PaneAction::PaneRowDown:         return ui->action_Pane_Group_Right;
    case PaneAction::PaneBalanceSiblings: return ui->action_Pane_Balance_Siblings;
    case PaneAction::PaneBalanceTab:      [[fallthrough]];
    case PaneAction::PaneAddTab:          [[fallthrough]];
    case PaneAction::PaneRenameTab:       [[fallthrough]];
    case PaneAction::PaneCloseTab:        return mainWindowTabs()->getPaneAction(cc);
    default:                              return nullptr;
    }
}

void MainWindow::setupTimers()
{
    // list timer
    connect(&listUpdateTimer, &QTimer::timeout, this, &MainWindow::updateList);

    listUpdateTimer.setInterval(cfgData().listUpdateInterval * CfgData::msPerUpdateTick);
    listUpdateTimer.start();

    // data timer
    connect(&dataUpdateTimer, &QTimer::timeout, this,
            static_cast<void (MainWindow::*)()>(&MainWindow::updateData));

    dataUpdateTimer.setInterval(cfgData().dataUpdateInterval * CfgData::msPerUpdateTick);
    dataUpdateTimer.start();

    // bg task timer, for nicer status bar behavior
    connect(&bgTaskTimer, &QTimer::timeout, this, &MainWindow::resetProgress);
    bgTaskTimer.setInterval(CfgData::msPerUpdateTick / 2);
    bgTaskTimer.setSingleShot(true);

    // egg timer
    connect(&eggTimer, &QTimer::timeout, this, &MainWindow::egg);
    eggTimer.setInterval(12000 * CfgData::msPerUpdateTick);
    eggTimer.start();
}

void MainWindow::setTimersEnabled(bool enable)
{
    if (enable) {
        listUpdateTimer.start();
        listUpdateTimer.start();
    } else {
        listUpdateTimer.stop();
        dataUpdateTimer.stop();
    }
}

QModelIndexList MainWindow::selectedContainers(PaneBase* focus) const
{
    PaneBase* focused = focusedPane(focus);

    if (focused != nullptr)
         return focused->getSelections();

    return { };
}

QModelIndexList MainWindow::selectedContainersWarn(PaneBase* focus) const
{
    PaneBase* focused = focusedPaneWarn(focus);

    if (focused != nullptr && !focused->hasSelection())
        statusMessage(UiType::Info, tr("No containers selected."));

    return selectedContainers(focused);
}

void MainWindow::recentSessionsChanged()
{
    if (ui == nullptr)
        return;

    return recentSessionsChanged(ui->menu_Recent_Sessions);
}

void MainWindow::resetEnvironment()
{
    // Remove environment vars except for these
    QVector<std::tuple<QByteArray, QByteArray>> preserve = {
        { "HOME", nullptr },
        { "USER", nullptr },
        { "PATH", nullptr },
        { "LOGNAME", nullptr },
        { "USERNAME", nullptr },
    };

    for (auto& p : preserve)
        std::get<1>(p) = qgetenv(std::get<0>(p));

    bool success = true;

    success = success && (clearenv() == 0);

    for (auto& p : preserve)
        success = success && (setenv(std::get<0>(p), std::get<1>(p), 0) == 0);

    if (!success) {
        QMessageBox::critical(this, tr("Aborting"), tr("Unable to set up environment"));
        throw Exit(5);
    }
}

void MainWindow::newConfig(bool newValues)
{
    undoMgr().newConfig(cfgData()); // Refresh undo manager

    // New autosave timer, and trigger save if there's dirty data and timer went from zero to non-zero
    if (newValues) {
        updateList();

        listUpdateTimer.setInterval(cfgData().listUpdateInterval * CfgData::msPerUpdateTick);
        dataUpdateTimer.setInterval(cfgData().dataUpdateInterval * CfgData::msPerUpdateTick);

        app().ctrlPool().setMaxThreadCount(cfgData().ctrlThreadCount);
        app().dataPool().setMaxThreadCount(cfgData().dataThreadCount);

        setupAutosave();
        dirtyStateChanged(newValues);
    }

    // let panes update from new config
    runOnPanels([](PaneBase* pane) { pane->newConfig(); });
}

void MainWindow::viewAsTree(bool asTree)
{
    runOnFocusPane(&PaneBase::viewAsTree, asTree);

    ui->action_View_AsTree->setChecked(asTree);
    resizeColumnsAllPanes();
}

void MainWindow::setupStatus()
{
    for (auto& t : statusText) {
       statusBar()->addWidget(&t);
       if (&t != &statusText.back())
           statusBar()->addWidget(new QLabel("|"));
    }

    // Add progress bar
    m_progressBar.setOrientation(Qt::Horizontal);
    m_progressBar.setMaximumWidth(300);
    statusBar()->addPermanentWidget(&m_progressBar);

    connect(statusBar(), &QStatusBar::messageChanged, this, &MainWindow::statusChanged);
}

void MainWindow::updateActions()
{
    if (ui == nullptr)
        return;

    PaneBase* focused = focusedPane();

    const bool hasFocused      = (focused != nullptr);
    const bool hasSelection    = hasFocused && focused->hasSelection();
    const bool hasSettingsFile = MainWindowBase::hasSettingsFile();

    // Update enabled status of things that need as selection
    {
        for (ContainerControl cc = ContainerControl::_First; cc < ContainerControl::_Count; Util::inc(cc))
            if (ContainerControlInfo::needsSelection(cc))
                if (QAction* action = getControlAction(cc))
                    action->setEnabled(hasSelection);

        ui->action_View_Select_None->setEnabled(hasSelection);
        ui->action_Copy_Selected->setEnabled(hasSelection);
    }

    // Update enabled state of pane actions/menus
    {
        for (PaneAction pa = PaneAction::_BeginData; pa < PaneAction::_Count; Util::inc(pa)) {
            const bool disable = (hasFocused && !focused->supportsAction(pa)) ||
                    (Pane::needsFocus(pa) && !hasFocused);

            if (QAction* action = getPaneAction(pa)) {
                action->setEnabled(!disable);
            } else {
                // Handle submenus (only PaneReplace applies ATM)
                if (pa == PaneAction::PaneReplace)
                    for (auto& a : paneClassRepActions())
                        a->setEnabled(!disable);
            }
        }

        // Technically this doesn't need a pane, but it's less confusing to pretend it does.
        ui->action_Containers_Refresh->setEnabled(hasFocused);
    }

    ui->action_Revert->setEnabled(hasSettingsFile);
    ui->action_Save_Settings->setEnabled(hasSettingsFile);
    ui->action_Save_Settings->setEnabled(hasSettingsFile && !m_privateSession);
    ui->action_Save_Settings_As->setEnabled(!m_privateSession);

    updateUndoActions(&undoMgr(), ui->action_Undo, ui->action_Redo, ui->action_Clear_Undo_Stack);
}

void MainWindow::selectionChanged(const QItemSelectionModel*,
                                  const QItemSelection& /*selected*/, const QItemSelection& /*deselected*/)
{
    updateActions();
    updateStatus();

    // Let multiple consumers get this from a single place, rather than N different panes.
    emit paneSelectionChanged();
}

void MainWindow::currentChanged(const QModelIndex &current)
{
    // This is basically a funnel point for all panes to report changed events to.
    // Consumers can then get the result from here, rather than N different panes.
    emit paneCurrentContainerChanged(Util::MapDown(current));
}

void MainWindow::setStat(MainWindow::Stat s, const char* txt, const std::function<bool(const QModelIndex&)>& pred,
                         ContainerState state, const QByteArray& fontStyle, const QColor& color)
{
    statusText.at(int(s)).setText(tr(txt).arg(app().containerModel().count(pred)));

    if (state != ContainerState::Unknown)
        Util::SetWidgetStyle(&statusText.at(int(s)), color, fontStyle);
}

void MainWindow::setStat(MainWindow::Stat s, const char* txt, const std::function<bool(const QModelIndex&)>& pred,
                         ContainerState state, const QByteArray& fontStyle)
{
    setStat(s, txt, pred, state, fontStyle, colorForState(state));
}

void MainWindow::updateStatus(bool force)
{
    if (!statusBar()->isVisible() && !force)
        return;

    using namespace std::placeholders;

    const auto isContainerP = std::bind(&ContainerModel::isContainer, &app().containerModel(), _1);
    const auto isRunningP   = std::bind(&ContainerModel::isState, &app().containerModel(), _1, ContainerState::Running);
    const auto isStoppedP   = std::bind(&ContainerModel::isState, &app().containerModel(), _1, ContainerState::Stopped);
    const auto isFrozenP    = std::bind(&ContainerModel::isState, &app().containerModel(), _1, ContainerState::Frozen);

    const auto isSelectedP  = [&](const QModelIndex& idx) {
        if (PaneBase* focus = focusedPane())
            return focus->isSelected(idx) && app().containerModel().isContainer(idx);
        return false;
    };

    const QColor hlColor = QGuiApplication::palette().color(QPalette::Highlight);

    setStat(Stat::Total,    "%1 total",       isContainerP, ContainerState::Defined);
    setStat(Stat::Selected, "%1 selected",    isSelectedP,  ContainerState::Defined, "italic", hlColor);
    setStat(Stat::Running,  "%1 running",     isRunningP,   ContainerState::Running);
    setStat(Stat::Stopped,  "%1 stopped",     isStoppedP,   ContainerState::Stopped);
    setStat(Stat::Frozen,   "%1 frozen",      isFrozenP,    ContainerState::Frozen);

    // Memory total
    qulonglong memTotal = 0;
    app().containerModel().apply([&memTotal](const QModelIndex& idx) {
        if (app().containerModel().isContainer(idx))
            memTotal += app().containerModel().data(ContainerModel::Memory, idx, Util::RawDataRole).toULongLong();
        return true;
    });

    statusText[int(Stat::TotalMem)].setText(QString(tr("Mem Total: ")) +
                                            Units(Format::AutoBinary)(memTotal));
}

void MainWindow::updateStatus()
{
    updateStatus(false);
}

// Try to work out which color should draw this state.
QColor MainWindow::colorForState(ContainerState state)
{
    if (const ColorizerItem* cc = cfgData().containerColorizer.find(ContainerStateInfo::text(state), ContainerModel::State);
            cc != nullptr)
        return cc->m_fgColor;

    // We failed to find it.  Use normal text color.
    return QGuiApplication::palette().color(QPalette::Text);
}

void MainWindow::commitData(QSessionManager& /*manager*/)
{
    sessionSave();
}

void MainWindow::setupSession() const
{
    QGuiApplication::setFallbackSessionManagementEnabled(false);
    connect(qApp, &QApplication::commitDataRequest, this, &MainWindow::commitData);
}

void MainWindow::setupMenus()
{
    const auto newAction = [&](QList<QAction*>& actions, PaneClass pc) {
        if (auto* action = new QAction(Icons::get(Pane::iconFile(pc)), Pane::name(pc), this)) {
            action->setToolTip(Pane::tooltip(pc));
            action->setWhatsThis(Pane::tooltip(pc));
            action->setStatusTip(Pane::tooltip(pc));
            actions.push_back(action);
        }
    };

    // Create actions for adding all the kinds of panes we have.
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc)) {
        newAction(paneClassAddActions(), pc);
        newAction(paneClassGrpActions(), pc);
        newAction(paneClassRepActions(), pc);
        newAction(paneClassWinActions(), pc);
    }

    ui->menuPane->addSeparator();
    ui->menuPane->addActions({ getPaneAction(PaneAction::PaneBalanceTab),
                               getPaneAction(PaneAction::PaneAddTab),
                               getPaneAction(PaneAction::PaneRenameTab),
                               getPaneAction(PaneAction::PaneCloseTab) });

    ui->menu_Add_Pane->addActions(paneClassAddActions());
    connect(ui->menu_Add_Pane, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::addPaneAction));

    ui->menu_Add_Group->addActions(paneClassGrpActions());
    connect(ui->menu_Add_Group, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::addGroupAction));

    ui->menu_Replace_Pane->addActions(paneClassRepActions());
    connect(ui->menu_Replace_Pane, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::replacePaneAction));

    ui->menu_Pane_in_Window->addActions(paneClassWinActions());
    connect(ui->menu_Pane_in_Window, &QMenu::triggered, this,
            static_cast<void(MainWindow::*)(QAction*)>(&MainWindow::paneInWindowAction));

    // menu connections
    connect(ui->action_About_Qt, &QAction::triggered, QCoreApplication::instance(), &QApplication::aboutQt);
    connect(ui->action_Configure, &QAction::triggered, &m_appConfig, &AppConfig::show);
    connect(ui->action_View_AsTree, &QAction::triggered, this, &MainWindow::viewAsTree);
    connect(ui->action_About_CM, &QAction::triggered, &m_aboutDialog, &AboutDialog::show);
    connect(ui->action_What_is, &QAction::triggered, this, &QWhatsThis::enterWhatsThisMode);
}

void MainWindow::setupActionTooltips()
{
    MainWindowBase::setupActionTooltips();
}

void MainWindow::updateList()
{
    app().containerModel().updateList();
    postUpdate();
}

void MainWindow::updateData()
{
    updateData(false);
}

void MainWindow::updateData(bool force)
{
    emit dataAboutToBeUpdated();
    app().containerModel().updateData(force);
    postUpdate();
    emit dataUpdated();
}

void MainWindow::updateAll()
{
    updateList();
    updateData(true);
    updateStatus(true);
}

MainWindow::InitStatic::InitStatic(MainWindow *mainWindow)
{
    ContainerClassBase::initContainerTypes(*mainWindow);
}

void MainWindow::on_action_Show_Filters_triggered(bool checked)
{
    runOnPanels([checked](PaneBase* pane) {
        pane->setFiltersVisible(checked);
    });
}

void MainWindow::on_action_Copy_Selected_triggered()
{
    runOnFocusPane(&Pane::copySelected);
}

void MainWindow::on_action_View_Show_All_triggered()
{
    runOnFocusPane(&Pane::showAll);
}

void MainWindow::on_action_View_Expand_All_triggered()
{
    runOnFocusPane(&Pane::expandAll);
}

void MainWindow::on_action_View_Collapse_All_triggered()
{
    runOnFocusPane(&Pane::collapseAll);
}

void MainWindow::on_action_View_Select_All_triggered()
{
    runOnFocusPane(&Pane::selectAll);
}

void MainWindow::on_action_View_Select_None_triggered()
{
    runOnFocusPane(&Pane::selectNone);
}

void MainWindow::on_action_Size_Cols_triggered()
{
    resizeColumnsAllPanes();
}

void MainWindow::newFocus(QObject* focus)
{
    MainWindowBase::newFocus(focus);

    if (PaneBase* focusPane = focusedPane(focus); focusPane != nullptr)
        ui->action_View_AsTree->setChecked(focusPane->viewIsTree());

    updateActions();
    updateStatus();
}

template <typename... ARG>
void MainWindow::containerAction(ContainerControl cc, const ARG&...)
{
    app().containerModel().control(cc, selectedContainersWarn());
}

void MainWindow::on_action_Start_Container_triggered()
{
    containerAction(ContainerControl::Start);
}

void MainWindow::on_action_Stop_Container_triggered()
{
    containerAction(ContainerControl::Stop);
}

void MainWindow::on_action_Freeze_Container_triggered()
{
    containerAction(ContainerControl::Freeze);
}

void MainWindow::on_action_Thaw_Container_triggered()
{
    containerAction(ContainerControl::Thaw);
}

void MainWindow::on_action_Reboot_Container_triggered()
{
    containerAction(ContainerControl::Reboot);
}

// Find child of given class and focus/show it, or add a new tab with that type
// if none were found.
template<typename P> P* MainWindow::findOrAddPane(PaneClass newPc, const QString& newTabName)
{
    TabWidget* tabs = mainWindowTabs();

   // Search through tabs, so we know which to show.
    for (int tab = 0; tab < tabs->count() - 1; ++tab) {
        if (P* pane = tabs->widget(tab)->findChild<P*>("ConsolePane")) {
            mainWindowTabs()->setCurrentIndex(tab);
            pane->setFocus();
            return pane;
        }
    }

    // Not found: add a new tab with this pane class, and return that.
    if (P* pane = Pane::factory<P>(newPc, *this)) {
        tabs->addTab(newTabName, pane);
        return pane;
    }

    return nullptr;
}

void MainWindow::on_action_ExecuteIn_Container_triggered()
{
    bool ok;
    const QString cmdline =
        QInputDialog::getText(this, tr("Command Line"),
                              tr("Command to execute:"), QLineEdit::Normal, "", &ok);

    if (!ok || cmdline.isEmpty()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    if (auto* op = findOrAddPane<ConsolePane>(PaneClass::Console, "Console")) {
        op->setContainerSet(selectedContainersWarn());
        op->execute(cmdline);
    } else {
        statusMessage(UiType::Error, tr("Unable to find or create output pane"));
        return;
    }
}

void MainWindow::on_action_Create_Container_triggered()
{
    unimplemented();
}

void MainWindow::on_action_Remove_Container_triggered()
{
    if (cfgData().warnOnRemove) {
        QString removeMessage = tr("WARNING!  This will remove the selected containers permanently:\n");

        const QModelIndexList toRemove = selectedContainersWarn();

        if (toRemove.empty())
            return;

        for (const auto& i : toRemove)
            removeMessage += "   " + app().containerModel().data(i, ContainerModel::Name).toString() + "\n";

        if (warningDialog(tr("Remove Containers"), removeMessage) != QMessageBox::Ok)
            return;
    }

    containerAction(ContainerControl::Remove);
}

void MainWindow::on_action_Containers_Refresh_triggered()
{
    updateAll();
}

void MainWindow::on_action_Pane_Close_triggered()
{
    removePane(focusedPaneWarn());
}

void MainWindow::on_action_Pane_Left_triggered()
{
    movePane(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Right_triggered()
{
    movePane(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Pane_Group_Left_triggered()
{
    movePaneParent(focusedPaneWarn(), -1);
}

void MainWindow::on_action_Pane_Group_Right_triggered()
{
    movePaneParent(focusedPaneWarn(), +1);
}

void MainWindow::on_action_Open_Settings_triggered()
{
    openSettings();
}

void MainWindow::on_action_Save_Settings_As_triggered()
{
    saveSettingsAs();
}

void MainWindow::on_action_Save_Settings_triggered()
{
    saveSettings();
}

void MainWindow::on_action_Revert_triggered()
{
    revertSettings();
}

void MainWindow::on_action_Enlarge_Font_triggered()
{
    changeFontSize(1.1);
}

void MainWindow::on_action_Shrink_Font_triggered()
{
    changeFontSize(0.9);
}

void MainWindow::on_action_Reset_Font_triggered()
{
    setFontSize(m_startupFontSize);
}

void MainWindow::resetProgress()
{
    if (bgTaskCount != bgTaskDone)
        return;

    if (bgTaskFailed == 0) {
        const QString successMsg = tr("All %1 task(s) succeeded");
        statusMessage(UiType::Success, successMsg.arg(bgTaskCount));
    }

    bgTaskCount  = 0;
    bgTaskDone   = 0;
    bgTaskFailed = 0;
    updateProgress(0, 0, 0);
}


void MainWindow::updateProgress(int done, int total, int failed)
{
    m_progressBar.setValue(done);
    m_progressBar.setMaximum(total);

    if (total == 0) {
        m_progressBar.reset();
        m_progressBar.setMaximum(1);
        m_progressBar.setFormat(tr("Done"));
    } else {
        m_progressBar.setFormat(tr("%p% (%v / %m)"));
    }

    if (failed > 0) {
        // Ugh - it's a big pain to change the progress bar color, because that can only be
        // dona via CSS, which also changes a lot of other things about it.
        const QString failedMsg = tr("ERROR: %1 of %2 task(s) failed");
        statusMessage(UiType::Error, failedMsg.arg(failed).arg(total));
    }
}

void MainWindow::resortAll()
{
    runOnPanels<PaneBase>([](PaneBase* pane) { pane->sort(); });
}

// Things to do after data or list updates
void MainWindow::postUpdate()
{
    // Re-sort: we do this only on the data update, to avoid excessive re-sorts
    resortAll();
    updateStatus();  // update status line for running/etc counts
}

void MainWindow::on_action_Pane_Balance_Siblings_triggered()
{
    balanceSiblingsInteractive();
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Horizontal_triggered()
{
    splitPaneInteractive(focusedPaneWarn(), Qt::Horizontal);
}

// Replace pane with splitter containing pane, and new pane.
void MainWindow::on_action_Pane_Split_Vertical_triggered()
{
    splitPaneInteractive(focusedPaneWarn(), Qt::Vertical);
}

QWidget* MainWindow::paneFactory(PaneClass_t paneClass) const
{
    if (paneClass == int(PaneClass::Group))
        return containerFactory();

    if (paneClass < int(PaneClass::_First))
        paneClass = PaneClass_t(int(PaneClass::Empty));

    return Pane::factory<QWidget>(PaneClass(int(paneClass)), const_cast<MainWindow&>(*this));
}

PaneBase* MainWindow::paneFactory(PaneClass paneClass) const
{
    return dynamic_cast<PaneBase*>(paneFactory(PaneClass_t(int(paneClass))));
}

Pane::Container* MainWindow::containerFactory() const
{
    return Pane::factory<Pane::Container>(PaneClass::Group, const_cast<MainWindow&>(*this));
}

void MainWindow::setupContainerActionContextMenu(QMenu& contextMenu, bool actionsInSubMenu) const
{
    QMenu* actionMenu = &contextMenu;

    if (actionsInSubMenu)
        actionMenu = contextMenu.addMenu("Container");

    // Container action context menu
    for (ContainerControl cc = ContainerControl::_First; cc < ContainerControl::_Count; Util::inc(cc)) {
        if (QAction* action = getControlAction(cc))
            actionMenu->addAction(action);

        if (ContainerControlInfo::separatorAfter(cc))
            actionMenu->addSeparator();
    }
}

void MainWindow::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

     MainWindowBase::saveUiConfig(settings);

     const_cast<MainWindow&>(*this).markModified(false, true); // mark as not modified post-save
}

void MainWindow::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    MainWindowBase::loadUiConfig(settings);

    markModified(false, true);         // mark as not modified post-load
}

void MainWindow::postLoadHook()
{
    ui->action_Show_Statusbar->setChecked(statusBar()->isVisible());
    ui->action_Show_Toolbar->setChecked(ui->mainToolBar->isVisible());

    MainWindowBase::postLoadHook();
}

void MainWindow::dirtyStateChanged(bool dirty)
{
    if (!dirty)
        return;

    markModified(dirty);

    // Set global modification flag to display in window title
    if (cfgData().dataAutosaveInterval > 0)
        autosaveTimer.start(cfgData().dataAutosaveInterval * 1000); // dataAutosaveInterval is in S, timer in mS
}

void MainWindow::on_action_New_Session_triggered()
{
    if (cfgData().warnOnRevert)
        if (warningDialog(tr("New Session"), tr("Reset settings to defaults?")) != QMessageBox::Ok)
            return;

    m_appConfig.resetDefault();
    setupDefaultPanels();
    statusMessage(UiType::Info, tr("Session reset."));
}

void MainWindow::on_action_Quit_without_Save_triggered()
{
    m_saveOnExit = false;
    close();
}

// Tedious: fix actions which didn't find icons in any default theme.  See comment in Icons get() method
// for why this appears necessary.
void MainWindow::setupActionIcons()
{
    Icons::defaultIcon(ui->action_About_CM,                "help-about");
    Icons::defaultIcon(ui->action_About_Qt,                "system-help");
    Icons::defaultIcon(ui->action_Configure,               "configure");
    Icons::defaultIcon(ui->action_Container_Configuration, "configure");
    Icons::defaultIcon(ui->action_Containers_Refresh,      "view-refresh");
    Icons::defaultIcon(ui->action_Copy_Selected,           "edit-copy");
    Icons::defaultIcon(ui->action_Create_Container,        "document-new");
    Icons::defaultIcon(ui->action_Donate,                  "help-donate");
    Icons::defaultIcon(ui->action_Enlarge_Font,            "format-font-size-more");
    Icons::defaultIcon(ui->action_ExecuteIn_Container,     "system-run");
    Icons::defaultIcon(ui->action_Freeze_Container,        "system-suspend");
    Icons::defaultIcon(ui->action_New_Session,             "document-new");
    Icons::defaultIcon(ui->action_New_Window,              "window-new");
    Icons::defaultIcon(ui->action_Open_Settings,           "document-open");
    Icons::defaultIcon(ui->action_Pane_Balance_Siblings,   "object-columns");
    Icons::defaultIcon(ui->action_Pane_Close,              "tab-close");
    Icons::defaultIcon(ui->action_Pane_Group_Left,         "go-previous");
    Icons::defaultIcon(ui->action_Pane_Group_Right,        "go-next");
    Icons::defaultIcon(ui->action_Pane_Left,               "go-previous");
    Icons::defaultIcon(ui->action_Pane_Right,              "go-next");
    Icons::defaultIcon(ui->action_Pane_Split_Horizontal,   "view-split-left-right");
    Icons::defaultIcon(ui->action_Pane_Split_Vertical,     "view-split-top-bottom");
    Icons::defaultIcon(ui->action_Quit,                    "application-exit");
    Icons::defaultIcon(ui->action_Quit_without_Save,       "application-exit");
    Icons::defaultIcon(ui->action_Reboot_Container,        "system-reboot");
    Icons::defaultIcon(ui->action_Remove_Container,        "edit-delete");
    Icons::defaultIcon(ui->action_Reset_Font,              "application-x-font-ttf");
    Icons::defaultIcon(ui->action_Revert,                  "document-revert");
    Icons::defaultIcon(ui->action_Save_Settings,           "document-save");
    Icons::defaultIcon(ui->action_Save_Settings_As,        "document-save-as");
    Icons::defaultIcon(ui->action_Show_Filters,            "view-filter");
    Icons::defaultIcon(ui->action_Show_Statusbar,          "kt-show-statusbar");
    Icons::defaultIcon(ui->action_Show_Toolbar,            "configure-toolbars");
    Icons::defaultIcon(ui->action_Shrink_Font,             "format-font-size-less");
    Icons::defaultIcon(ui->action_Size_Cols,               "zoom-fit-best");
    Icons::defaultIcon(ui->action_Start_Container,         "media-playback-start");
    Icons::defaultIcon(ui->action_Stop_Container,          "process-stop");
    Icons::defaultIcon(ui->action_Thaw_Container,          "media-playback-start");
    Icons::defaultIcon(ui->action_View_AsTree,             "view-list-tree");
    Icons::defaultIcon(ui->action_View_Collapse_All,       "format-indent-less");
    Icons::defaultIcon(ui->action_View_Expand_All,         "format-indent-more");
    Icons::defaultIcon(ui->action_View_Select_All,         "edit-select-all");
    Icons::defaultIcon(ui->action_View_Select_None,        "edit-select-none");
    Icons::defaultIcon(ui->action_View_Show_All,           "view-restore");
    Icons::defaultIcon(ui->action_What_is,                 "help-whatsthis");
    Icons::defaultIcon(ui->action_Clear_Undo_Stack,        "edit-clear");
    Icons::defaultIcon(ui->action_Undo,                    "edit-undo");
    Icons::defaultIcon(ui->action_Redo,                    "edit-redo");
}

void MainWindow::on_action_New_Window_triggered()
{
    newWindowInteractive();
}

void MainWindow::on_action_Donate_triggered()
{
    m_aboutDialog.showTab(AboutDialog::Donate);
}

// Things to do after an undo OR redo.
void MainWindow::postUndoActions()
{
    resortAll();      // Resort, since view autosort is disabled for perf reasons
    markModified(!undoMgr().atSavePoint());
    updateActions();
}

void MainWindow::on_action_Undo_triggered()
{
    (void)this; // Preserve as a non-static method for stylistic reasons. This placates clang-tidy.
    undoMgr().undo(); // undo
}

void MainWindow::on_action_Redo_triggered()
{
    (void)this; // Preserve as a non-static method for stylistic reasons. This placates clang-tidy.
    undoMgr().redo(); // redo
}

void MainWindow::on_action_Clear_Undo_Stack_triggered()
{
    undoMgr().clear();
    updateActions();
}
