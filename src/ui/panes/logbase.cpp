/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <functional>
#include <QPlainTextEdit>
#include <QToolButton>

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>

#include "src/core/app.h"
#include "src/core/containermodel.h"

#include "logbase.h"
#include "ui_logbase.h"

LogBase::LogBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    MappedBase(mainWindow, paneClass, parent),
    ui(new Ui::LogBasePane),
    nameColumn(0)
{
    ui->setupUi(this);
    paneFilterBar = ui->logFilterCtrl;

    setupSubsetComboBox();
    setupSignals();
    Util::SetupWhatsThis(this);

    ui->logFilterCtrl->setStretchFactor(0, 80);
    ui->logFilterCtrl->setStretchFactor(1, 20);
}

LogBase::~LogBase()
{
    delete ui;
    ui = nullptr;
}

namespace {
// Contains for set & vector
template <typename C0, typename C1>
bool setEqVec(const C0& c0, const C1& c1)
{
    for (const auto& i : c1)
        if (!c0.contains(i))
            return false;

    return true;
}

template <typename C0, typename C1>
QSet<C0>& assign(QSet<C0>& c0, const C1& c1) {
    c0.clear();
    for (const auto& i : c1)
        c0.insert(i);
    return c0;
}
} // end anonymous namespace

void LogBase::setContainerSet(const QModelIndexList& cs)
{
    // See if we've already got these, in which case do nothing.
    if (containerSet.size() == cs.size() && setEqVec(containerSet, cs))
        return;

    assign(containerSet, cs);
    processModelReset();
}

bool LogBase::hasSelection() const
{
    return ui->logTree->topLevelItemCount() > 0;
}

bool LogBase::isSelected(const QModelIndex &idx) const
{
    const QTreeWidgetItem* parent = indexToParentItem(idx);

    return (containerSet.isEmpty() || containerSet.contains(idx)) &&
            (parent != nullptr) && parent->checkState(nameColumn) == Qt::Checked;
}

QModelIndexList LogBase::getSelections() const
{
    QModelIndexList selected;

    if (containerSet.isEmpty()) {
        for (auto it = logMap.begin(); it != logMap.end(); ++it)
            if (isSelected(it.key()))
                selected.push_back(it.key());
    } else {
        for (const auto& c : containerSet)
            if (isSelected(c))
                selected.push_back(c);
    }

    return selected;
}

bool LogBase::supportsAction(PaneAction pa) const
{
    switch (pa) {
    case PaneAction::ViewAsTree:    return false;
    default:                        return MappedBase::supportsAction(pa);
    }
}

void LogBase::setupSubsetComboBox()
{
    auto* columnSelect = new QStandardItem(tr("All"));
    filterColumnModel.appendRow(columnSelect);

    ContainerModel::setupComboBox<ContainerModel>(*ui->filterColumn, filterColumnModel, nullptr);

    connect(ui->filterColumn, (void (QComboBox::*)(int)) &QComboBox::currentIndexChanged,
                this, &LogBase::setQueryColumnUi);

    setQueryColumnUi(0);
}

void LogBase::setupContextMenus()
{
    paneMenu.addAction(ui->action_AddAllRunning);
    paneMenu.addAction(ui->action_SelectExpanded);
    paneMenu.addAction(ui->action_RemoveUnselected);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &LogBase::customContextMenuRequested, this, &LogBase::showActionContextMenu);
}

void LogBase::setupSignals()
{
    // Filter signals
    connect(ui->filterContainers, &QLineEdit::textChanged, this, &LogBase::setQuery);

    // data change/insertion/deletion signals
    connect(&containerModel, &ContainerModel::dataChanged, this, &LogBase::processDataChanged);
    connect(&containerModel, &ContainerModel::rowsInserted, this, &LogBase::processRowsInserted);
    connect(&containerModel, &ContainerModel::rowsAboutToBeRemoved, this, &LogBase::processRowsAboutToBeRemoved);

    // Log lines spinner
    connect(ui->logLines, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            this, static_cast<void(LogBase::*)(int) const>(&LogBase::setLogDisplayLines));
}

void LogBase::newConfig()
{
    MappedBase::newConfig();

    applyToWidgets<QWidget>(&QWidget::setFont, cfgData().consoleFont);

    setLogDisplayLines(ui->logLines->value());
}

QToolButton *LogBase::createClearButton() const
{
    static const QString tooltipClear =
            "<html><head/><body><p>Clear the scrollback history for all containers.</p></body></html>";

    auto* clearButton = new QToolButton();
    clearButton->setIcon(Icons::get("edit-clear-history"));
    clearButton->setToolTip(tooltipClear);
    clearButton->setWhatsThis(tooltipClear);

    // Clear signal
    connect(clearButton, &QToolButton::pressed, this, &LogBase::clearLogs);

    return clearButton;
}

void LogBase::setQueryColumnUi(int column)
{
    MappedBase::setQueryColumn(column - 1); // -1 for "All" header
    ui->filterColumn->setCurrentIndex(column);
}

void LogBase::setQuery(const QString &pattern)
{
    MappedBase::setQuery(pattern);
    ui->filterContainers->setText(pattern);
}

void LogBase::processModelReset()
{
    // Disable sorting during large updates
    const bool prevSortEnabled = ui->logTree->isSortingEnabled();
    ui->logTree->setSortingEnabled(false);

    MappedBase::processModelReset();

    ui->logTree->setSortingEnabled(prevSortEnabled);
}

// Accept only ones in the container set we are interested in.
bool LogBase::accept(const QModelIndex &idx) const
{
    return MappedBase::accept(idx) &&
            (containerSet.isEmpty() || containerSet.contains(idx));
}

QWidget* LogBase::logWidgetfactory()
{
    // TODO: get data below from config
    auto* pte = new QPlainTextEdit();
    pte->setReadOnly(true);
    pte->setBaseSize(320, 160);
    pte->setMaximumBlockCount(cfgData().consoleHistoryLines);
    pte->setLineWrapMode(QPlainTextEdit::WidgetWidth);
    pte->setFont(cfgData().consoleFont);
    setLogDisplayLines(pte, 8);

    return pte;
}

void LogBase::setTitle(const QString &title)
{
    if (auto* gb = qobject_cast<QGroupBox*>(ui->verticalLayout->parent()))
        gb->setTitle(title);
}

QTreeWidgetItem *LogBase::indexToTreeItem(const QModelIndex &idx) const
{
    if (auto* widget = indexToWidget<QWidget>(idx))
        return reinterpret_cast<QTreeWidgetItem*>(widget->property(widgetItemProperty).toULongLong());

    return nullptr;
}

QTreeWidgetItem *LogBase::indexToParentItem(const QModelIndex &idx) const
{
    if (QTreeWidgetItem* item = indexToTreeItem(idx))
        return item->parent();

    return nullptr;
}

QModelIndex LogBase::widgetToIndex(const QWidget* widget)
{
    return widget->property(indexProperty).toModelIndex();
}

QTreeWidgetItem* LogBase::widgetToItem(const QWidget *widget)
{
    return reinterpret_cast<QTreeWidgetItem*>(widget->property(widgetItemProperty).toULongLong());
}

QTreeWidgetItem* LogBase::widgetToParentItem(const QWidget *widget)
{
    if (QTreeWidgetItem* item = widgetToItem(widget))
        return item->parent();

    return nullptr;
}

void LogBase::appendStr(QPlainTextEdit *pte, const QColor& color, const char *text)
{
    if (text != nullptr && text[0] != '\0') {
        pte->appendHtml("<font color=" + color.name() + "><pre>" + text + "</pre></font>");
    }
}

void LogBase::appendStr(QPlainTextEdit *pte, const QColor& color, const QVector<QByteArray>& output,
                        Out outIndex, const char *prefix)
{
    if (output.size() <= outIndex || output[outIndex].isEmpty())
        return;

    const QByteArray fontBegin = "<font color=" + color.name().toUtf8() + "><pre>";
    prefix = (prefix == nullptr) ? "" : prefix;

    const QByteArray& out = output[outIndex];
    int endOfLine;
    for (int pos = 0; pos < out.size(); pos = endOfLine + 1) {
        endOfLine = out.indexOf('\n', pos);
        if (endOfLine < 0)
            endOfLine = out.size();

        pte->appendHtml(fontBegin + prefix + out.mid(pos, endOfLine - pos) + "</pre></font>");
        prefix = ""; // only insert prefix on first line
    }
}

void LogBase::appendStr(QPlainTextEdit *pte, UiType color, const QVector<QByteArray>& output,
                        Out outIndex, const char *prefix)
{
    appendStr(pte, cfgData().uiColor[color], output, outIndex, prefix);
}

void LogBase::insert(const QModelIndex& idx, int pos)
{
    if (!idx.isValid())
        return;

    auto* headerItem = new QTreeWidgetItem();
    auto* item = new QTreeWidgetItem();
    QWidget* widget = logWidgetfactory();

    if (item == nullptr || widget == nullptr || headerItem == nullptr) {
        delete headerItem;
        delete item;
        delete widget;
        return;
    }

    widget->setProperty(indexProperty, QPersistentModelIndex(idx));
    widget->setProperty(widgetItemProperty, quint64(item));

    if (nameColumn >= 0) {
        headerItem->setData(nameColumn, Qt::DisplayRole,
                            containerModel.data(idx, ContainerModel::Name).toString());

        QFont containerNameFont = headerItem->font(0);
        containerNameFont.setBold(true);
        headerItem->setData(nameColumn, Qt::FontRole, containerNameFont);

        headerItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        headerItem->setCheckState(nameColumn, Qt::Checked);
    }

    if (pos < 0)
        pos = ui->logTree->topLevelItemCount();

    ui->logTree->insertTopLevelItem(pos, headerItem);

    headerItem->addChild(item);
    ui->logTree->setItemWidget(item, 0, widget);

    item->setFirstColumnSpanned(true);

    logMap.insert(idx, widget);
}

bool LogBase::contains(const QModelIndex &idx) const
{
    return logMap.find(idx) != logMap.end();
}

void LogBase::show(const QModelIndex &idx, int /*pos*/)
{
    if (QTreeWidgetItem* parent = indexToParentItem(idx))
        return parent->setExpanded(true);
}

void LogBase::hide(const QModelIndex &idx)
{
    if (QTreeWidgetItem* parent = indexToParentItem(idx))
        return parent->setExpanded(false);
}

bool LogBase::shown(const QModelIndex &idx) const
{
    if (QTreeWidgetItem* parent = indexToParentItem(idx))
        return parent->isExpanded();

    return false;
}

void LogBase::clear()
{
    logMap.clear();
    ui->logTree->clear();
}

void LogBase::remove(const QModelIndex& idx)
{
    if (QTreeWidgetItem* parent = indexToParentItem(idx)) {
        logMap.remove(idx);
        delete parent;
    }
}

void LogBase::setExpanded(bool expanded)
{
    applyToParents(&QTreeWidgetItem::setExpanded, expanded);
}

void LogBase::setLogDisplayLines(QPlainTextEdit* pte, int count)
{
    const int totalHeight = QFontMetricsF(pte->font()).height() * (float(count) + .5);

    pte->setMinimumHeight(totalHeight);
    pte->setMaximumHeight(totalHeight);
}

void LogBase::setLogDisplayLines(int count) const
{
    count = Util::Clamp(count, 2, 32);

    ui->logLines->setValue(count);

    for (auto* widget : logMap) {
        if (auto* pte = dynamic_cast<QPlainTextEdit*>(widget)) {
            setLogDisplayLines(pte, count);
        } else {
            assert(0);
        }
    }

    // The QTreeWidget doesn't appear to re-adjust for cell contents unless something
    // happens to it.  This is simply to force it to relayout.
    {
        const int count = ui->logTree->topLevelItemCount();
        auto* newItem = new QTreeWidgetItem();
        ui->logTree->insertTopLevelItem(count, newItem);
        delete newItem;
    }

    ui->logLines->updateGeometry();
}

void LogBase::showActionContextMenu(const QPoint &pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

void LogBase::on_action_AddAllRunning_triggered()
{
    containerSet.clear();
    processModelReset();
}

void LogBase::on_action_SelectExpanded_triggered()
{
    for (auto& widget : logMap)
        if (QTreeWidgetItem* parent = widgetToParentItem(widget))
            parent->setCheckState(nameColumn, parent->isExpanded() ? Qt::Checked : Qt::Unchecked);
}

void LogBase::on_action_RemoveUnselected_triggered()
{
    containerSet.clear();

    for (auto& widget : logMap)
        if (QTreeWidgetItem* parent = widgetToParentItem(widget))
            if (parent->checkState(nameColumn) == Qt::Checked)
                containerSet.insert(widgetToIndex(widget));

    processModelReset();
}

void LogBase::selectAll()
{
    applyToParents(&QTreeWidgetItem::setCheckState, nameColumn, Qt::Checked);
}

void LogBase::selectNone()
{
    applyToParents(&QTreeWidgetItem::setCheckState, nameColumn, Qt::Unchecked);
}

void LogBase::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    MappedBase::save(settings);

    SL::Save(settings, "sortColumn",      ui->logTree->header()->sortIndicatorSection());
    SL::Save(settings, "sortOrder",       ui->logTree->header()->sortIndicatorOrder());
    SL::Save(settings, "logDisplayLines", ui->logLines->value());
}

void LogBase::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    MappedBase::load(settings);
    auto* header = ui->logTree->header();

    header->setSortIndicator(SL::Load(settings, "sortColumn", 0),
                             SL::Load(settings, "sortOrder", Qt::AscendingOrder));

    setLogDisplayLines(SL::Load(settings, "logDisplayLines", 8));
}

