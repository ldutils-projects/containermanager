/*
    Copyright 2018-2021 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/resources.h>
#include "src/version.h"
#include "src/ui/windows/mainwindow.h"
#include <src/core/app.h>
#include <src/core/builddate.h>
#include <src/util/cmdline.h>

int main(int argc, char *argv[])
{
    App::setApplicationName(Appname);
    App::setApplicationDisplayName(Apptitle);
    App::setApplicationVersion(Version);

    CmdLine opts(Appname, Version, BuildDate, argc, (const char **)argv);
    if (const int rc = opts.processArgs(); rc != 0)
        return Exit::code(rc);

    App app(argc, argv, opts);
    if (const int rc = app.rc(); rc != 0)
        return Exit::code(rc);

    MainWindow mainWindow;
    mainWindow.show();

    return app.exec();
}
