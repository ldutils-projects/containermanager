/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERITEM_H
#define CONTAINERITEM_H

#include <QIcon>

#include <src/core/treeitem.h>
#include <src/core/modelmetadata.h>

class ContainerModel;
class ContainerPrivate;

class ContainerItem : public TreeItem
{
protected:
    explicit ContainerItem(const ItemData& data, TreeItem *parent = nullptr);
    explicit ContainerItem(TreeItem *parent = nullptr);
    ~ContainerItem() override;

    using TreeItem::data;
    QVariant data(ModelType column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;

    ContainerItem *factory(const ContainerItem::ItemData& data, TreeItem* parent) override;

    bool setPrivateData(ContainerPrivate* data);
    ContainerPrivate* privateData() const;

    bool isContainer() const;

private:
    ContainerItem(const ContainerItem&) = delete;
    ContainerItem& operator=(const ContainerItem&) = delete;

    ContainerPrivate* m_privateData;

    friend class ContainerModel;
};

#endif // CONTAINERITEM_H
