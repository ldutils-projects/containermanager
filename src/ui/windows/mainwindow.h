/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <array>
#include <functional>
#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QHeaderView>
#include <QTimer>
#include <QLabel>
#include <QList>
#include <QSystemTrayIcon>
#include <QProgressBar>
#include <QAtomicInteger>

#include "src/core/iconcache.h"
#include <src/ui/windows/mainwindowbase.h>
#include "src/ui/panes/pane.h"
#include "src/ui/windows/appconfig.h"
#include "src/ui/windows/aboutdialog.h"

class ThreadBase;
class CmdLine;
enum class ContainerState;
enum class UiType;

namespace Ui {
class MainWindow;
} // namespace Ui

class MainWindow final : public MainWindowBase
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr, const CmdLine* cmdLineOverride = nullptr);
    ~MainWindow() override;

    QAction*        getControlAction(ContainerControl cc) const;
    QAction*        getPaneAction(PaneAction cc) const override;

    IconCache& iconCache() { return m_iconCache; }
    const IconCache& iconCache() const { return m_iconCache; }

    QWidget* paneFactory(PaneClass_t paneClass = PaneClass_t(-1)) const override;
    PaneBase* paneFactory(PaneClass paneClass) const;
    Pane::Container* containerFactory() const override;

    void setupContainerActionContextMenu(QMenu& contextMenu, bool actionsInSubMenu = false) const;

    void  addBGTask() { ++bgTaskCount; };

    void newConfig(bool newValues) override;

public slots:
    void updateList();
    void updateData(bool force);
    void updateData();
    void updateAll();
    void viewAsTree(bool);
    void updateStatus();
    void updateActions() override; // update action enable/disable state
    void selectionChanged(const QItemSelectionModel* selector,
                          const QItemSelection &selected, const QItemSelection &deselected) override;
    void currentChanged(const QModelIndex &current) override;
    void resetProgress();
    void statusChanged(const QString &) const; // reset status line color
    void startedBGTask(ThreadBase*);
    void finishedBGTask(int rc);

signals:
    void paneSelectionChanged();
    void paneCurrentContainerChanged(const QModelIndex &current);
    void dataAboutToBeUpdated();
    void dataUpdated(); // post-data-update signal

private slots:
    void newFocus(QObject*) override;
    void commitData(QSessionManager& manager);
    void postLoadHook() override;
    void egg();

    void dirtyStateChanged(bool) override;

    void on_action_Containers_Refresh_triggered();
    void on_action_Copy_Selected_triggered();
    void on_action_Create_Container_triggered();
    void on_action_Enlarge_Font_triggered();
    void on_action_ExecuteIn_Container_triggered();
    void on_action_Freeze_Container_triggered();
    void on_action_New_Session_triggered();
    void on_action_Open_Settings_triggered();
    void on_action_Pane_Balance_Siblings_triggered();
    void on_action_Pane_Close_triggered();
    void on_action_Pane_Group_Left_triggered();
    void on_action_Pane_Group_Right_triggered();
    void on_action_Pane_Left_triggered();
    void on_action_Pane_Right_triggered();
    void on_action_Pane_Split_Horizontal_triggered();
    void on_action_Pane_Split_Vertical_triggered();
    void on_action_Quit_without_Save_triggered();
    void on_action_Reboot_Container_triggered();
    void on_action_Remove_Container_triggered();
    void on_action_Reset_Font_triggered();
    void on_action_Revert_triggered();
    void on_action_Save_Settings_As_triggered();
    void on_action_Save_Settings_triggered();
    void on_action_Show_Filters_triggered(bool checked);
    void on_action_Shrink_Font_triggered();
    void on_action_Size_Cols_triggered();
    void on_action_Start_Container_triggered();
    void on_action_Stop_Container_triggered();
    void on_action_Thaw_Container_triggered();
    void on_action_View_Collapse_All_triggered();
    void on_action_View_Expand_All_triggered();
    void on_action_View_Select_All_triggered();
    void on_action_View_Select_None_triggered();
    void on_action_View_Show_All_triggered();
    void on_action_New_Window_triggered();
    void on_action_Donate_triggered();
    void on_action_Undo_triggered();
    void on_action_Redo_triggered();
    void on_action_Clear_Undo_Stack_triggered();

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    template <typename... ARG>
    void containerAction(ContainerControl, const ARG&... args);
    void resetEnvironment();
    void setTimersEnabled(bool enable);
    void setupSession() const;
    void setupDefaultPanels();
    void setupMenus();
    void setupActionTooltips();  // set WhatsThis to match ToolTip
    void setupStatus();
    void setupSystemTray();
    void setupTimers();
    void setupSignals();
    void setupAutosave();
    void postUpdate();
    void updateStatus(bool force);
    void resortAll(); // resort all panes
    void postUndoActions(); // actions to perform after an undo

    // load config data from these settings
    void loadCfgData(QSettings& settings) override { m_appConfig.load(settings); }

    using MainWindowBase::recentSessionsChanged;
    void recentSessionsChanged();

    template <typename P> P* findOrAddPane(PaneClass newPc, const QString& newTabName);

    static QColor colorForState(ContainerState state);

    // Status bar items
    enum class Stat {
        Total,
        Selected,
        Running,
        Stopped,
        Frozen,
        TotalMem,
        _Count,
    };

    // set status bar data
    void setStat(Stat s, const char* txt, const std::function<bool(const QModelIndex&)>& pred,
                 ContainerState state, const QByteArray& fontStyle, const QColor& color);

    void setStat(Stat s, const char* txt, const std::function<bool(const QModelIndex&)>& pred,
                 ContainerState state, const QByteArray& fontStyle = "normal");

    void      setupActionIcons();

    QModelIndexList selectedContainers(PaneBase* focus = nullptr) const;
    QModelIndexList selectedContainersWarn(PaneBase* focus = nullptr) const;

    void updateProgress(int done, int total, int failed);

    // helper to call static initializers before other object construction
    struct InitStatic {
        InitStatic(MainWindow* mainWindow);
    };

    Ui::MainWindow*        ui;
    IconCache              m_iconCache;

    // must be initialized first
    InitStatic             initStatic;

    // Dialog boxes
    AppConfig              m_appConfig;
    AboutDialog            m_aboutDialog;

    QTimer                 autosaveTimer;
    QTimer                 listUpdateTimer;
    QTimer                 dataUpdateTimer;
    QTimer                 bgTaskTimer;
    QTimer                 eggTimer;

    QSystemTrayIcon        systemTrayIcon;

    QAtomicInteger<int>    bgTaskCount;        // number of launched BG tasks
    QAtomicInteger<int>    bgTaskDone;         // number of BG tasks completed
    QAtomicInteger<int>    bgTaskFailed;       // number of BG tasks which failed

    std::array<QLabel, int(Stat::_Count)> statusText;
};

#endif // MAINWINDOW_H
