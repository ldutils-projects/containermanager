/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERPANE_H
#define CONTAINERPANE_H

#include <QGroupBox>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QString>
#include <QTimer>

#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/widgets/usecdelegate.h"
#include "src/ui/widgets/sizedelegate.h"
#include "src/ui/widgets/cpusharesdelegate.h"
#include "datacolumnpane.h"

#include "pane.h"

namespace Ui {
class ContainerPane;
} // namespace Ui

class QStandardItem;
class MainWindow;
class CfgData;

class ContainerPane final : public DataColumnPane
{
    Q_OBJECT

public:
    explicit ContainerPane(MainWindow& mainWindow, QWidget *parent = nullptr);
    ~ContainerPane() override;

private slots:
    void showActionContextMenu(const QPoint&);
    void on_ContainerPane_toggled(bool checked) { paneToggled(checked); }
    void filterTextChanged(const QString& regex) override;

signals:
    void currentChanged(const QModelIndex &current);

private:
    void setupContainerView();   // configure main container tree view
    void setupFilterColumn();    // filter column combobox
    void setupDelegates();       // editing delegates
    void setupSignals();         // setup signals
    void setupContextMenus();    // build context menus
    void setupFilters();
    const DefColumns& defColumnView() const override;

    Ui::ContainerPane*    ui;

    USecDelegate          uSecDelegate;      // edit uSec times
    SizeDelegate          sizeDelegate;      // edit size limits
    CpuSharesDelegate     cpuSharesDelegate; // edit CPU shares

    QTimer                typingIdle;
};

#endif // CONTAINERPANE_H
