/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <functional>
#include <chrono>

#include <QApplication>
#include <QLegendMarker>

#include <src/util/roles.h>
#include "src/util/util.h"
#include "src/core/containermodel.h"
#include "src/core/containerstate.h"
#include "src/ui/windows/mainwindow.h"
#include "piechartpane.h"
#include "ui_chartbase.h"

PieChartPane::PieChartPane(MainWindow& mainWindow, QWidget* parent) :
    ChartBaseData(mainWindow, PaneClass::PieChart, parent),
    pieSeries(new QPieSeries()),
    hoverOverTimer(this),
    labelsAlwaysVisible(false)
{
    setupChart();
    setupTimer();
    setupSignals();
    setupContextMenus();
    processModelReset();   // seed initial model data

    setChartData(ContainerModel::Memory); // default
}

PieChartPane::~PieChartPane()
{
    // The view has taken ownership of the chart, so we don't delete it
    pieSeries->disconnect(); // stop sending signals during destruction
    deleteUI(ui);
}

void PieChartPane::expandAll()
{
    labelsAlwaysVisible = true;
    pieSeries->setLabelsVisible(labelsAlwaysVisible);
}

void PieChartPane::collapseAll()
{
    labelsAlwaysVisible = false;
    pieSeries->setLabelsVisible(labelsAlwaysVisible);
}

void PieChartPane::resizeToFit(int /*defer*/)
{
    zoom(zoomLevel = initZoom);
    chart->setMargins(QMargins(2,2,2,2));
}

void PieChartPane::hoverData(Data *data, bool state)
{
    if (ui == nullptr) // don't do when shutting down
        return;

    using namespace std::chrono_literals;

    if (state) {
        hoverOverTimer.stop();
    } else {
        hoverOverTimer.start(500ms);
    }

    data->setExploded(state || isSelected(data));
    data->setLabelVisible(state || drawLabel(data));

    // Send signal that current container changed
    if (state)
        emit paneCurrentContainerChanged(dataToIndex(data));

    // Update markers
    setLabelColor(data, state);
}

void PieChartPane::displayAllLabels()
{
    if (labelsAlwaysVisible)
        pieSeries->setLabelsVisible(true);
}

void PieChartPane::zoom(float zoom)
{
    pieSeries->setHoleSize(holeSize * zoom);
    pieSeries->setPieSize(pieSize * zoom);
}

void PieChartPane::setupChart()
{    
    if (chart == nullptr || pieSeries == nullptr)
        return;

    //    chartView.setRubberBand(ChartViewZoom::RectangleRubberBand);
    //    chartView.setRubberBandSelectionMode(Qt::IntersectsItemShape);

    chart->setAnimationOptions(QChart::AllAnimations);
    chart->setAnimationDuration(1000);
    chart->addSeries(pieSeries); // takes ownership of series

    chart->legend()->setVisible(true);
    chart->legend()->setBackgroundVisible(false);
    chart->legend()->setAlignment(Qt::AlignLeft);
    chart->legend()->setLabelColor(labelNormal);
    chart->legend()->setContentsMargins(0,0,0,0);

    resizeToFit();
}

void PieChartPane::setupTimer()
{
    hoverOverTimer.setSingleShot(true);
    connect(&hoverOverTimer, &QTimer::timeout, this, &PieChartPane::displayAllLabels);
}

void PieChartPane::setupSignals()
{
    // let main window know about selection changes
    connect(this, &PieChartPane::dataSelectionChanged, &mainWindow(), &MainWindow::selectionChanged);
    connect(this, &PieChartPane::paneCurrentContainerChanged, &mainWindow(), &MainWindow::currentChanged);

    // mouse tracking
    connect(pieSeries, &QPieSeries::hovered, this, &PieChartPane::hoverData);
    connect(pieSeries, &QPieSeries::clicked, this, &PieChartPane::selectData);
}

void PieChartPane::setupContextMenus()
{
    // Must be here, not in our base class, because it's called from the constructor and uses virtual
    // methods.    
    mainWindow().setupContainerActionContextMenu(paneMenu);
    ChartBaseData::setupContextMenus();
}

// Interpolate color range across chart elements
void PieChartPane::updateChart()
{
    if (ui == nullptr || chart == nullptr || pieSeries == nullptr)
        return;

    const auto entries = pieSeries->slices();
    
    // We must do this after updating ALL the data values, so the angles are calculated properly.
    const auto markers = chart->legend()->markers(pieSeries);
    int marker = 0;

    for (const auto& data : entries) {
        const QModelIndex idx = dataToIndex(data);

        if (!idx.isValid())
            continue;

        // color
        const QColor color = dataColor(idx);
        data->setColor(color);

        updateMarker(data, markers, marker++, color, data->label()); // (must be before setLabelColor)

        setLabelColor(data, false);  // must be after setting dataProperty above.
        updateDataValue(data, idx);
    }

    for (const auto& data : entries)
        data->setLabelVisible(drawLabel(data));
}

void PieChartPane::insert(const QModelIndex& idx, int pos)
{
    Data* data = updateDataValue(new Data(), idx);

    if (data == nullptr)
        return;

    ChartBaseData::insert(idx, data);

    QFont labelFont = QApplication::font();
    labelFont.setPointSizeF(labelFont.pointSizeF() * 0.8);

    if (pos == -1)
        pos = pieSeries->count();

    pieSeries->insert(pos, data);

    data->setProperty(indexProperty, idx);
    data->setProperty(nameProperty, containerModel.data(idx, ContainerModel::Name));

    data->setLabelArmLengthFactor(armLength);
    data->setLabelPosition(Data::LabelOutside);
    data->setLabelFont(labelFont);
    data->setExplodeDistanceFactor(explodedDistance);

    createMarker(pos, data);
}

void PieChartPane::clear()
{
    if (pieSeries == nullptr)
        return;

    ChartBaseData::clear();
    pieSeries->clear();
}

void PieChartPane::remove(Data* data)
{
    if (pieSeries == nullptr || data == nullptr)
        return;

    ChartBaseData::remove(data);
    pieSeries->remove(data);
}

bool PieChartPane::drawLabel(const Data* data) const
{
    return isSelected(data) ||
            (labelsAlwaysVisible &&
             (data->angleSpan() > labelMinAngleSpan) &&
             (pieSeries->count() <= labelMaxCount));
}

PieChartPane::Data* PieChartPane::updateDataValue(Data* data, QModelIndex idx) const
{
    if (!idx.isValid())
        idx = dataToIndex(data);

    if (data == nullptr)
        data = indexToData(idx);

    if (data == nullptr || !idx.isValid())
        return data;

    const double value = containerModel.data(plotColumn, idx, Util::PlotRole).toDouble();
    data->setValue(std::max(value, 0.001)); // avoid zeros

    QString valueAsString;
    data->setLabel(label(data, idx, false, valueAsString));
    data->setLabelVisible(drawLabel(data));

    // Update legend marker
    if (QLegendMarker* marker = dataToLegend(data))
        marker->setLabel(label(data, idx, true, valueAsString));

    return data;
}

void PieChartPane::setSelected(Data* data, bool select)
{
    if (data == nullptr)
        return;

    ChartBaseData::setSelected(data, select);

    data->setExploded(select);
    data->setLabelVisible(drawLabel(data));
}

void PieChartPane::selectData(Data* data)
{
    ChartBaseData::selectData(data, [this]() { return pieSeries->slices(); });
}

void PieChartPane::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    ChartBaseData::save(settings);

    MemberSave(settings, labelsAlwaysVisible);
}

void PieChartPane::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    ChartBaseData::load(settings);

    MemberLoad(settings, labelsAlwaysVisible);
}
