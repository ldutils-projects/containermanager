/*
    Copyright 2018-2023 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LINECHARTBASE_H
#define LINECHARTBASE_H

#include <cassert>
#include <array>

#include <QTimer>
#include <QDateTime>
#include <QLegendMarker>
#include <QtCharts/QValueAxis>
#include <QtCharts/QDateTimeAxis>
#include <QSignalBlocker>
#include <QPoint>

#include <src/util/reverseadapter.h>
#include <src/util/roles.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "chartbase.h"

class MainWindow;

template <typename LOWERDATA, typename DATA>
class LineChartBase : public ChartBaseData<DATA>
{
public:
    LineChartBase(MainWindow& mainWindow, QWidget* parent) :
        ChartBaseData<DATA>(mainWindow, PaneClass::LineChart, parent),
        displayedDuration(cfgData().defaultGraphSeconds() * 1000),  // S to mS
        lastUpdate(QDateTime::currentMSecsSinceEpoch()),
        xAxis(new QDateTimeAxis(this->chart)),
        yAxis(new QValueAxis(this->chart)),
        axisUpdateTimer(this)
    {
        setupContextMenus();
        setupChart();
        setupZoom();
        forceScaleUpdate = true;
    }

    using ChartBaseData<DATA>::ChartBaseData;

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

protected:
    void    newConfig() override;
    void    resizeToFit(int defer = -1) override;
    void    remove(DATA* data) override;
    void    insert(const QModelIndex& idx, int pos = -1) override;
    DATA*   insertImpl(const QModelIndex& idx, int pos = -1);
    virtual void setStacked(bool stacked);
    void    setupChart();
    void    findMaxDisplayedY(bool force);
    qint64  oldestDisplayedTime() const { return lastUpdate - displayedDuration + startOffset; }
    virtual LOWERDATA* lower(DATA* data) const = 0;
    virtual LOWERDATA* lower(QAbstractSeries* data) const = 0;
    void    show(const QModelIndex& idx, int pos = -1) override;
    void    hide(const QModelIndex& idx) override;
    bool    shown(const QModelIndex& idx) const override;
    void    clear() override;
    void    clearHistory() override;
    void    setIdxVisible(const QModelIndex& idx, bool visible);
    void    updateAxes();
    void    updateYAxis();
    void    updateXAxis();
    double  convertScale(double oldYScale, double newYScale);
    void    cleanup(); // remove excess data
    void    zoom(float zoom) override;
    void    setupZoom();
    virtual void dataUpdated();
    DATA*   newDataValue(DATA*, QModelIndex idx, double& base);
    DATA*   updateDataValue(DATA*, QModelIndex idx) const;
    void    updateDataValue(QModelIndex idx) const override { updateDataValue(nullptr, idx); }
    void    updateChart() override;
    bool    supportsAction(PaneAction pa) const override;
    virtual void recalcAxes(); // force recalc after add/remove of containers
    virtual void visibilityChange();
    virtual void pan(QMouseEvent* event, const QPoint& rel);
    virtual void endPan();
    virtual DATA* dataFactory() const { return new DATA(); }
    virtual bool isStacked() const { return false; }

    struct SignalBlocker {
        SignalBlocker(LineChartBase* lcb, const QList<QAbstractSeries*>& series) :
            lcb(lcb), series(series)
        {
            for (auto* it : series)
                blocked.push_back(lcb->lower(static_cast<DATA*>(it))->blockSignals(true));
        }

        ~SignalBlocker() {
            for (auto* it : series)
                lcb->lower(static_cast<DATA*>(it))->blockSignals(blocked.takeFirst());
        }

    private:
        QVector<bool>       blocked;
        LineChartBase*      lcb;
        const QList<QAbstractSeries*>& series;
    };

    friend SignalBlocker;

    qint64                   displayedDuration;        // in mS
    qint64                   startOffset = 0;          // time offset, for dragging
    qint64                   lastUpdate;               // last update time

    double                   yScale = 1.0;             // Y axis scale
    double                   maxDisplayedY = 0.01;     // max displayed Y value
    bool                     forceScaleUpdate = false; // true to force a scale update

    QDateTimeAxis*           xAxis;                    // X axis
    QValueAxis*              yAxis;                    // Y axis

    QVector<double>          maxAtIndex;               // max value displayed at some index
    QTimer                   axisUpdateTimer;          // refresh axis after chart updates

    static const constexpr char* visProperty = "lc-visible";

private:
    bool    canHide() const override { return true; }
    QString titleSuffix() const override { return QObject::tr(" History"); }
    void    setupContextMenus();
};

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::setupChart()
{
    if (this->chart == nullptr)
        return;

    // this-> is necessary below because we're a template class accessing instance data from a
    // parent.

    this->chart->setAnimationOptions(QChart::NoAnimation);
    this->chart->addAxis(xAxis, Qt::AlignBottom);
    this->chart->addAxis(yAxis, Qt::AlignLeft);

    this->chart->legend()->setBackgroundVisible(false);
    this->chart->legend()->setAlignment(Qt::AlignLeft);
    this->chart->legend()->setLabelColor(this->labelNormal);
    this->chart->legend()->setContentsMargins(0,0,0,0);

    yAxis->setRange(0, maxDisplayedY);
    yAxis->setGridLinePen(this->majorGridPen);
    yAxis->setMinorGridLinePen(this->minorGridPen);
    yAxis->setLinePen(this->majorGridPen);
    yAxis->setLabelsColor(this->labelNormal);
    yAxis->setTickCount(5);
    yAxis->setMinorTickCount(4);

    xAxis->setRange(QDateTime::fromMSecsSinceEpoch(lastUpdate),
                    QDateTime::fromMSecsSinceEpoch(lastUpdate + displayedDuration));
    xAxis->setLabelsVisible(true);
    xAxis->setFormat("hh:mm:ss");  // if we let the locale do this, we get dates mixed in
    xAxis->setGridLinePen(this->majorGridPen);
    xAxis->setLinePen(this->majorGridPen);
    xAxis->setLabelsColor(this->labelNormal);
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::setupZoom()
{
    static const qint64 shortestDisplayedTime = 15 * 1000;           // 15 seconds
    static const qint64 longestDisplayedTime  = 24 * 60 * 60 * 1000; // 24 hours

    // zoom parameters for line chart (override from ChartBase)
    this->initZoom = cfgData().defaultGraphSeconds() * 1000;

    this->zoomLevel = this->initZoom;

    this->minZoom  = longestDisplayedTime;
    this->maxZoom  = shortestDisplayedTime;
    this->zoomStep = 1.0 / this->zoomStep;
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::setupContextMenus()
{
    // Must be here, not in our base class, because it's called from the constructor and uses virtual
    // methods.
    this->mainWindow().setupContainerActionContextMenu(this->paneMenu);
    ChartBaseData<DATA>::setupContextMenus();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::show(const QModelIndex& idx, int /*pos*/)
{
    // Defer setting the series as shown/hidden
    if (DATA* data = this->indexToData(idx))
        data->setProperty(visProperty, true);
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::hide(const QModelIndex& idx)
{
    // Defer setting the series as shown/hidden
    if (DATA* data = this->indexToData(idx))
        data->setProperty(visProperty, false);
}

template <typename LOWERDATA, typename DATA>
bool LineChartBase<LOWERDATA, DATA>::shown(const QModelIndex& idx) const
{
    if (DATA* data = this->indexToData(idx))
        return data->isVisible();

    return false;
}

// Clear history, but not container references
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::clearHistory()
{
    for (const auto& data : this->dataMap)
        lower(data)->clear();

    maxAtIndex.clear();

    updateAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::clear()
{
    if (this->chart == nullptr)
        return;

    clearHistory();
    ChartBaseData<DATA>::clear();
    this->chart->removeAllSeries();  // force a repop from model

    updateAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::setIdxVisible(const QModelIndex& idx, bool visible)
{
    if (DATA* data = this->indexToData(idx)) {
        data->setVisible(visible);
        this->dataToLegend(data)->setVisible(visible);
    }
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::zoom(float zoom)
{
    displayedDuration = int64_t(zoom);  // in mS
    updateAxes();
}


// Remove excess data points.  This would be better as a rolling buffer, but that's not
// how the Qt QListSeries seems to work, so we use removePoints() every so often.
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::cleanup()
{
    const int keepDataPoints  = std::max(cfgData().graphDataPoints, 64);
    const int excessThreshold = std::max(int(keepDataPoints * 0.15), 8);

    if (maxAtIndex.size() < (keepDataPoints + excessThreshold))
        return;

    for (const auto& data : this->dataMap) {
        auto* lowerData = lower(data);
        assert(lowerData->count() == maxAtIndex.size());
        lowerData->removePoints(0, lowerData->count() - keepDataPoints);
    }

    // This could be done more efficiently, if slightly less conveniently, with a rolling buffer
    maxAtIndex.remove(0, maxAtIndex.size() - keepDataPoints);
}


// Alas, the QValueAxis isn't capable of scaling, and neither can it be easily
// subclassed to do so, and neither can a scale be applied to a QLineSeries, so
// we fall back on scaling the data we shovel into the series.  This causes some
// awkward rescalings: hence this function.
// TODO: Find a better way!
template <typename LOWERDATA, typename DATA>
double LineChartBase<LOWERDATA, DATA>::convertScale(double oldYScale, double newYScale)
{
    const double rescale = newYScale / oldYScale;

    for (const auto& upperData : this->dataMap) {        
        auto* data = lower(upperData);
        QSignalBlocker block(data);

        for (int i = 0; i < data->count(); ++i)
            data->replace(i, data->at(i).x(), data->at(i).y() * rescale);
    }

    for (auto& maxVal : maxAtIndex)
        maxVal *= rescale;

    maxDisplayedY *= rescale;

    return newYScale;
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::updateAxes()
{
    updateXAxis(); // do first: this calculates max displayed Y
    updateYAxis(); // depends on X axis calculations
}

// Time based axis: use the QDateTimeAxis thing, which is alas not very capable.
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::updateXAxis()
{
    if (xAxis == nullptr)
        return;

    const int64_t oldest = oldestDisplayedTime();

    xAxis->setRange(QDateTime::fromMSecsSinceEpoch(oldest),
                    QDateTime::fromMSecsSinceEpoch(oldest + displayedDuration));

    findMaxDisplayedY(false);
}

// Q axis: attempt a human-friendly display.
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::updateYAxis()
{
    if (yAxis == nullptr)
        return;

    const double maxScaledY = maxDisplayedY;

    const Units& units = cfgData().units(this->plotColumn, true);
    const double newYScale = 1.0 / units.multiplier(maxDisplayedY / yScale);
    const bool   rescale = newYScale != yScale || forceScaleUpdate;

    if (rescale || maxScaledY > yAxis->max() || maxScaledY < yAxis->max() * 0.50) {
        if (rescale)
            yScale = convertScale(yScale, newYScale);

        forceScaleUpdate = false;

        // Awkward: the label format doesn't accept the same QString arg() formats as other
        // things, so we kludge-convert at least the precision.  Also, we can't easily display
        // nice time formatting, so we use single-unit timescales.
        std::array<char, 64> numFmt;
        sprintf(numFmt.data(), "%%.%df ", units.precision());

        yAxis->setLabelFormat(numFmt.data() + units.suffix(maxDisplayedY / yScale) +
                              ContainerModel::mdRateSuffix(this->plotColumn));

        yAxis->setMax(maxDisplayedY);
        yAxis->applyNiceNumbers();
    }
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::visibilityChange()
{
    for (auto it = this->dataMap.begin(); it != this->dataMap.end(); ++it)
        setIdxVisible(it.key(), (*it)->property(visProperty).toBool());
}

// Set Y axis appropriately for contained data.  This is a little expensive, so
// don't do it too often.
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::findMaxDisplayedY(bool force)
{
    if (this->dataMap.empty() || maxAtIndex.empty())
        return;

    // In force mode, recalculate the maxAtIndex values too (expensive)
    if (force) {
        const auto entries = this->chart->series();

        bool visChange = false;
        for (const auto& data : entries) {
            const bool wasVisible = lower(data)->isVisible();
            const bool nowVisible = lower(data)->property(visProperty).toBool();

            visChange = visChange || (wasVisible != nowVisible);
        }

        if (visChange && isStacked()) {
            QVector<bool> wasVisible;
            QVector<bool> nowVisible;

            wasVisible.reserve(entries.size());
            nowVisible.reserve(entries.size());

            for (int s = 0; s < entries.size(); ++s) {
                wasVisible.push_back(entries[s]->isVisible());
                nowVisible.push_back(entries[s]->property(visProperty).toBool());
            }

            // block signals during replace() below, or Qt gets very, very slow
            const SignalBlocker block(this, entries);

            const int count = entries.empty() ? 0 : maxAtIndex.count();
            for (int i = 0; i < count; ++i) {
                double increment = 0.0;  // total add/removed increment
                double prevBase  = 0.0;  // pre-vis-change bases
                for (int s = entries.size() - 1; s >= 0; --s) {
                    auto* data = lower(entries[s]);
                    const double oldY = data->at(i).y();

                    if (wasVisible[s] && !nowVisible[s]) {
                        data->replace(i, data->at(i).x(), oldY - prevBase);
                        increment -= (oldY - prevBase);
                    }

                    if (!wasVisible[s] && nowVisible[s]) {
                        data->replace(i, data->at(i).x(), oldY + prevBase + increment);
                        increment += oldY;
                    }

                    if (wasVisible[s] && nowVisible[s])
                        data->replace(i, data->at(i).x(), data->at(i).y() + increment);

                    if (wasVisible[s])
                        prevBase = oldY;
                }
            }
        }

        // Update visibility
        if (visChange)
            visibilityChange();

        // Update and max indices
        maxAtIndex.fill(0.0);
        for (const auto& data : entries) {
            if (!data->isVisible())
                continue;

            const auto lowerData = lower(data);
            for (int i = 0; i < lowerData->count(); ++i)
                maxAtIndex[i] = std::max(maxAtIndex[i], lowerData->at(i).y());

            if (isStacked())  // in stacked mode, we only have to look at the top data
                break;
        }
    }

    // Find maximum value that may still be displayed
    const qint64 oldest = oldestDisplayedTime();

    const auto* data = lower(*this->dataMap.begin());
    assert(maxAtIndex.size() == data->count());

    // We attempt to go one spot past the oldest, to avoid stepping on any line from a
    // previous entry.  Hence, check exit after the max, not before.
    maxDisplayedY = 0.01 * yScale;  // avoid 0-0 ranges

    for (int i = data->count() - 1; i >= 0; --i) {
        maxDisplayedY = std::max(maxDisplayedY, maxAtIndex[i]);
        if (data->at(i).x() < oldest)
            break;
    }
}

// Change stacked type
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::setStacked(bool stacked)
{
    if (stacked == isStacked())  // nothing to do
        return;

    auto entries = this->chart->series();
    const int count = entries.empty() ? 0 : maxAtIndex.count();

    const SignalBlocker block(this, entries);

    for (int i = 0; i < count; ++i) {
        double base = 0.0;
        for (auto* d : Util::reverse_adapter(entries)) {
            auto* data = lower(d);

            if (data != nullptr && data->isVisible()) {
                double prevY = data->at(i).y();
                data->replace(i, data->at(i).x(), prevY + base * (stacked ? 1 : -1));
                base = stacked ? data->at(i).y() : prevY;
            }
        }
    }

    // Update max-Y values
    findMaxDisplayedY(true);
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::remove(DATA* data)
{
    if (this->chart == nullptr || data == nullptr)
        return;

    ChartBaseData<DATA>::remove(data);

    this->chart->removeSeries(data); // releases ownership back to us, unlike pie slices in a QPieSeries
    data->deleteLater();

    if (this->chart->series().empty())
        maxAtIndex.clear();
}

// Update values for all charted containers after master data update
template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::dataUpdated()
{
    lastUpdate = QDateTime::currentMSecsSinceEpoch();

    double base = 0.0;
    const auto entries = this->chart->series();

    // We process this in reverse order due to the way QAreaSeries draws,
    // and our lack of control over the order of adding series into charts.
    // When using the next series down as the lower series, this way we avoid
    // overwriting with the wrong color.
    for (auto* d : Util::reverse_adapter(entries)) {
        if (auto* data = static_cast<DATA*>(d); data != nullptr)
            newDataValue(data, this->dataToIndex(data), base);
    }

    cleanup();
    updateAxes();
}

// Update single container data value
template <typename LOWERDATA, typename DATA>
DATA* LineChartBase<LOWERDATA, DATA>::newDataValue(DATA* data, QModelIndex idx, double& base)
{
    if (!idx.isValid())
        idx = this->dataToIndex(data);

    if (data == nullptr)
        data = this->indexToData(idx);

    if (data == nullptr || !idx.isValid())
        return data;

    const double modelValue = yScale * this->containerModel.data(this->plotColumn, idx, Util::PlotRole).toDouble();
    const double stackedValue = modelValue + base;

    auto* lowerData = lower(data);

    // If visible, add stacked value.  Otherwise, model value.
    lowerData->append(lastUpdate, data->isVisible() ? stackedValue : modelValue);

    // Track max value per index, across all data sets
    if (maxAtIndex.size() < lowerData->count()) {
        maxAtIndex.push_back(data->isVisible() ? stackedValue : 0.0);
    } else {
        if (data->isVisible())
            maxAtIndex.back() = std::max(maxAtIndex.back(), stackedValue);
    }

    if (data->isVisible()) {
        maxDisplayedY = std::max(stackedValue, maxDisplayedY);

        if (isStacked())
            base = stackedValue;
    }

    return data;
}

template <typename LOWERDATA, typename DATA>
DATA* LineChartBase<LOWERDATA, DATA>::updateDataValue(DATA* data, QModelIndex idx) const
{
    if (!idx.isValid())
        idx = this->dataToIndex(data);

    if (data == nullptr)
        data = this->indexToData(idx);

    if (data == nullptr || !idx.isValid())
        return data;

    QString valueAsString;
    data->setName(this->label(data, idx, false, valueAsString));

    // Update legend marker
    if (QLegendMarker* marker = this->dataToLegend(data))
        marker->setLabel(this->label(data, idx, true, valueAsString));

    return data;
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::updateChart()
{
    if (this->ui == nullptr || this->chart == nullptr)
        return;

    const auto entries = this->chart->series();

    auto markers = this->chart->legend()->markers();
    int marker = 0;

    for (auto& entry : entries) {
        if (!entry->isVisible())
            continue;

        auto* data = dynamic_cast<DATA*>(entry);
        const QModelIndex idx = this->dataToIndex(data);

        if (data == nullptr || !idx.isValid())
            continue;

        // Color
        QPen pen = this->dataColor(idx);
        pen.setWidth(cfgData().graphLineWidth);
        data->setPen(pen);
        data->setPointsVisible(false);

        this->updateMarker(data, markers, marker++, pen.color());  // Legend marker
        updateDataValue(data, idx);
    }

    static const int axisTimerInterval = 200;
    axisUpdateTimer.start(axisTimerInterval);  // recalc axes, but not too often
}

template <typename LOWERDATA, typename DATA>
DATA* LineChartBase<LOWERDATA, DATA>::insertImpl(const QModelIndex& idx, int pos)
{
    auto* data = updateDataValue(dataFactory(), idx);

    if (data == nullptr)
        return nullptr;

    ChartBaseData<DATA>::insert(idx, data);

    auto* lowerData = lower(data);

    // Populate it as far as the graph goes so far
    const auto entries = this->chart->series();

    if (pos < 0)
        pos = entries.size();

    // If other containers have data, fill in zeros for this one up until the present moment.
    if (entries.size() > 0) {
        if (auto* first = lower(entries[0])) {
            while (lowerData->count() < first->count())
                lowerData->append(first->at(lowerData->count()).x(), 0.0);
        }
    }

    // The QChart gives us no choices about insertion order: addSeries is the only way in,
    // and it appends.
    this->chart->addSeries(data);

    data->setProperty(this->indexProperty, idx);
    data->setProperty(this->nameProperty, this->containerModel.data(idx, ContainerModel::Name));
    data->setProperty(visProperty, true);

    data->attachAxis(xAxis);
    data->attachAxis(yAxis);

    this->createMarker(pos, data);

    return data;
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::insert(const QModelIndex& idx, int pos)
{
    insertImpl(idx, pos);
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::newConfig()
{
    ChartBaseData<DATA>::newConfig();

    displayedDuration = cfgData().defaultGraphSeconds() * 1000;

    cleanup();
    updateAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::resizeToFit(int /*defer*/)
{
    displayedDuration = this->zoomLevel = this->initZoom;
    startOffset = 0;
    recalcAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::recalcAxes()
{
    findMaxDisplayedY(true);

    // Hide or show newly changed entries:

    // Redo this no matter what happened
    if (yAxis != nullptr) {
        yAxis->setMax(maxDisplayedY);
        yAxis->applyNiceNumbers();
    }

    updateAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::pan(QMouseEvent* event, const QPoint& rel)
{
    // Only left click drag
    if ((event->buttons() & Qt::LeftButton) == 0)
        return;

    startOffset += -rel.x() * (displayedDuration / std::max(this->chartView.width(), 1));

    updateAxes();
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::endPan()
{
    startOffset = 0;
    updateAxes();
}

template <typename LOWERDATA, typename DATA>
bool LineChartBase<LOWERDATA, DATA>::supportsAction(PaneAction pa) const
{
    switch (pa) {
    case PaneAction::ExpandAll:
    case PaneAction::CollapseAll:   return false;
    default:                        return ChartBaseData<DATA>::supportsAction(pa);
    }
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::save(QSettings& settings) const
{
    if (this->ui == nullptr)
        return;

    ChartBaseData<DATA>::save(settings);

    MemberSave(settings, displayedDuration);
}

template <typename LOWERDATA, typename DATA>
void LineChartBase<LOWERDATA, DATA>::load(QSettings& settings)
{
    if (this->ui == nullptr)
        return;

    ChartBaseData<DATA>::load(settings);

    MemberLoad(settings, displayedDuration);

    forceScaleUpdate = true;
}

#endif // LINECHARTBASE_H
