/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERDETAILSFILTER_H
#define CONTAINERDETAILSFILTER_H

#include <src/ui/filters/detailfilter.h>
#include "src/core/containermodel.h"

class ContainerDetailsFilter  : public DetailFilter
{
public:
    ContainerDetailsFilter(QObject *parent = nullptr);

    using DetailFilter::dataRow;

private:
    enum {
        _First  = ContainerModel::_Count,
        Machine = _First,
        System,
        Container,
        Uptime,
        Network,
        NetBytes,
        NetSpeed,
        CGroup,
        Memory,
        Disk,
        _Count,
    };

    QString text(DetailFilter::Header) const override;

    static const Line rootItem; // structure
};

#endif // CONTAINERDETAILSFILTER_H
