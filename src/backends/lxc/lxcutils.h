/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LXCUTILS_H
#define LXCUTILS_H

#include <cassert>
#include <cstdio>
#include <cinttypes>
#include <QFile>
#include <QByteArray>
#include <QStringList>

#include <src/util/units.h>
#include "src/util/util.h"

class LxcUtils {
public:
    static const char* configLineValue(const char* cfgLine);

    template <typename T> static T readConfig(const QByteArray&, const QByteArray&) {
        assert(0);
    }
};

template <>
inline QByteArray LxcUtils::readConfig(const QByteArray& file, const QByteArray& key)
{
    QFile fp(Util::ExpandDirName(file));
    if (!fp.open(QIODevice::ReadOnly | QIODevice::Text))
        return { };

    while (!fp.atEnd()) {
        const QByteArray line = fp.readLine().trimmed();

        if (line.startsWith(key) && bool(isspace(line[key.size()])))
             return LxcUtils::configLineValue(line.data());
    }

    return { };
}

template <>
inline QStringList LxcUtils::readConfig(const QByteArray& file, const QByteArray& key)
{
    QStringList list;

    QFile fp(Util::ExpandDirName(file));
    if (!fp.open(QIODevice::ReadOnly | QIODevice::Text))
        return list;

    while (!fp.atEnd()) {
        const QByteArray line = fp.readLine().trimmed();

        if (line.startsWith(key) && bool(isspace(line[key.size()])))
            list.append(LxcUtils::configLineValue(line.data()));
    }

    return list;
}

// Read a config value with possible K/M/G/T suffix
template <>
inline uint64_t LxcUtils::readConfig(const QByteArray& file, const QByteArray& key)
{
    const QByteArray line = readConfig<QByteArray>(file, key);

    if (line.isEmpty())
        return uint64_t(0);

    uint64_t size;
    if (sscanf(line.data(), "%" SCNu64 " K ", &size) == 1)
        return size * 1_KiB;
    if (sscanf(line.data(), "%" SCNu64 " M ", &size) == 1)
        return size * 1_MiB;
    if (sscanf(line.data(), "%" SCNu64 " G ", &size) == 1)
        return size * 1_GiB;
    if (sscanf(line.data(), "%" SCNu64 " T ", &size) == 1)
        return size * 1_TiB;
    if (sscanf(line.data(), "%" SCNu64 " ", &size) == 1)
        return size;

    return 0;
}

#endif // LXCUTILS_H
