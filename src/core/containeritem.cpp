/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStyle>
#include <QApplication>
#include <QThread>  // for idealThreadCount

#include <src/util/icons.h>
#include <src/util/roles.h>
#include <src/util/units.h>
#include "src/core/app.h"
#include "containeritem.h"
#include "containermodel.h"
#include "containerclass.h"
#include "containerprivate.h"
#include "containerstate.h"

ContainerItem::ContainerItem(const TreeItem::ItemData &data, TreeItem *parent) :
    TreeItem(data, parent),
    m_privateData(nullptr)
{
}

ContainerItem::ContainerItem(TreeItem *parent) :
    TreeItem(parent),
    m_privateData(nullptr)
{
}

ContainerItem::~ContainerItem()
{
    delete m_privateData;
}

bool ContainerItem::isContainer() const
{
    return (privateData() != nullptr) && privateData()->isContainer();
}

// Convert items to display format
QVariant ContainerItem::data(int column, int role) const
{
    if (parent() != nullptr) {
        const QVariant& rawData = TreeItem::data(column, Util::RawDataRole);

        // Text alignment:
        if (role == Qt::TextAlignmentRole)
            return { ContainerModel::mdAlignment(column) };

        // Return tooltips.
        if (role == Qt::DecorationRole) {
            switch (column) {
            case ContainerModel::Name: {
                if (!isContainer())
                    return cfgData().iconDir;

                const QVariant& rawClass = TreeItem::data(int(ContainerModel::Class), Util::RawDataRole);

                if (const ContainerClassBase* cc = ContainerClassBase::find(ContainerClass(rawClass.toUInt())))
                     return cc->icon();

                break;
            }

            case ContainerModel::Unprivileged:  [[fallthrough]];
            case ContainerModel::Ephemeral:     [[fallthrough]];
            case ContainerModel::Control:
                if (rawData.toBool())
                    return cfgData().iconCheck;
                break;

            default: break;
            }
        }

        if (role == Util::PlotRole) {
            switch (column) {
            case ContainerModel::CfsQuota:
                // For plotting, handle -1 specially: multiply # of available HW threads by period
                if (rawData.toLongLong() < 0)
                    return data(ContainerModel::CfsPeriod, Util::RawDataRole).toLongLong() *
                        QThread::idealThreadCount();

                return rawData.toLongLong();

            default:
                return rawData;
            }
        }

        if (role == Qt::DisplayRole || role == Util::CopyRole) {
            switch (column) {
            case ContainerModel::Kernel:    [[fallthrough]];
            case ContainerModel::Machine:   [[fallthrough]];
            case ContainerModel::OS:        [[fallthrough]];
            case ContainerModel::Distro:    [[fallthrough]];
            case ContainerModel::Release:   [[fallthrough]];
            case ContainerModel::Codename:  [[fallthrough]];
            case ContainerModel::CLink:     [[fallthrough]];
            case ContainerModel::HLink:     [[fallthrough]];
            case ContainerModel::IPV4:      [[fallthrough]];
            case ContainerModel::IPV6:      [[fallthrough]];
            case ContainerModel::HWAddress: {
                const QVariant& displayData = TreeItem::data(column, Util::RawDataRole);
                if (displayData.isValid()) {
                    if (role == Util::CopyRole)
                        return displayData.toString().replace('\n', ' ');
                    return displayData;
                }
                break;
            }

            case ContainerModel::RxBytes:   [[fallthrough]];
            case ContainerModel::TxBytes:   [[fallthrough]];
            case ContainerModel::TxRxBytes: [[fallthrough]];
            case ContainerModel::RxSpeed:   [[fallthrough]];
            case ContainerModel::TxSpeed:   [[fallthrough]];
            case ContainerModel::TxRxSpeed:
            {
                const QVariant& displayData = TreeItem::data(column, Util::RawDataRole);
                const QList<QVariant>& asList = displayData.toList();
                if (!asList.empty()) {
                    const QString& suffix = ContainerModel::mdRateSuffix(column);
                    QString out;
                    // these are lists
                    for (const auto& b : asList)
                        out += cfgData().units(column)(b) + (role == Util::CopyRole ? ' ' : '\n');

                    out.truncate(out.length()-1);
                    return out + suffix;
                }
                break;
            }

            default: break;
            }

            if (rawData.isValid()) {
                switch (column) {
                case ContainerModel::Class: {
                    const ContainerClassBase* cc = ContainerClassBase::find(ContainerClass(rawData.toUInt()));
                    if (cc != nullptr)
                        return cc->name;
                    return QObject::tr("n/a");
                }

                case ContainerModel::Unprivileged:  [[fallthrough]];
                case ContainerModel::Ephemeral:     [[fallthrough]];
                case ContainerModel::Control:
                    if (role == Util::CopyRole)
                        return rawData.toBool() ? QObject::tr("true") : QObject::tr("false");
                    break;

                case ContainerModel::State:
                    return ContainerStateInfo::text(ContainerState(rawData.toInt()));

                case ContainerModel::Uptime:       [[fallthrough]];
                case ContainerModel::CpuUse:       [[fallthrough]];
                case ContainerModel::Memory:       [[fallthrough]];
                case ContainerModel::KMem:         [[fallthrough]];
                case ContainerModel::Load:         [[fallthrough]];
                case ContainerModel::MemPct:
                    return cfgData().units(column)(rawData);

                case ContainerModel::Processes:    [[fallthrough]];
                case ContainerModel::CpuShares:
                    if (rawData.toLongLong() <= 0)
                        return QObject::tr("n/a");
                    return rawData;

                case ContainerModel::CfsPeriod:
                    if (rawData.toLongLong() <= 0)
                        return QObject::tr("n/a");
                    return rawData.toString() + QObject::tr(" uS");

                case ContainerModel::CfsQuota:
                    return rawData.toLongLong() < 0 ? QObject::tr("unlimited") : (rawData.toString() + QObject::tr(" uS"));

#if USEIO
                case ContainerModel::ReadBytes:    [[fallthrough]];
                case ContainerModel::WriteBytes:   [[fallthrough]];
                case ContainerModel::ReadSpeed:    [[fallthrough]];
                case ContainerModel::WriteSpeed:   [[fallthrough]];
#endif

                case ContainerModel::MemoryLimit:
                    if (rawData.toULongLong() > 32_TiB)
                        return QObject::tr("unlimited");
                    else
                        return cfgData().unitsMemory(rawData);

                default: break;
                }
            }
        } // end of display or copy role
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool ContainerItem::setData(ModelType column, const QVariant &value, int role, bool& changed)
{
    // Try to set the data for the container.
    if (role == Qt::EditRole) {
        changed = false;

        if ((privateData() != nullptr) && privateData()->set(column, value))
            return TreeItem::setData(column, value, Util::RawDataRole, changed);

        return false;
    }

    return TreeItem::setData(column, value, role, changed);
}

ContainerItem *ContainerItem::factory(const ContainerItem::ItemData& data, TreeItem *parent)
{
    [[maybe_unused]] auto* ci = dynamic_cast<ContainerItem*>(parent);

    assert(ci != nullptr);

    return new ContainerItem(data, parent);
}

bool ContainerItem::setPrivateData(ContainerPrivate *data)
{
    delete m_privateData;
    m_privateData = data;
    return true;
}

ContainerPrivate *ContainerItem::privateData() const
{
    return m_privateData;
}
