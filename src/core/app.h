/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APP_H
#define APP_H

#include <QThreadPool>
#include <src/core/appbase.h>

#include "src/core/cfgdata.h"

class QString;
class CmdLine;

class App : public AppBase
{
public:
    App(int &argc, char **argv, const CmdLine&);
    ~App() override;

    CfgData&       cfgData() override { return m_cfgData; }
    const CfgData& cfgData() const override { return m_cfgData; }
    const CmdLine& cmdLine() const { return reinterpret_cast<const CmdLine&>(m_cmdLine); }

    ContainerModel& containerModel() { return m_containerModel; }
    const ContainerModel& containerModel() const { return m_containerModel; }

    void haltThreads();
    QThreadPool& ctrlPool() { return m_ctrlThreadPool; }
    QThreadPool& dataPool() { return m_dataThreadPool; }

private:
    static void setupResources();
    static void setupIcons();
    static void registerTypes();

    struct InitStatic { InitStatic(); } initStatic;  // static initialization happens first, before other members

    // Models
    ContainerModel  m_containerModel;

    QThreadPool     m_ctrlThreadPool;  // for UI commands (start/stop/etc)
    QThreadPool     m_dataThreadPool;  // for gathering data from threads

    CfgData         m_cfgData;         // app configuration
};

inline App& app() { return reinterpret_cast<App&>(*qApp); }
inline const CfgData& cfgData() { return app().cfgData(); }
inline CfgData& cfgDataWritable() { return app().cfgData(); }

#endif // APP_H
