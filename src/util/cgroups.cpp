/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <array>

#include <QFile>

#include "cgroups.h"

CGroups::CGroups() :
    containerOnly(true)
{
}

int CGroups::lineCount(pid_t pid, const char *key, const char* useController) const
{
    const CGroups::Controllers& controllers = readControllers(pid, useController);

    for (const auto& controller : controllers) {
        const QByteArray cgroupFile = controller + "/" + key;

        QFile file(cgroupFile);

        std::array<char, 1024> buf;
        int lines = 0;

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            for (int bytes = 0; bool((bytes = file.read(buf.data(), sizeof(buf)))); ) {
                for (int pos = 0; pos<bytes; ++pos)
                    if (buf.at(pos) == '\n')
                        ++lines;
            }

            return lines;
        }
    }

    return 0;
}

// Read controllers from /proc/self/cgroup
const CGroups::Controllers& CGroups::readControllers(pid_t pid, const char* useController) const
{
    std::array<char, 128> root;
    qsnprintf(root.data(), sizeof(root), "/proc/%d/cgroup", pid);

    return readControllers(root.data(), useController);
}

// Read controllers: return requested one if it exists, or the whole list
const CGroups::Controllers& CGroups::readControllers(const QByteArray& root, const char* useController) const
{
    static const QByteArray cgroupFs = "/sys/fs/cgroup/";

    // return prior cache set
    if (controllers.empty()) {
        QFile file(root);

        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return controllers;

        // this is inefficient, but there aren't many lines so it isn't a large burden.
        const auto lines = file.read(4096).split('\n');   // TODO: fix hardcoded size

        for (const auto& line : lines) {
            const int controllerBegin = line.indexOf(':');
            if (controllerBegin <= 0)
                return controllers;

            const int controllerEnd = line.indexOf(':', controllerBegin+1);

            if (controllerEnd <= 0) // something malformatted.
                return controllers;

            const QByteArray controller = line.mid(controllerBegin+1, controllerEnd-controllerBegin-1);
            const QByteArray path       = line.mid(controllerEnd+1);

            if (controller.size() == 0) {
                // do something here about unified controllers
            } else {
                // Skip if it looks too generic.
                if (!containerOnly || (path != "/" && path != "/user.slice"))
                    controllers.push_back(cgroupFs + controller + path);
            }
        }
    } // end of empty controller fill

    // return single requested controller
    if (useController != nullptr) {
        single.clear();

        const QByteArray prefix = cgroupFs + useController;
        for (const auto& ctrl : controllers) {
            if (ctrl.startsWith(prefix)) {
                single.push_back(ctrl);
                return single;
            }
        }
    }

    // Return entire controller list
    static const Controllers empty;
    return empty;
}

bool CGroups::open(pid_t pid, const char* key, const char* useController, QFile& file, QIODevice::OpenMode mode) const
{
    const CGroups::Controllers& controllers = readControllers(pid, useController);

    if (controllers.empty())  // no controllers found
        return false;

    for (const auto& controller : controllers) {
        file.setFileName(controller + "/" + key);

        if (file.exists() && file.open(mode))
            return true;
    }

    return false;
}

