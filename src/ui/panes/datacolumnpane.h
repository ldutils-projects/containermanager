/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATACOLUMNPANE_H
#define DATACOLUMNPANE_H

#include <src/ui/panes/datacolumnpanebase.h>
#include "pane.h"

// Intermediate class for panes which show columns of data.  Provides methods to
// hide/sort/etc columns.  Has no UI of its own.

class MainWindow;

class DataColumnPane : public DataColumnPaneBase
{
    Q_OBJECT

public:
    DataColumnPane(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent = nullptr);
    ~DataColumnPane() override;

    [[nodiscard]] QString name() const override { return Pane::name(PaneClass(int(paneClass()))); }

protected:
    const MainWindow& mainWindow() const;
    MainWindow& mainWindow();
};

#endif // DATACOLUMNPANE_H
