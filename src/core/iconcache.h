/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONCACHE_H
#define ICONCACHE_H

#include <QHash>
#include <QIcon>
#include <QPair>

class IconCache
{
public:
    // Icon and display-name
    using IconDesc = QPair<QIcon, QString>;

private:
    // Map from resource string to IconDesc as above.
    QHash<QString, IconDesc> icons;

public:
    IconCache(const QString& root);

    using const_iterator = decltype(icons)::const_iterator;
    using iterator       = decltype(icons)::iterator;

    const_iterator cbegin() const { return icons.cbegin(); }
    const_iterator cend()   const { return icons.cend(); }

    iterator       begin()  { return icons.begin(); }
    iterator       end()    { return icons.end(); }

    // Return IconDesc for given resource name
    IconDesc& operator[](const QString& name);

    const QIcon& icon(const QString& resource) const;
    const QIcon& icon(const char* resource) const { return icon(QString(resource)); }

    const_iterator find(const QString& resource) const { return icons.constFind(resource); }
    iterator find(const QString& resource) { return icons.find(resource); }

    int count() const { return icons.count(); }

private:
    static QString resourceName(const QString& resource);
    static QString displayName(const QString& resource);
};

#endif // ICONCACHE_H
