/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "containerprivate.h"
#include "src/core/containercontrol.h"
#include "src/ui/windows/mainwindow.h"
#include <src/util/roles.h>

ContainerPrivate::ContainerPrivate(MainWindow &mainWindow, ContainerModel &model, const ContainerId &id) :
    mainWindow(mainWindow), m_model(model), m_id(id), m_stateInTransition(false)
{
}

void ContainerPrivate::setTransitionState(const QModelIndex& idx, ContainerControl ctrl)
{
    // Set transitional state.  It'll update with the real state next display update.
    // This happens before kicking off the thread.
    model().setData(ContainerModel::State, idx, int(ContainerControlInfo::transitionState(ctrl)),
                    Util::RawDataRole);
}
