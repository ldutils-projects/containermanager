/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LINECHARTPANE_H
#define LINECHARTPANE_H

#include <QtCharts/QSplineSeries>
#include <QPoint>

#include "linechartbase.h"

class LineChartPane final : public LineChartBase<QSplineSeries, QSplineSeries>
{
    Q_OBJECT
    using Data      = QSplineSeries;
    using LowerData = QSplineSeries;

public:
    LineChartPane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~LineChartPane() override;

private slots:
    // These must live here because they're Qt slots, which can't exist in a temlate method.
    void dataUpdated() override { LineChartBase::dataUpdated(); }
    void recalcAxes() override { LineChartBase::recalcAxes(); }
    void pan(QMouseEvent* event, const QPoint& rel) override { LineChartBase::pan(event, rel); }
    void endPan() override { LineChartBase::endPan(); }
    void hoverLegend(bool state) override { ChartBaseData::hoverLegend(QObject::sender(), state); }
    void selectLegend() override { ChartBaseData::selectLegend(QObject::sender()); }
    void hoverData(LineChartPane::Data*, bool state) override;
    void selectData(LineChartPane::Data* data) override { ChartBaseData::selectData(data, [this]() { return chart->series(); }); }
    void setStacked(bool stacked) override { LineChartBase::setStacked(stacked); }

private:
    void setupSignals();

    LowerData* lower(Data* data) const override { return data; }
    LowerData* lower(QAbstractSeries* data) const override { return static_cast<LowerData*>(data); }
};

#endif // LINECHARTPANE_H
