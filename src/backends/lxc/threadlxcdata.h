/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef THREADLXCDATA_H
#define THREADLXCDATA_H

#include "src/core/threaddata.h"

// Object to put into the QT global thread pool for background updates
class ThreadLxcData : public ThreadData
{
public:
    using ThreadData::ThreadData;

protected:
    bool startContainerPid(const Pipe& stdoutfd, const Pipe& stderrfd, pid_t& pid) override;
};

#endif // THREADLXCDATA_H
