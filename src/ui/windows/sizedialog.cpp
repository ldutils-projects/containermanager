/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>

#include <src/util/units.h>
#include <src/util/util.h>
#include <src/util/ui.h>

#include "sizedialog.h"

#include "ui_sizedialog.h"

SizeDialog::SizeDialog(size_t initialValue, int lowestPow2, int highestPow2, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SizeDialog),
    currentValue(initialValue),
    initialValue(initialValue),
    lowestPow2(lowestPow2),
    highestPow2(highestPow2)
{
    ui->setupUi(this);

    lowestPow2  = Util::Clamp(lowestPow2, 2, 62);
    highestPow2 = Util::Clamp(highestPow2, lowestPow2+1, 63);

    ui->sizeSlider->setMinimum(lowestPow2 * 2);  // * 2 because we handle half-steps
    ui->sizeSlider->setMaximum(highestPow2 * 2);

    setupUnits();
    Util::SetupWhatsThis(this);

    setValue(initialValue);
}

SizeDialog::~SizeDialog()
{
    delete ui;
}

void SizeDialog::setupUnits()
{
    for (Format f = Format::KiB; f <= Format::TiB; Util::inc(f))
        ui->sizeUnits->addItem(Units::suffix(f));
}

size_t SizeDialog::currentUnitMultiplier() const
{
    const int idx = ui->sizeUnits->currentIndex();
    if (idx < 0)
        return 1;  // something is wrong

    return Units::multiplier(Format(idx + int(Format::KiB)));
}

int SizeDialog::indexFor(Format f) const
{
    return ui->sizeUnits->findText(Units::suffix(f));
}

// Power of 2 exponents with half steps (e.g, 2, 3, 4, 6, 8, 12, 16, ...)
size_t SizeDialog::sliderToVal(int sliderVal)
{
    return ((1UL<<size_t(sliderVal/2)) + ((sliderVal % 2 == 0) ? 0 : (1UL<<size_t(sliderVal/2-1))));
}

// Convert value to slider position
int SizeDialog::valToSlider(size_t val)
{
    int slider = 0;
    for (size_t v = val; v>1; v >>= 1, slider += 2)
        if (v == 3)
            ++slider;

    return slider;
}

void SizeDialog::setValue(uint64_t size)
{
    Format fmt = Format::MiB;

    currentValue = size;

    size = Util::Clamp(size, uint64_t(1)<<lowestPow2, uint64_t(1)<<highestPow2);

    // Work out base
    if      (size > 2047_GiB) fmt = Format::TiB;
    else if (size > 2047_MiB) fmt = Format::GiB;
    else if (size > 2047_KiB) fmt = Format::MiB;
    else                      fmt = Format::KiB;

    const int comboIndex = indexFor(fmt);
    if (comboIndex < 0)  // something went wrong: it should always be found
        return;

    // Set units box to index for this size
    ui->sizeUnits->setCurrentIndex(comboIndex);

    // Now adjust the slider and spinner to match
    ui->sizeSlider->setValue(valToSlider(size));
    ui->sizeSpinBox->setValue(size / currentUnitMultiplier());
}

void SizeDialog::on_buttonBox_accepted()
{
    emit accepted();
}

void SizeDialog::on_buttonBox_rejected()
{
    currentValue = initialValue;
    emit rejected();
}

void SizeDialog::on_sizeUnits_currentIndexChanged(int/* index*/)
{
    setValue(ui->sizeSpinBox->value() * currentUnitMultiplier());
}

void SizeDialog::on_sizeSlider_valueChanged(int value)
{
    setValue(sliderToVal(value));
}

void SizeDialog::on_sizeSpinBox_valueChanged(int value)
{
    size_t cooked = size_t(value) * currentUnitMultiplier();

    // bump to next unit
    if (value == ui->sizeSpinBox->maximum())
        cooked = 2 * currentUnitMultiplier() * 1024;

    if (value == ui->sizeSpinBox->minimum())
        cooked = 2047 * (currentUnitMultiplier() / 1024);

    setValue(cooked);
}
