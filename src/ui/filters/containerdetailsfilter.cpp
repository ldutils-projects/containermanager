/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "containerdetailsfilter.h"

// This is the layout we'll display.
const ContainerDetailsFilter::Line ContainerDetailsFilter::rootItem = {
    Header(_Count), {
        { Header(Machine), {
            int(ContainerModel::Name),
            int(ContainerModel::Class),
            int(ContainerModel::State),
            { Header(System), {
                int(ContainerModel::OS),
                int(ContainerModel::Kernel),
                int(ContainerModel::Machine),
                int(ContainerModel::Distro),
                int(ContainerModel::Release),
                int(ContainerModel::Codename),
            } },
            { Header(Container), {
              int(ContainerModel::Unprivileged),
              int(ContainerModel::Ephemeral),
              int(ContainerModel::Control),
              int(ContainerModel::ConfigFile),
            } },
        } },
        { Header(Uptime), {
            int(ContainerModel::Uptime),
            int(ContainerModel::Load),
            int(ContainerModel::CpuUse),
            int(ContainerModel::Processes),
        } },
        { Header(CGroup), {
            int(ContainerModel::CpuShares),
            int(ContainerModel::CfsQuota),
            int(ContainerModel::CfsPeriod),
            int(ContainerModel::MemoryLimit),
        }},
        { Header(Memory), {
            int(ContainerModel::Memory),
            int(ContainerModel::MemPct),
            int(ContainerModel::KMem),
        } },
        { Header(Network), {
            int(ContainerModel::HWAddress),
            int(ContainerModel::HLink),
            int(ContainerModel::CLink),
            int(ContainerModel::IPV4),
            int(ContainerModel::IPV6),
            { Header(NetBytes), {
                int(ContainerModel::TxBytes),
                int(ContainerModel::RxBytes),
                int(ContainerModel::TxRxBytes),
            } },
            { Header(NetSpeed), {
                int(ContainerModel::TxSpeed),
                int(ContainerModel::RxSpeed),
                int(ContainerModel::TxRxSpeed),
            } },
        } },
#if USEIO
        { Header(Disk), {
            int(ContainerModel::ReadBytes),
            int(ContainerModel::WriteBytes),
            int(ContainerModel::ReadSpeed),
            int(ContainerModel::WriteSpeed),
        } },
#endif
    },
};

ContainerDetailsFilter::ContainerDetailsFilter(QObject *parent) :
    DetailFilter(rootItem, int(ContainerModel::_First), int(ContainerModel::_Count), parent)
{
}

QString ContainerDetailsFilter::text(DetailFilter::Header header) const
{
    if (int(header) >= _First) {
        switch (int(header)) {
        case Machine:   return QObject::tr("Machine");
        case System:    return QObject::tr("System");
        case Container: return QObject::tr("Container Info");
        case Uptime:    return QObject::tr("Run State");
        case Network:   return QObject::tr("Networking");
        case NetBytes:  return QObject::tr("Data");
        case NetSpeed:  return QObject::tr("Speed");
        case CGroup:    return QObject::tr("Limits");
        case Memory:    return QObject::tr("Memory");
        case Disk:      return QObject::tr("Disk");
        default:                return QObject::tr("n/a");
        }
    } else {
        return ContainerModel::mdName(int(header));
    }
}
