/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPPEDBASE_H
#define MAPPEDBASE_H

#include <src/core/query.h>
#include "pane.h"

class QAbstractItemModel;

class MappedBase : public Pane
{
    Q_OBJECT

public:
    MappedBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent = nullptr);

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setModel(const QAbstractItemModel* model);

protected slots:
    virtual void processModelReset();
    virtual void processRowsInserted(const QModelIndex& parent, int start, int end);
    virtual void processRowsAboutToBeRemoved(const QModelIndex& parent, int start, int end);
    virtual void processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                                    const QVector<int>& roles);

    virtual void setQueryColumn(int);
    void setQuery(const QString&) override;

protected:
    void showAll() override;
    void newConfig() override;

    virtual bool accept(const QModelIndex& idx) const;
    virtual bool filterAcceptsRow(const QModelIndex& idx) const;
    virtual bool invalidateFilter();  // return true if any changes

    virtual void insert(const QModelIndex& idx, int pos = -1) = 0;  // insert before pos, if >=0
    virtual void remove(const QModelIndex& idx) = 0;
    virtual bool contains(const QModelIndex& idx) const = 0;
    virtual void show(const QModelIndex& idx, int pos = -1) { return insert(idx, pos); }
    virtual void hide(const QModelIndex& idx) { return remove(idx); }
    virtual bool shown(const QModelIndex& idx) const { return contains(idx); }
    virtual void clear() = 0;
    virtual void clearHistory() { } // Clear history, but not container references
    virtual bool canHide() const { return false; }

    // Insert or remove entries if the filter now accepts/rejects them, for use from
    // processDataChanged.  Returns true on changes.
    bool filterUpdate(const QModelIndex& idx, int& pos, bool accepted,
                      void (MappedBase::*insert)(const QModelIndex&, int),
                      void (MappedBase::*remove)(const QModelIndex&),
                      bool (MappedBase::*contains)(const QModelIndex&) const);

    bool filterUpdate(const QModelIndex& idx, int& pos);

    bool dataChangedUpdate(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                           const QVector<int>& /*roles*/,
                           bool& spansState, bool& spansPlot);

    ModelType                 m_queryColumn;  // which column the m_queryText should search
    const QAbstractItemModel* m_model;        // model we'll display data for
    QString                   m_queryText;    // in-use query text
    Query::Context            m_queryCtx;     // for query language
    Query::query_uniqueptr_t  m_queryRoot;    // root of node filter tree
};

#endif // MAPPEDBASE_H
