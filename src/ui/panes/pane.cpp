/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>
#include <QMenu>
#include <QWidget>
#include <QToolTip>

#include "pane.h"
#include "src/util/util.h"
#include "src/core/containercontrol.h"
#include "src/ui/windows/mainwindow.h"
#include <src/ui/panes/emptypane.h>
#include "src/ui/panes/areachartpane.h"
#include "src/ui/panes/consolepane.h"
#include "src/ui/panes/containerpane.h"
#include "src/ui/panes/detailpane.h"
#include "src/ui/panes/piechartpane.h"
#include "src/ui/panes/linechartpane.h"

Pane::Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    PaneBase(mainWindow, PaneClass_t(int(paneClass)), parent),
    containerModel(app().containerModel())
{
}

const MainWindow& Pane::mainWindow() const
{
    return static_cast<const MainWindow&>(PaneBase::mainWindow());
}

MainWindow& Pane::mainWindow()
{
    return static_cast<MainWindow&>(PaneBase::mainWindow());
}

QString Pane::name(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:     return QObject::tr("Empty Pane");
    case PaneClass::Container: return QObject::tr("Container List");
    case PaneClass::Detail:    return QObject::tr("Container Details");
    case PaneClass::PieChart:  return QObject::tr("Pie Chart");
    case PaneClass::LineChart: return QObject::tr("Line Chart");
    case PaneClass::AreaChart: return QObject::tr("Area Chart");
    case PaneClass::Console:   return QObject::tr("Console");
    case PaneClass::Group:     return QObject::tr("Pane Group");
    case PaneClass::_Count:    break;
    }

    assert(0);
    return QObject::tr("n/a");
}

PaneClass Pane::findClass(const QString& className)
{
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc))
        if (name(pc) == className)
            return pc;

    return PaneClass::Empty; // failed to find it
}

QWidget* Pane::widgetFactory(PaneClass pc, MainWindowBase& mainWindow)
{
    // This can't be done in the pane constructor, since the obj is not fully baked yet.
    const auto setClass = [](PaneBase* pane) {
        pane->setObjectName(pane->metaObject()->className());
        return pane;
    };

    auto& mw = static_cast<MainWindow&>(mainWindow);

    switch (pc) {
    case PaneClass::Empty:     return setClass(new EmptyPane(mw, PaneClass_t(int(PaneClass::Empty))));
    case PaneClass::Container: return setClass(new ContainerPane(mw));
    case PaneClass::Detail:    return setClass(new DetailPane(mw));
    case PaneClass::PieChart:  return setClass(new PieChartPane(mw));
    case PaneClass::LineChart: return setClass(new LineChartPane(mw));
    case PaneClass::AreaChart: return setClass(new AreaChartPane(mw));
    case PaneClass::Console:   return setClass(new ConsolePane(mw));
    case PaneClass::Group:     return newContainer(mainWindow);
    case PaneClass::_Count:    break;
    }

    assert(0);
    return nullptr;
}

// create, set up, and return new pane container
Pane::Container *Pane::newContainer(MainWindowBase& mainWindow)
{
    auto* splitter = new Pane::Container(mainWindow);

    splitter->setChildrenCollapsible(false); // we allow removing children, but not collapsing.
    splitter->setObjectName(containerName);

    return splitter;
}

QString Pane::tooltip(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:     return QObject::tr("Displays nothing.");
    case PaneClass::Container: return QObject::tr("Container tree, with columns for data.");
    case PaneClass::Detail:    return QObject::tr("Detail view of a single container.");
    case PaneClass::PieChart:  return QObject::tr("Dynamically updated pie chart of selected data, from multiple containers.");
    case PaneClass::LineChart: return QObject::tr("Moving line chart of selected data over time, from multiple containers.");
    case PaneClass::AreaChart: return QObject::tr("Moving stacked line chart showing data from multiple containers over time.");
    case PaneClass::Console:   return QObject::tr("Multi-container command execution.");
    case PaneClass::Group:     return QObject::tr("Horizontal or vertical group of panes.");
    case PaneClass::_Count:    break;
    }

    assert(0);
    return QObject::tr("n/a");
}

QString Pane::iconFile(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:     return "window-new";
    case PaneClass::Container: return "view-list-tree";
    case PaneClass::Detail:    return "view-list-details";
    case PaneClass::PieChart:  return "office-chart-pie";
    case PaneClass::LineChart: return "office-chart-line";
    case PaneClass::AreaChart: return "office-chart-area";
    case PaneClass::Console:   return "utilities-terminal";
    case PaneClass::Group:     return "window-new";
    case PaneClass::_Count:    break;
    }

    assert(0);
    return { };
}

QString Pane::whatsthis(PaneClass pc)
{
    return tooltip(pc);
}

QString Pane::statusTip(PaneClass pc)
{
    return tooltip(pc);
}

bool Pane::spans(ModelType target, ModelType left, ModelType right)
{
    return target >= left && target <= right;
}

bool Pane::spans(ModelType target, const QModelIndex& left, const QModelIndex& right)
{
    return spans(target, left.column(), right.column());
}

QDataStream& operator<<(QDataStream& out, const PaneClass& pc)
{
    return out << std::underlying_type<PaneClass>::type(pc);
}

QDataStream& operator>>(QDataStream& in, PaneClass& pc)
{
    return in >> (std::underlying_type<PaneClass>::type&)(pc);
}
