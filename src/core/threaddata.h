/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef THREADDATA_H
#define THREADDATA_H

#include <cinttypes>

#include <QByteArray>
#include <QVector>
#include <QPersistentModelIndex>
#include <QRegExp>

#include <src/core/modelmetadata.h>
#include <src/util/pipe.h>

#include "src/core/threadbase.h"

class ContainerPrivate;
class ContainerModel;
class MainWindow;
class Pipe;

// Thread for reading stdout/stderr from a background process and updating the
// container model, or sending log messages with the result.
class ThreadData : public ThreadBase
{
public:
    enum class FinishCommand {
        None,     // do nothing
        Data,     // set ContainerData from entire output
        Words,    // set multiple ContainerData from sequential output words (not lines!)
        EnvFile,  // set multiple ContainerData from values in an env-like file
        Log,      // send to log, all at once after command completes.
        LogAsync, // send to log in asynchronous chunks as data arrives.
        _Count,
    };

    ThreadData(MainWindow& mainWindow, ContainerPrivate* privateData,
               int timeout, const QByteArray& progStr, const QVector<QByteArray>& argvStr);

    // Helper to call the above constructor, varargs style
    template <typename... T>
    ThreadData(MainWindow& mainWindow, ContainerPrivate* privateData, int timeout,
               const QByteArray& progStr, const T&... t) :
        ThreadData(mainWindow, privateData, timeout, progStr, { progStr, t... } )
    { }

    ~ThreadData() override;

    // Output line put in entry pointed to by idx
    ThreadData* requestData(const QModelIndex& idx, const QVector<ModelType>& cols);

    // Subsequent space-separated words put in "cols" columns of row pointed to by idx
    ThreadData* requestWords(const QModelIndex& idx, const QVector<ModelType>& cols);

    // Subsequent env-like matches in output to "keys" put in "cols" entries.
    ThreadData* requestEnvFile(const QModelIndex& idx,
                               const QVector<QRegExp>& keys,
                               const QVector<ModelType>& cols);

    // Request output messages
    ThreadData* requestLog(const QModelIndex& idx, const QByteArray& cmdline);

    // Request asynchronous output messages
    ThreadData* requestLogAsync(const QModelIndex& idx, const QByteArray& cmdline);

    bool setsThreadData(const QModelIndex& idx, const QVector<ModelType>& cols) const;

protected:
    virtual bool startContainerPid(const Pipe& stdoutfd, const Pipe& stderrfd, pid_t& pid) = 0;

    bool prepareToRun() final;
    void run() final;

    void setData();
    void setWords();
    void setEnvFile();
    void setLog();
    void sendPartialOutput();  // emit any partial output in units of whole lines.

    // Timeouts for thread run/read loop
    static const uint32_t  timeout_Done    = 1000;       // in uSec
    static const uint32_t  timeout_Running = 250000;     // in uSec

    pid_t                  m_pid = -1;      // PID of LXC attach process
    Pipe                   m_stdoutfd;      // stdout FD from LXC attach process
    Pipe                   m_stderrfd;      // stderr FD from LXC attach process

    FinishCommand          m_finishCommand; // what should we do when thread has completed
    QPersistentModelIndex  m_index;         // index in tree we belonged to at thread start
    QVector<ModelType>     m_columns;       // if non-empty, set from multiple words of the output
    QVector<QRegExp>       m_envKeys;       // for parsing env-like init files

    QByteArray             m_progStr;       // raw storage, since these must persist for the life
    QVector<QByteArray>    m_argvStr;       // of the thread.

    int                    m_timeout;       // how long to wait for subprocesses

    // for packaging up arg lists to send to a C interface
    QVector<const char*>   m_argv;          // arg list, arg[0] to include prog

    QVector<QByteArray>    m_output;        // outputs

    ContainerPrivate*      m_privateData;
};

#endif // THREADDATA_H
