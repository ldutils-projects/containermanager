/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QComboBox>

#include "src/core/containerclass.h"
#include "containerclassdelegate.h"

ContainerClassDelegate::ContainerClassDelegate(QObject *parent) :
    ComboBoxDelegate(parent)

{
    for (const auto& cclass : ContainerClassBase::ContainerClasses)
        if (cclass->containerClass != ContainerClass::None)
            addItem(cclass->name);
}
