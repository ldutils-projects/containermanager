/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QStandardItemModel>
#include "colorlistdelegate.h"

QWidget* ColorListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    auto* colorEditor = dynamic_cast<QColorDialog*>(ColorDelegate::createEditor(parent, option, index));

    const auto* colorModel = qobject_cast<const QStandardItemModel*>(index.model());

    if (colorEditor == nullptr || colorModel == nullptr || bgCol.empty())
        return nullptr;

    colorEditor->setCurrentColor(colorModel->item(index.row(), bgCol[0])->background().color());

    return colorEditor;
}

void ColorListDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    auto* colorEditor = static_cast<QColorDialog*>(editor);
    auto* colorModel  = qobject_cast<QStandardItemModel*>(model);

    if (colorEditor == nullptr || colorModel == nullptr)
        return;

    if (colorEditor->result() != QDialog::Accepted)
        return;

    if (enabled)
        for (const auto& fg : fgCol)
            colorModel->item(index.row(), fg)->setForeground(colorEditor->currentColor());

    // Always update the first BG color: that's the color patch.
    for (const auto& bg : bgCol) {
        colorModel->item(index.row(), bg)->setBackground(colorEditor->currentColor());
        if (!enabled)
            break;
    }
}

