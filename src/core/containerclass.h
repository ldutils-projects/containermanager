/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERCLASS_H
#define CONTAINERCLASS_H

#include <QString>
#include <QList>
#include <QVector>
#include <QStandardItem>
#include <QHash>
#include <QVariant>
#include <QIcon>

class MainWindow;
class ContainerPrivate;

enum class ContainerClass {
    None = 0,
    Lxc,
    _Count,
};

Q_DECLARE_METATYPE(ContainerClass)
QDataStream& operator<<(QDataStream&, const ContainerClass&);
QDataStream& operator>>(QDataStream&, ContainerClass&);

// For use with sets/etc
inline uint qHash(ContainerClass cc, uint seed = 0) {
    return qHash(int(cc), seed);
}

class ContainerClassDelegate;
class ContainerModel;
class MainWindow;
class CfgDir;

class ContainerClassBase
{
public:
    ContainerClassBase(ContainerClass cclass, const QString& name, MainWindow& mainWindow);

    const ContainerClass containerClass;
    const QString        name;  // class name - e.g, "Lxc"

    // virtual method to update container list.  Top level items must store cfgDir.directory
    virtual void updateList(ContainerModel& model, const QModelIndex& parent, const CfgDir& cfgDir,
                            QSet<const ContainerPrivate*>& updated) const = 0;

    // container directories
    virtual void defaultDirs(QList<CfgDir>& dirs) const = 0;

    // Icon for this class
    virtual const QIcon& icon() const = 0;

    static void initContainerTypes(MainWindow& mainWindow);
    static const ContainerClassBase* find(const QString& name);
    static const ContainerClassBase* find(enum ContainerClass type);

protected:
    MainWindow& mainWindow;

private:
    ContainerClassBase(const ContainerClassBase&) = delete;
    ContainerClassBase& operator=(const ContainerClassBase&) = delete;

    static QHash<ContainerClass, const ContainerClassBase*> ContainerClasses;

    friend class ContainerClassDelegate;
};

class ContainerClassNone : public ContainerClassBase
{
public:
    ContainerClassNone(ContainerClass cclass, const QString& name, MainWindow& mainWindow) :
        ContainerClassBase(cclass, name, mainWindow) { }

    void updateList(ContainerModel& /*model*/, const QModelIndex& /*parent*/, const CfgDir& /*cfgDir*/,
                    QSet<const ContainerPrivate*>& /*updated*/) const override { }

    void defaultDirs(QList<CfgDir>& /*dirs*/) const override { }

    const QIcon& icon() const override { return classIcon; }

private:
    QIcon classIcon;
};

#endif // CONTAINERCLASS_H
