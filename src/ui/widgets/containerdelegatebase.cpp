/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QItemSelectionModel>

#include "containerdelegatebase.h"

ContainerDelegateBase::ContainerDelegateBase(QObject* parent, bool popupEditor) :
    QStyledItemDelegate(parent),
    selector(nullptr),
    popupEditor(popupEditor)
{
}

void ContainerDelegateBase::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/*index*/) const
{
    if (popupEditor) {
        const QRect newGeometry = { static_cast<QWidget*>(parent())->mapToGlobal(option.rect.topLeft()),
                                    editor->sizeHint() };

        editor->setGeometry(newGeometry);
    } else {
        editor->setGeometry(option.rect);
    }
}

QDialog* ContainerDelegateBase::setPopup(QDialog* editor)
{
    editor->setWindowFlags(Qt::Popup);
    editor->setModal(true);
    editor->show();

    return editor;
}

void ContainerDelegateBase::updateResultFromEditor(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index,
                                                   const std::function<QVariant(bool&)>& fetchFromEditor) const
{
    if (editor == nullptr)
        return;

    bool accepted;
    const QVariant value = fetchFromEditor(accepted);

    if (!accepted)
        return;

    // Set all selected rows, if someone set up the selector.
    const QModelIndexList selected = (selector != nullptr) ? selector->selectedRows() : QModelIndexList();

    if (!selected.empty()) {
        // We have multiple selected rows to set.
        for (const auto& s : selected)
            model->setData(s.sibling(s.row(), index.column()), value, Qt::EditRole);
    } else {
        // No selection: just set the index item.
        model->setData(index, value, Qt::EditRole);
    }
}
