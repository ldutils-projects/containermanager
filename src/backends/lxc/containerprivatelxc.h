/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERPRIVATELXC_H
#define CONTAINERPRIVATELXC_H

#include <cinttypes>

#include <QObject>
#include <QByteArray>
#include <QStringList>
#include <QVector>
#include <QRegExp>
#include <QElapsedTimer>

#include "src/core/containerprivate.h"
#include "src/core/containermodel.h"
#include "src/util/cgroups.h"
#include "threadlxcdata.h"

class QThreadPool;
class MainWindow;
struct lxc_container;
enum class ContainerControl;

class ContainerPrivateLxc : public ContainerPrivate
{
public:
    ContainerPrivateLxc(MainWindow& mainWindow,
                        ContainerModel& model, const char* name, const char* configPath,
                        uid_t uid, gid_t gid);
    ContainerPrivateLxc(MainWindow& mainWindow,
                        ContainerModel& model, const char* name, const char* configPath,
                        const QByteArray& uid_s, const QByteArray& gid_s);
    ~ContainerPrivateLxc() override;

    lxc_container* lxc() const { return lxc_data; }

    // Update model's data for this container, at row given by index
    void updateData(const QModelIndex& index, bool force) final;

    // Set data for the container(!).  Return true for success.
    bool set(ModelType, const QVariant& value) final;

    // return false for non-containers, such as headers, etc.
    bool isContainer() const final { return lxc() != nullptr; }

    // Container operations: start, stop etc.
    bool control(const QModelIndex& idx, ContainerControl) override;

    // Execute command in container asynchronously.
    bool execute(const QModelIndex& idx, const QByteArray& cmdline,
                 const QByteArray& progstr, const QVector<QByteArray>& argvs,
                 int timeout = -1, ThreadSet* = nullptr) override;

    QByteArray  rootfs;     // to avoid reading it each time

private:
    void setupSignals();

    using SizeList = QList<QVariant>;

    // Start async thread writing to idx, only if all columns are null
    template <typename... T>
    bool asyncSet(const QModelIndex& idx,
                  const QVector<QRegExp>& keys,
                  const QVector<ModelType>& cols,
                  ThreadLxcData::FinishCommand,
                  const QByteArray& progStr, const T&... t);

    // Start async thread for container control
    template <typename... T>
    bool asyncControl(const QModelIndex& idx, ContainerControl ctrl, const T&... t);

    template <typename THREAD>
    THREAD* threadsetContains(const ThreadSet&, const QModelIndex&,
                              const QVector<ModelType>& cols = { }) const;

    // Data collection functions
    ContainerState state() const;
    static ContainerState state(lxc_container* lxc);

    quint64 procUptime(pid_t pid) const;

    void interfaces(QStringList& interfaces, QStringList& hwAddr,
                    SizeList& tx, SizeList& rx, SizeList& txrx,
                    quint64& txTotal, quint64& rxTotal, quint64& txrxTotal) const;

    void rates(float dTime, const SizeList& txBytes, const SizeList& rxBytes,
               SizeList& txSpeed, SizeList& rxSpeed, SizeList& txrxSpeed,
               quint64& txSpeedTotal, quint64& rxSpeedTotal, quint64& txrxSpeedTotal) const;

    void addresses(QStringList& ipv4, QStringList& ipv6) const;

#if BLKIO
    void blockIo(pid_t pid, float dTime, quint64& readBytes, quint64& writeBytes,
                 quint64& readSpeed, quint64& writeSpeed) const;
#endif

    template <typename T>
    T getCgroupItem(pid_t pid, const char* item, int buflen, const char* controller) const;

    template <typename T>
    T getCgroupItem(pid_t pid, int buflen, ModelType) const;

    template <typename T>
    bool setCgroupItem(pid_t pid, const T& value, ModelType);

    friend class ThreadLxcData;
    friend class ThreadLxcCtrl;

    // for calculating rates of changes
    bool        prevRunning;
    quint64     prevCpuUse;
    quint64     prevUptime;
    SizeList    prevTxBytes;
    SizeList    prevRxBytes;
    quint64     prevReadBytes;
    quint64     prevWriteBytes;
    uid_t       uid;
    gid_t       gid;

    mutable uint64_t bootTimeInTcks;  // mutable: it's a private cache
    QElapsedTimer    elapsed;         // time since we last ran

    ThreadSet      ctrlThreads;  // control threads
    ThreadSet      dataThreads;  // data threads

    QVector<int>   dataTries;    // if must try too many times, give up.
    static const int maxDataFailures = 5;

    lxc_container* lxc_data;

    // Cache cgroup
    CGroups   cgroups;
};

#endif // CONTAINERPRIVATELXC_H
