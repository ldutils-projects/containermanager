/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERID_H
#define CONTAINERID_H

#include <QHash>
#include <QByteArray>

enum class ContainerClass;

// Data which, together, uniquely identifies a container.
class ContainerId
{
public:
    ContainerId(const QByteArray& name, const QByteArray& path, ContainerClass cClass) :
        m_name(name), m_path(path), m_class(cClass) { }

    // use default copy constructor and assignment

    bool operator==(const ContainerId& other) const {
        return name() == other.name() && directory() == other.directory() && getClass() == other.getClass();
    }

    const QByteArray& directory() const { return m_path; }
    const QByteArray& name()      const { return m_name; }
    ContainerClass    getClass()  const { return m_class; }

private:
    const QByteArray     m_name;
    const QByteArray     m_path;
    const ContainerClass m_class;
};

uint qHash(const ContainerId& item, uint seed = 0);

#endif // CONTAINERID_H
