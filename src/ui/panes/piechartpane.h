/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PIECHARTPANE_H
#define PIECHARTPANE_H

#include <QTimer>
#include <QMenu>
#include <QtCharts/QPieSeries>

#include "chartbase.h"

class PieChartPane final : public ChartBaseData<QPieSlice>
{
    Q_OBJECT

public:
    PieChartPane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~PieChartPane() override;

protected:
    void expandAll() override;
    void collapseAll() override;
    void resizeToFit(int defer = -1) override;

private slots:
    void displayAllLabels();
    void hoverData(QtCharts::QPieSlice* data, bool state) override;
    void selectData(QtCharts::QPieSlice*) override;
    void hoverLegend(bool state) override { ChartBaseData::hoverLegend(QObject::sender(), state); }
    void selectLegend() override { ChartBaseData::selectLegend(QObject::sender()); }

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    using Data = QPieSlice;

    void insert(const QModelIndex& idx, int pos = -1) override;
    void remove(Data* data) override;
    void clear() override;

    void        setupChart();
    void        setupTimer();
    void        setupContextMenus();
    void        setupSignals();
    void        updateChart() override;
    bool        drawLabel(const Data*) const; // return true if label should be drawn
    void        zoom(float zoom) override;
    Data*       updateDataValue(Data*, QModelIndex idx) const;
    void        updateDataValue(QModelIndex idx) const override { updateDataValue(nullptr, idx); }
    void        setDataLabelColor(Data* data, const QColor& color) const override { data->setLabelColor(color); }

    void        setSelected(Data* data, bool select) override;

    QPieSeries* pieSeries;
    QTimer      hoverOverTimer;
    bool        labelsAlwaysVisible;

    static const constexpr float labelMinAngleSpan = 5;
    static const constexpr float explodedDistance = 0.1;
    static const constexpr float armLength = 0.12;
    static const constexpr float holeSize = 0.2;
    static const constexpr float pieSize = 1.0;
    static const constexpr int   labelMaxCount = 20;
};

#endif // PIECHARTPANE_H
