/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "usecdelegate.h"
#include "src/core/containermodel.h"
#include "src/ui/windows/usecdialog.h"
#include "src/ui/filters/subtreefilter.h"
#include <src/util/roles.h>

USecDelegate::USecDelegate(QObject* parent) :
    ContainerDelegateBase(parent)
{
}

QWidget *USecDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex& index) const
{
    const auto* cModel = dynamic_cast<const SubTreeFilter*>(index.model());

    if (cModel == nullptr)
        return nullptr;

    int period    = cModel->data(cModel->sibling(index.row(), ContainerModel::CfsPeriod, index),
                                 Util::RawDataRole).toInt();

    if (period == 0)  // FIX: KLUDGE: if editing in detail view, we fail to find the period.
        period = 100000;

    const int value     = index.model()->data(index, Util::RawDataRole).toInt();
    const int maxPeriod = 1000000;  // TODO: get from somewhere...
    const bool editingPeriod = (index.column() == ContainerModel::CfsPeriod);

    return setPopup(new USecDialog(value, editingPeriod ? maxPeriod : period, editingPeriod, parent));
}

void USecDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    updateResultsFromDialog<USecDialog>(editor, model, index);
}
