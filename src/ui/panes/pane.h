/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANE_H
#define PANE_H

#include <src/ui/panes/panebase.h>
#include "panegroup.h"
#include "src/core/cfgdata.h"

class QModelIndex;
class MainWindow;
class QString;

class ContainerModel;

// Update this when adding more pane types
enum class PaneClass {
    _First = 0,
    Empty     = _First,
    Container,
    Detail,
    PieChart,
    LineChart,
    AreaChart,
    Console,
    _Count,

    Group = 65536, // avoid namespace used by the main pane classes
};

Q_DECLARE_METATYPE(PaneClass)
QDataStream& operator<<(QDataStream&, const PaneClass&);
QDataStream& operator>>(QDataStream&, PaneClass&);

enum class ContainerData;

// Class for operations provided by UI panels
class Pane : public PaneBase
{
    Q_OBJECT

public:
    using Container = PaneGroup;

    // *** begin Pane API
    Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent);

    template <typename T>
    static T* factory(PaneClass pc, MainWindowBase& mainWindow) {
        return dynamic_cast<T*>(widgetFactory(pc, mainWindow));
    }

    [[nodiscard]] QString name() const override { return name(PaneClass(int(paneClass()))); }

    // Static things we can ask about a pane action or class.
    static QString   name(PaneClass pc);
    static PaneClass findClass(const QString& className); // name to class
    static QString   tooltip(PaneClass pc);
    static QString   whatsthis(PaneClass pc);
    static QString   statusTip(PaneClass pc);
    static QString   iconFile(PaneClass pc);

protected:
    friend class PaneGroupBase;
    const MainWindow& mainWindow() const;
    MainWindow& mainWindow();

    static QWidget* widgetFactory(PaneClass pc, MainWindowBase& mainWindow); // factory

    static bool spans(ModelType target, ModelType left, ModelType right);
    static bool spans(ModelType target, const QModelIndex& left, const QModelIndex& right);

    ContainerModel&  containerModel;

private:
    static Container* newContainer(MainWindowBase& mainWindow);

    friend class PaneGroup;

    Pane(const Pane&) = delete;
    Pane& operator=(const Pane&) = delete;
};

#endif // PANE_H
