/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QToolButton>
#include <QBoxLayout>

#include <src/core/iconcache.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "icondialog.h"
#include "ui_icondialog.h"

IconDialog::IconDialog(const MainWindow& mainWindow, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IconDialog),
    mainWindow(mainWindow)
{
    ui->setupUi(this);

    setupResourceIcons();
    Util::SetupWhatsThis(this);
}

IconDialog::~IconDialog()
{
    delete ui;
}

void IconDialog::setupResourceIcons()
{
    const IconCache& iconCache = mainWindow.iconCache();

    // Calculate how many rows we'll need
    const int rows = (iconCache.count() + ui->iconTable->columnCount() - 1) /
            ui->iconTable->columnCount();

    // * 2 because we use one row for icons, one for names.
    ui->iconTable->setRowCount(rows * 2);

    int row = 0;
    int col = 0;

    for (auto icon = iconCache.cbegin(); icon != iconCache.cend(); ++icon) {
        auto* iconWidget = new QLabel(this);
        auto* nameWidget = new QLabel(icon->second);

        iconWidget->setPixmap(icon->first.pixmap(QSize(32,32)));

        iconWidget->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        nameWidget->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        iconWidget->setProperty("resource", icon.key());
        nameWidget->setProperty("resource", icon.key());

        ui->iconTable->setCellWidget(row + 0, col, iconWidget);
        ui->iconTable->setCellWidget(row + 1, col, nameWidget);

        if (++col >= ui->iconTable->columnCount()) {
            col = 0;
            row += 2;
        }
    }

    connect(ui->iconTable, &QTableWidget::cellDoubleClicked, this, &IconDialog::iconDoubleClicked);
}

void IconDialog::on_openFromFile_clicked()
{
    // TODO: save the last used directory, use as next default.
    const QString iconFile =
            QFileDialog::getOpenFileName(this, tr("Select Icon File"), "~",
                                         "Images (*.png *.xpm *.jpg *.svg)",
                                         nullptr, QFileDialog::ReadOnly);

    if (iconFile.isEmpty()) {
        reject();
    } else {
        selectedIcon = QIcon(iconFile);
        accept();
    }
}

void IconDialog::iconDoubleClicked(int row, int col)
{
    const IconCache& iconCache = mainWindow.iconCache();

    const QString& resource = ui->iconTable->cellWidget(row, col)->property("resource").toString();

    const auto entry = iconCache.find(resource);
    if (entry == iconCache.cend()) {
        reject();
        return;
    }

    selectedIcon = entry->first;
    accept();
}
