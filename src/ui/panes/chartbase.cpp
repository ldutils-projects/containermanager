/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <typeinfo>

#include <QSet>
#include <QPair>
#include <QMultiMap>

#include "src/core/containermodel.h"
#include "src/core/cfgdata.h"
#include "src/core/containerstate.h"
#include "src/util/util.h"
#include "src/util/ui.h"

#include "chartbase.h"
#include "ui_chartbase.h"

ChartBase::ChartBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    MappedBase(mainWindow, paneClass, parent),
    ui(new Ui::ChartBasePane),
    chart(new QChart()),
    chartView(this),
    plotColumn(ContainerModel::Memory),
    zoomStep(1.1),
    zoomLevel(1.0 / pow(zoomStep, 3)),
    initZoom(zoomLevel),
    minZoom(1 / pow(zoomStep, 20)),
    maxZoom(1.0),
    labelHighlight(QApplication::palette().highlight().color()),
    labelNormal(QApplication::palette().text().color()),
    labelSelected(QApplication::palette().link().color()),
    majorGridPen(QApplication::palette().alternateBase(), 2, Qt::SolidLine),
    minorGridPen(QApplication::palette().alternateBase(), 1, Qt::DashLine),
    markerNormalPen(QApplication::palette().window().color()),
    markerHighlightPen(QApplication::palette().highlight().color())
{
    ui->setupUi(this);
    paneFilterBar = ui->chartFilterBar;

    setupSubsetComboBox();
    setupPlotColumnComboBox();
    setupSignals();
    setupChartView();

    Util::SetupWhatsThis(this);
}

ChartBase::~ChartBase()
{
    deleteUI(ui);
}

// Must call from subclass constructors, not our constructor.
void ChartBase::setupContextMenus()
{
    setupActionContextMenu(paneMenu);

    connect(&chartView, &ChartViewZoom::customContextMenuRequested, this, &ChartBase::showActionContextMenu);
}

void ChartBase::setupSignals()
{
    // Filter signals
    connect(ui->chartFilter, &QLineEdit::textChanged, this, &ChartBase::setQuery);
    connect(ui->showLegend, &QCheckBox::toggled, this, &ChartBase::showLegend);
}

void ChartBase::setupChartView()
{
    if (chart == nullptr)
        return;

    chartView.setChart(chart);  // takes ownership of chart

    chart->setBackgroundVisible(true);
    chart->setPlotAreaBackgroundVisible(true);
    chart->setPlotAreaBackgroundBrush(QApplication::palette().base());
    chart->setBackgroundBrush(QApplication::palette().window());

    ui->topLayout->addWidget(&chartView);
    chartView.setRenderHint(QPainter::Antialiasing);

    disableToolTipsFor(&chartView);
}

void ChartBase::setupPlotColumnComboBox()
{
    ContainerModel::setupComboBox<ContainerModel>(*ui->chartData, plotColumnModel, &plotColumnEntries,
                                                  ContainerModel::mdIdentityItem,
                                                  [](ModelType mt) { return ContainerModel::mdIsChartable(mt); });

    connect(ui->chartData, (void (QComboBox::*)(int)) &QComboBox::currentIndexChanged,
                this, &ChartBase::setChartColumn);
}

void ChartBase::setupSubsetComboBox()
{
    auto* columnSelect = new QStandardItem(tr("All"));
    filterColumnModel.appendRow(columnSelect);

    ContainerModel::setupComboBox<ContainerModel>(*ui->filterColumn, filterColumnModel, nullptr);

    connect(ui->filterColumn, (void (QComboBox::*)(int)) &QComboBox::currentIndexChanged,
                this, &ChartBase::setQueryColumnUi);

    ChartBase::setQueryColumnUi(0);
}

void ChartBase::setChartData(ModelType mt)
{
    const int column = plotColumnEntries.indexOf(mt);
    if (column >= 0)
        setChartColumn(column);
}

void ChartBase::newConfig()
{
    MappedBase::newConfig();

    invalidateFilter();
    calcDataColors();
    updateChart();
    zoom(zoomLevel);
    setChartData(plotColumn);
    ui->chartFilter->setText(m_queryText);

    // This seems necessary to refresh the legend labels.  Unclear why.
    chart->legend()->setVisible(false);
    chart->legend()->setVisible(ui->showLegend->isChecked());
}

void ChartBase::setChartColumn(ModelType column)
{
    if (ui == nullptr)
        return;

    ui->chartData->setCurrentIndex(column);

    const ModelType newColumn = plotColumnEntries[column];

    paneBox().setTitle(ContainerModel::mdName(newColumn) + titleSuffix());

    if (plotColumn == newColumn) // avoid clearing data if nothing changed
        return;

    plotColumn = newColumn; // map from subset displayed in combo box

    if (typeid(*this) != typeid(ChartBase)) {
        clearHistory();
        updateChart();
    }
}

void ChartBase::setQueryColumnUi(ModelType column)
{
    MappedBase::setQueryColumn(column - 1);  // -1 for "All" header
    ui->filterColumn->setCurrentIndex(column);
}

// Work out which running containers match which patterns.  If multiple
// containers match one pattern, colors are interpolate across the set.
// We make some attempts to balance the sizes of each list, because a
// single container can match multiple patterns, and we don't want one
// very long interpolated set, and a bunch of others which are either empty
// or have just one item.
void ChartBase::calcDataColors()
{
    // Needs ordering, so must be a QMap, not a Qhash
    QMultiMap<const ColorizerItem*, QModelIndex> ccToIndex;

//    // fallback in case there are no defined ranges.
//    static const ColorizerItem fallback({ ContainerModel::Name, "",
//                                          QColor::fromHsv(0xd0, 0xa0, 0xd0),
//                                          QColor::fromHsv(0x00, 0xa0, 0xd0) });

    QSet<QModelIndex> inBucket;

    // Step 1: file containers into buckets they fit into, without regard
    // to length of buckets.
    const auto running = allRunning();
    for (const auto& idx : running) {
        if (const ColorizerItem* cc = cfgData().graphColorizer.find(idx)) {
            ccToIndex.insert(cc, idx);
            inBucket.insert(idx);
        }
    }

//    // Step 1.5: If some had no match, use the static fallback range.
//    for (const auto& idx : running)
//        if (!inBucket.contains(idx))
//            ccToIndex.insertMulti(&fallback, idx);

    // Step 2: attempt to equalize buckets.  This is imperfect.  Oh well.
    bool changed    = false;
    int safetyLimit = 0;
    decltype(ccToIndex.uniqueKeys()) keys = ccToIndex.uniqueKeys();

    QVector<QPair<int, const ColorizerItem*>> keyLengths;
    keyLengths.reserve(keys.size());

    for (const auto* const key : keys)
        keyLengths.push_back(qMakePair(ccToIndex.count(key), key));

    // add ones that don't (yet) have any members
    Util::Recurse(cfgData().graphColorizer, [&keyLengths, &ccToIndex](const QModelIndex& idx) {
        const ColorizerItem* cc = cfgData().graphColorizer.getItem(idx);
        if (!ccToIndex.contains(cc))
            keyLengths.push_back(qMakePair(0, cc));
        return true;
    });

    // Step 2: Reshuffle to equalize bucket sizes as far as possible.
    do {
        changed = false;
        std::sort(keyLengths.begin(), keyLengths.end());  // sort in length order

        // File things from longer to shorter buckets
        for (int longer = keyLengths.size()-1; longer > 0; --longer) {
            if (keyLengths[longer].first > keyLengths[longer-1].first) {
                for (int shorter = 0; shorter < longer; ++shorter) {
                    const ColorizerItem* ccShorter = keyLengths.at(shorter).second;
                    const ColorizerItem* ccLonger  = keyLengths.at(longer).second;
                    const auto ccLongerBegin = ccToIndex.lowerBound(ccLonger);
                    const auto ccLongerEnd = ccToIndex.upperBound(ccLonger);

                    // try each item in this bucket in reverse order, until we
                    // find one that is accepted by the shorter list.
                    for (auto it = ccLongerBegin; it != ccLongerEnd && keyLengths[longer].first > keyLengths[shorter].first; ) {
                        if (ccShorter->match(*it)) {

                            ccToIndex.insert(ccShorter, *it);
                            it = ccToIndex.erase(it);

                            if (keyLengths[shorter].first == 0)
                                keys.push_back(ccShorter);

                            ++keyLengths[shorter].first;
                            --keyLengths[longer].first;
                            changed = true;
                            break;
                        }

                        ++it;
                    }
                }
                break;
            }
        }
    } while (changed && ++safetyLimit < 1024);

    // Step 3: create reverse mapping from indexes to iterpolated colors.
    indexColorMap.clear();

    for (const auto& key : keys) {
        const auto beginIt = ccToIndex.lowerBound(key);
        const auto endIt = ccToIndex.upperBound(key);

        int index = 0;
        const auto count = float(ccToIndex.count(key));
        for (auto cc = beginIt; cc != endIt; ++cc, ++index)
            indexColorMap.insert(*cc, Util::Lerp(key->m_fgColor, key->m_bgColor, float(index) / count));
    }
}

// Figure out which color to use for charted data
const QColor& ChartBase::dataColor(const QModelIndex& idx) const
{
    const auto it = indexColorMap.find(idx);
    if (it != indexColorMap.end())
        return *it;

    static const QColor gray(Qt::gray);
    return gray;
}

void ChartBase::showLegend(bool checked)
{
    if (ui == nullptr)
        return;

    ui->showLegend->setChecked(checked);
    chart->legend()->setVisible(checked);
}

void ChartBase::wheelEvent(QWheelEvent *event)
{
    if (event->angleDelta().y() > 0) {
        zoomLevel *= zoomStep;
    } else if (event->angleDelta().y() < 0) {
        zoomLevel /= zoomStep;
    }

    // Allow either order on min/max, so subclass can decide which way zoom goes.
    zoomLevel = Util::Clamp(zoomLevel,
                            std::min(minZoom, maxZoom),
                            std::max(minZoom, maxZoom));

    zoom(zoomLevel);
}

QGroupBox& ChartBase::paneBox() const
{
    return *qobject_cast<QGroupBox*>(ui->topLayout->parent());
}

QList<QModelIndex> ChartBase::allRunning() const
{
    QList<QModelIndex> running;
    running.reserve(64);

    Util::Recurse(containerModel, [this, &running](const QModelIndex& idx) {
        if (accept(idx))
            running.push_back(idx);
        return true;
    });

    return running;
}

void ChartBase::processModelReset()
{
    MappedBase::processModelReset();

    calcDataColors();
    updateChart();
}

bool ChartBase::invalidateFilter()
{
    const bool changed = MappedBase::invalidateFilter();

    if (changed)
        updateChart();

    return changed;
}

void ChartBase::processRowsInserted(const QModelIndex& parent, int start, int end)
{
    MappedBase::processRowsInserted(parent, start, end);
    calcDataColors();
    updateChart();
}

void ChartBase::processRowsAboutToBeRemoved(const QModelIndex& parent, int start, int end)
{
    MappedBase::processRowsAboutToBeRemoved(parent, start, end);
    calcDataColors();
    updateChart();
}

bool ChartBase::supportsAction(PaneAction pa) const
{
    switch (pa) {
    case PaneAction::CopySelected:
    case PaneAction::ViewAsTree:    return false;
    default:                        return MappedBase::supportsAction(pa);
    }
}

void ChartBase::processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                                   const QVector<int>& roles)
{
    bool spansState, spansPlot;

    const bool changed = dataChangedUpdate(topLeft, bottomRight, roles, spansState, spansPlot);

    // Handle row insertion/removal based on new data/filter situation
    if (changed) {
        calcDataColors();
        updateChart();
    }

    // Update our data
    if (spansPlot) {
        for (int row = topLeft.row(); row <= bottomRight.row(); ++row) {
            const QModelIndex idx = containerModel.index(row, 0, topLeft.parent());
            updateDataValue(idx);
        }
    }
}

void ChartBase::showActionContextMenu(const QPoint& pos)
{
    paneMenu.exec(chartView.mapToGlobal(pos));
}

void ChartBase::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    MappedBase::save(settings);

    MemberSave(settings, plotColumn);
    MemberSave(settings, zoomLevel);

    SL::Save(settings, "showLegend",     ui->showLegend->isChecked());
}

void ChartBase::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    MappedBase::load(settings);

    MemberLoad(settings, plotColumn);
    MemberLoad(settings, zoomLevel);

    showLegend(SL::Load(settings, "showLegend", true));

    // Later on, someone will send newConfig, so there's only so much we have to update.
}
