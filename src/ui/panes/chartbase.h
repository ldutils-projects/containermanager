/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHARTBASE_H
#define CHARTBASE_H

#include <functional>
#include <cmath>

#include <QApplication>
#include <QtCharts/QChart>
#include <QtCharts/QLegendMarker>
#include <QStandardItemModel>
#include <QColor>
#include <QHash>
#include <QMenu>
#include <QModelIndexList>

#include <src/ui/widgets/chartviewzoom.h>
#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "mappedbase.h"

namespace Ui {
class ChartBasePane;
} // namespace Ui

using namespace QtCharts;

class MainWindow;
class ContainerModel;
enum class ContainerData;

class ChartBase : public MappedBase
{
    Q_OBJECT

public:
    ChartBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent = nullptr);
    ~ChartBase() override;

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

signals:
    void dataSelectionChanged(const QItemSelectionModel* selector,
                              const QItemSelection& /*selected*/ = QItemSelection(),
                              const QItemSelection& /*deselected*/ = QItemSelection());
    void paneCurrentContainerChanged(const QModelIndex &current);

protected slots:
    void processRowsInserted(const QModelIndex& parent, int start, int end) override;
    void processRowsAboutToBeRemoved(const QModelIndex& parent, int start, int end) override;
    void processDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight,
                                    const QVector<int>& roles) override;

    void processModelReset() override;
    virtual void showLegend(bool);    

    void showActionContextMenu(const QPoint& pos);

private slots:
    void on_ChartBasePane_toggled(bool checked) { paneToggled(checked); }

protected:
    static const constexpr char* indexProperty    = "cm-index";
    static const constexpr char* nameProperty     = "cm-name";
    static const constexpr char* legendProperty   = "cm-legend";
    static const constexpr char* dataProperty     = "cm-data";
    static const constexpr char* selectedProperty = "cm-selected";

    void newConfig() override;
    void wheelEvent(QWheelEvent*) override;
    bool invalidateFilter() override;  // return true if any changes
    bool supportsAction(PaneAction) const override;

    virtual void zoom(float) { }
    virtual void updateChart() = 0;

    virtual QString titleSuffix() const { return ""; }

    void setChartData(ModelType);
    virtual void setChartColumn(ModelType);
    virtual void setQueryColumnUi(ModelType);

    void   calcDataColors();
    const QColor& dataColor(const QModelIndex& idx) const;

    virtual void updateDataValue(QModelIndex idx) const = 0;

    void setupContextMenus(); // must call from subclass constructors

    Ui::ChartBasePane* ui;
    QChart*            chart;        // chart to display
    ChartViewZoom      chartView;    // main chart view area
    ModelType          plotColumn;   // what we'll chart
    float              zoomStep;     // zoom step multiplier
    float              zoomLevel;    // current zoom
    float              initZoom;     // init zoom level
    float              minZoom;      // min zoom level
    float              maxZoom;      // max zoom level

    // Colors subclasses should use.
    const QColor       labelHighlight;
    const QColor       labelNormal;
    const QColor       labelSelected;
    const QPen         majorGridPen;
    const QPen         minorGridPen;
    const QPen         markerNormalPen;
    const QPen         markerHighlightPen;

private:
    QModelIndexList allRunning() const;  // return list of all running containers

    void setupPlotColumnComboBox();
    void setupSubsetComboBox();
    void setupSignals();
    void setupChartView();
    QGroupBox& paneBox() const;

    // Map indicies to colors, for stable color view across filter subsetting.
    QHash<QPersistentModelIndex, QColor> indexColorMap;

    QVector<ModelType>      plotColumnEntries;
    QStandardItemModel      plotColumnModel;
    QStandardItemModel      filterColumnModel;
};

// This template class provides some common chart functionality which depends on
// the details of the specific data - e.g, a QPieSlice or a QLineSeries
template <typename DATA>
class ChartBaseData : public ChartBase
{
public:
    ChartBaseData(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent = nullptr) :
        ChartBase(mainWindow, paneClass, parent),
        prevSelectedData(nullptr)
    { }

    ~ChartBaseData() override;

protected:
    void           selectAll() override;
    void           selectNone() override;
    bool           hasSelection() const override;
    bool           isSelected(const QModelIndex& idx) const override { return isSelected(indexToData(idx)); }
    QModelIndexList getSelections() const override;

    DATA*          indexToData(const QModelIndex& idx) const;
    QModelIndex    dataToIndex(const DATA* data) const;
    QString        dataToName(const DATA* data) const;
    QLegendMarker* dataToLegend(const DATA* data) const;
    DATA*          legendToData(const QLegendMarker*) const;

    bool invalidateFilter() override;

    template <typename LIST> void update(LIST items);

    bool           isSelected(const DATA* data) const;
    virtual void   setSelected(DATA* data, bool select);
    virtual void   setDataLabelColor(DATA* /*data*/, const QColor& /*color*/) const { }
    void           setLabelColor(DATA* data, bool isHovered);
    DATA*          prevSelect();
    void           setPrevSelect(DATA* data) { prevSelectedData = data; }
    virtual void   hoverLegend(bool state) = 0;
    virtual void   selectLegend() = 0;

    void           hoverLegend(QObject*, bool state);
    void           selectLegend(QObject*);

    void           remove(const QModelIndex& idx) override { remove(indexToData(idx)); }
    virtual void   remove(DATA* data);
    bool           contains(const QModelIndex& idx) const override { return indexToData(idx) != nullptr; }
    void           clear() override { setPrevSelect(nullptr); dataMap.clear(); }
    void           insert(const QModelIndex& idx, DATA* data);

    virtual void   hoverData(DATA* data, bool state) = 0;
    virtual void   selectData(DATA* data) = 0;

    void createMarker(int pos, DATA* data);
    void updateMarker(DATA* data, const QList<QLegendMarker*>& markers, int marker,
                      const QColor& color, const QString& newLabel = "");

    template <typename GEN>
    void           selectData(DATA* data, const GEN&);

    QString        label(DATA* data, QModelIndex idx, bool legend, QString& value) const;

    QHash<QPersistentModelIndex, DATA*> dataMap; // cache indexes to data, for efficiency

    DATA*  prevSelectedData; // cache previous selection, for multi-select
};


// *************************************************************************************************
// Implementation for index to data map template methods
// *************************************************************************************************

template<typename DATA>
ChartBaseData<DATA>::~ChartBaseData()
{
    // avoids issues if hovering over legend during destruction
    const auto markers = chart->legend()->markers();
    for (auto* marker : markers)
        marker->disconnect();
}

template<typename DATA>
DATA* ChartBaseData<DATA>::indexToData(const QModelIndex& idx) const
{
    const auto it = dataMap.find(idx);
    return (it == dataMap.end()) ? nullptr : *it;
}

template<typename DATA>
QModelIndex ChartBaseData<DATA>::dataToIndex(const DATA* data) const
{
    if (data == nullptr)
        return { };

    return data->property(indexProperty).toModelIndex();
}

template<typename DATA>
QString ChartBaseData<DATA>::dataToName(const DATA* data) const
{
    if (data == nullptr)
        return "";

    return data->property(nameProperty).toString();
}

template<typename DATA>
QLegendMarker* ChartBaseData<DATA>::dataToLegend(const DATA* data) const
{
     return reinterpret_cast<QLegendMarker*>(data->property(legendProperty).toULongLong());
}

template<typename DATA>
DATA* ChartBaseData<DATA>::legendToData(const QLegendMarker* marker) const
{
     return reinterpret_cast<DATA*>(marker->property(dataProperty).toULongLong());
}

template <typename DATA>
bool ChartBaseData<DATA>::invalidateFilter()
{
    setPrevSelect(nullptr);
    return ChartBase::invalidateFilter();
}

template <typename DATA>
bool ChartBaseData<DATA>::isSelected(const DATA* data) const
{
    if (data == nullptr)
        return false;

    const QVariant selected = data->property(selectedProperty);

    return selected.isValid() && selected.toBool();
}

template <typename DATA>
QModelIndexList ChartBaseData<DATA>::getSelections() const
{
    QModelIndexList selection;

    for (auto it = dataMap.begin(); it != dataMap.end(); ++it)
        if (isSelected(*it))
            selection.push_back(it.key());

    return selection;
}

template <typename DATA>
void ChartBaseData<DATA>::setSelected(DATA* data, bool select)
{
    if (data == nullptr)
        return;

    data->setProperty(selectedProperty, select);
    setLabelColor(data, false);
}

template <typename DATA>
bool ChartBaseData<DATA>::hasSelection() const
{
    for (auto& data : dataMap)
        if (isSelected(data))
            return true;

    return false;
}

template <typename DATA>
void ChartBaseData<DATA>::setLabelColor(DATA* data, bool isHovered)
{
    if (data == nullptr)
        return;

    const bool   selected = isSelected(data);
    const QColor selectColor = selected ? labelSelected : labelNormal;
    const QColor dataLabelColor = (selected && isHovered) ? labelHighlight : selectColor;
    const QColor markerLabelColor = isHovered ? labelHighlight : selectColor;

    setDataLabelColor(data, dataLabelColor);

    if (QLegendMarker* marker = dataToLegend(data)) {
        marker->setPen(isHovered ? markerHighlightPen : markerNormalPen);
        marker->setLabelBrush(markerLabelColor);
    }
}

template <typename DATA>
DATA* ChartBaseData<DATA>::prevSelect()
{
    // Make sure it hasn't gone stale.  If so, clear it.  Note this is a value,
    // not a key, so we search linearly, but this isn't very preformance sensitive.
    if (!dataToIndex(prevSelectedData).isValid())
        return prevSelectedData = nullptr;

    return prevSelectedData;
}

template <typename DATA>
void ChartBaseData<DATA>::selectAll()
{
    for (auto& data : dataMap)
        setSelected(data, true);

    emit dataSelectionChanged(nullptr);
}

template <typename DATA>
void ChartBaseData<DATA>::selectNone()
{
    for (auto& data : dataMap)
        setSelected(data, false);

    emit dataSelectionChanged(nullptr);
}

template <typename DATA>
void ChartBaseData<DATA>::hoverLegend(QObject* object, bool state)
{
    if (auto* marker = static_cast<QLegendMarker*>(object))
        if (DATA* data = legendToData(marker))
            hoverData(data, state);
}

template <typename DATA>
void ChartBaseData<DATA>::selectLegend(QObject* object)
{
    if (auto* marker = static_cast<QLegendMarker*>(object))
        if (DATA* data = legendToData(marker))
            selectData(data);
}

template <typename DATA>
void ChartBaseData<DATA>::insert(const QModelIndex& idx, DATA* data)
{
    dataMap.insert(idx, data);
}

template <typename DATA>
void ChartBaseData<DATA>::remove(DATA* data)
{
    if (data == nullptr)
        return;

    dataMap.remove(dataToIndex(data));
}

template <typename DATA>
template <typename GEN>
void ChartBaseData<DATA>::selectData(DATA* data, const GEN& genList)
{
    if (data == nullptr)
        return;

    if (QApplication::keyboardModifiers() & Qt::ShiftModifier) {
        if (DATA* prior = prevSelect()) {
            // Range-selection
            const auto entries = genList();

            if (!entries.isEmpty()) {
                int firstIndex = entries.indexOf(prior);
                int lastIndex  = entries.indexOf(data);

                if (firstIndex > lastIndex)
                    lastIndex += entries.size();

                for (int i = firstIndex; i <= lastIndex; ++i)
                    setSelected(static_cast<DATA*>(entries[i % entries.size()]), true);
            }
        } else {
            setSelected(data, true);
        }

    } else if (QApplication::keyboardModifiers() & Qt::ControlModifier) {
        // Multi-select
        setSelected(data, !isSelected(data));
    } else {
        // Single select
        if (isSelected(data)) {
            selectNone();
        } else {
            selectNone();
            setSelected(data, true);
        }
    }

    emit dataSelectionChanged(nullptr);

    setPrevSelect(data);
}

// Initializer legend marker that goes with some newly inserted data item
template <typename DATA>
void ChartBaseData<DATA>::createMarker(int pos, DATA* data)
{
    const auto markers = chart->legend()->markers();

    if (pos < markers.size()) {
        markers[pos]->setPen(QGuiApplication::palette().color(QPalette::Base));
        markers[pos]->setProperty(dataProperty, qulonglong(data));
        connect(markers[pos], &QLegendMarker::hovered, this,
                static_cast<void (ChartBaseData::*)(bool)>(&ChartBaseData::hoverLegend), Qt::UniqueConnection);
        connect(markers[pos], &QLegendMarker::clicked, this,
                static_cast<void (ChartBaseData::*)()>(&ChartBaseData::selectLegend), Qt::UniqueConnection);
        data->setProperty(legendProperty, qulonglong(markers[pos]));
    }
}

template <typename DATA>
void ChartBaseData<DATA>::updateMarker(DATA* data, const QList<QLegendMarker*>& markers, int marker,
                                       const QColor& color, const QString& newLabel)
{
    if (marker < 0 || marker >= markers.size())
        return;

    // Legend marker
    data->setProperty(legendProperty, quint64(markers[marker]));

    if (!newLabel.isEmpty())
        markers[marker]->setLabel(newLabel);

    markers[marker]->setBrush(color);
    markers[marker]->setProperty(dataProperty, quint64(data));
}

template <typename DATA>
QString ChartBaseData<DATA>::label(DATA* data, QModelIndex idx, bool legend, QString& value) const
{
    const QString label = "<b>" + dataToName(data) + "</b>";

    if (legend && !cfgData().dataInGraphLabels)
        return label;

    if (value.isEmpty())
        value = containerModel.data(plotColumn, idx, Qt::DisplayRole).toString();

    return label + "<br>" + value;
}

#endif // CHARTBASE_H
