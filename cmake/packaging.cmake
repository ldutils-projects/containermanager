#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ContainerManager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

# Generic data
set(CPACK_GENERATOR "DEB;RPM")
set(CPACK_SOURCE_GENERATOR "External")

set(CPACK_PACKAGE_NAME "${EXENAME}")
set(CPACK_PACKAGE_VENDOR "Loopdawg Software")
set(CPACK_PACKAGE_VERSION "${PKGVERSION}")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "${EXENAME}")
set(CPACK_PACKAGE_CONTACT "loopdawg <loopdawg@see.manpage.for.email>")
set(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}/art/logos/projects/containermanager-128x128.png")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "LXC container manager using Qt")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE.md")
set(CPACK_PACKAGE_DESCRIPTION "ContainerManager is an application to manage and monitor collections of LXC
containers.  It provides charts to view dynamic container data such as memory
or CPU usage, and control facilities for collections of containers.  It can
manage containers with different guest OSs, and provides a simple parallel
console.")

# Source generation
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")
set(CPACK_SOURCE_IGNORE_FILES "/\\\\.git/" "/.*\\\\.json" "/.*\\\\.user")
set(CPACK_EXTERNAL_PACKAGE_SCRIPT "${LDUTILS_ROOT}/${CMAKE_INSTALL_DATAROOTDIR}/cmake/${LDUTILS}/pack-tar.cmake")

# DEB generation
if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
   set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "amd64")
elseif(${CMAKE_SIZEOF_VOID_P} EQUAL 4)
   set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386")
endif()

set(CPACK_DEBIAN_PACKAGE_DEPENDS "liblxc1 (>= 3.0.3), libqt5charts5 (>= 5.11.3), libqt5widgets5 (>= 5.11.3), libqt5gui5 (>= 5.11.3), libqt5core5a (>= 5.11.3), libstdc++6 (>= 8.3), libc6 (>= 2.28), libgcc-s1 | libgcc1")
set(CPACK_DEBIAN_PACKAGE_SUGGESTS "lxc")
set(CPACK_DEBIAN_PACKAGE_SECTION "admin")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
set(CPACK_DEBIAN_COMPRESSION_TYPE "xz")
set(CPACK_DEBIAN_FILE_NAME "DEB-DEFAULT")

# RPM generation
if(${CMAKE_SIZEOF_VOID_P} EQUAL 8)
   set(CPACK_RPM_PACKAGE_ARCHITECTURE "x86_64")
elseif(${CMAKE_SIZEOF_VOID_P} EQUAL 4)
   set(CPACK_RPM_PACKAGE_ARCHITECTURE "i386")
endif()

set(CPACK_RPM_PACKAGE_REQUIRES "liblxc1 >= 3.0.3, libQt5Charts5 >= 5.11.3, libQt5Widgets5 >= 5.11.3, libQt5Gui5 >= 5.11.3, libQt5Core5 >= 5.11.3, libstdc++6 >= 8.3, glibc >= 2.28, libgcc_s1")
# set(CPACK_RPM_PACKAGE_SUGGESTS "lxc")  # This is not yet supported in CPack's RPM generator
set(CPACK_RPM_PACKAGE_LICENSE "GPLv3")
set(CPACK_RPM_COMPRESSION_TYPE "xz")
set(CPACK_RPM_PACKAGE_GROUP "misc")
set(CPACK_RPM_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION}")
set(CPACK_RPM_FILE_NAME "RPM-DEFAULT")
set(CPACK_RPM_PACKAGE_AUTOREQ "no")

include(CPack)
