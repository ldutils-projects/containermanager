/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDirIterator>

#include "iconcache.h"

IconCache::IconCache(const QString& root)
{
    QDirIterator it(root, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        const QString& file = it.next();

        if (it.fileInfo().isFile()) {
            iterator iconDesc = find(file);

            if (iconDesc == end())
                iconDesc = icons.insert(resourceName(file), IconDesc());

            iconDesc->first.addFile(file);
            if (iconDesc->second.isEmpty())
                iconDesc->second = displayName(file);
        }
    }
}

IconCache::IconDesc& IconCache::operator[](const QString& name)
{
    const QString prefix = resourceName(name);

    const auto it = icons.find(prefix);

    if (it != icons.end())
        return *it;

    return *icons.insert(prefix, IconDesc());
}

const QIcon &IconCache::icon(const QString& resource) const
{
    const auto iconDesc = find(resourceName(resource));

    static const QIcon empty;

    if (iconDesc == cend())
        return empty;

    return iconDesc->first;
}

QString IconCache::resourceName(const QString& resource)
{
    QString prefix = resource;

    const int dashPos = prefix.lastIndexOf('-');

    if (dashPos > 0)
        prefix.truncate(dashPos);

    return prefix;
}

QString IconCache::displayName(const QString& resource)
{
    const int lastSlash  = resource.lastIndexOf('/');
    const int underscore = resource.indexOf('_', lastSlash);
    const int dot        = resource.lastIndexOf('.');

    if (lastSlash >= 0 && dot >= 0 && underscore < 0)
        return resource.mid(lastSlash + 1, dot - lastSlash - 1);

    if (lastSlash < 0)
        return "n/a";

    return resource.mid(lastSlash + 1, underscore - lastSlash - 1);
}
