/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef THREADBASE_H
#define THREADBASE_H

#include <functional>
#include <QRunnable>
#include <QAtomicInt>
#include <QMutex>
#include <QSet>
#include <QThreadPool>
#include <QModelIndex>

#include "src/core/outputs.h"

class ThreadSet;
class MainWindow;
enum class UiType;

// Object to put into the QT global thread pool for background updates.  The
// thread should clean up nicely after itself if requestingCancel() goes true.
class ThreadBase : public QObject, public QRunnable
{
    Q_OBJECT

public:
    ThreadBase(MainWindow&);

    ~ThreadBase() override;  // will attempt to cancel thread

    bool requestingCancel() const          { return bool(m_doCancel); }
    void requestCancel(bool cancel = true) { m_doCancel = int(cancel); }
    bool running()          const          { return bool(m_running); }
    int  rc()               const          { return m_rc; }

    static const int partialResultRc = -1;
    static const int startingRc = -2;

signals:
    void statusMessage(UiType, const QString& text) const;
    void logMessage(const QModelIndex& idx, int rc,
                    const QVector<QByteArray>& output) const;
    void cancelled(const QModelIndex& idx) const;

protected:
    void run() override { }
    void cancel();

    // Get read to run. Return false if we shouldn't.
    virtual bool prepareToRun() { return true; }

    static bool killPid(pid_t);

    static const int       killTries       = 40;
    static const uint32_t  timeout_Killing = 250000000;  // in nSec (note different units)

    // Put this on the stack in run() to manage the run/cancel variables and
    // restart behavior.  It can be a const object.
    class Runner {
    public:
        Runner(ThreadBase* thread);
        virtual ~Runner();

    private:
        ThreadBase* m_thread;
    };

    friend class ThreadSet;

    MainWindow&          m_mainWindow;
    ThreadSet*           m_threadSet;
    QAtomicInt           m_running;
    QAtomicInt           m_doCancel;
    int                  m_rc;          // return code

private:
    ThreadBase(const ThreadBase&) = delete;
    ThreadBase& operator=(const ThreadBase&) = delete;
};

class ThreadSet : public QObject
{
    Q_OBJECT

public:
    ThreadSet(QThreadPool& threadPool) :
        m_threadPool(threadPool) { }

    ~ThreadSet() override;

    // This takes ownership of the thread, and will delete it when it finishes.
    bool start(ThreadBase* thread);
    size_t count() const { return m_threads.count(); }

    bool cancelAll();   // attempt to cancel all threads in this set

    ThreadBase* contains(const std::function<bool(const ThreadBase*)>& pred) const;

signals:
    void threadAboutToStart(ThreadBase* thread) const;
    void threadStarted(ThreadBase* thread) const;
    void threadFinished(int rc) const;

private:
    ThreadSet(const ThreadSet&) = delete;
    ThreadSet& operator=(const ThreadSet&) = delete;

    void remove(ThreadBase* thread);

    friend class ThreadBase::Runner;

    QThreadPool&      m_threadPool;
    QSet<ThreadBase*> m_threads;
    QMutex            m_mutex;
};

#endif // THREADBASE_H
