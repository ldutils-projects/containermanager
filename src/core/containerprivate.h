/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERPRIVATE_H
#define CONTAINERPRIVATE_H

#include <QAtomicInteger>
#include <QVector>
#include <QByteArray>
#include <src/core/modelmetadata.h>
#include "src/core/containerid.h"

class QModelIndex;
class QVariant;
class ContainerModel;
class MainWindow;
class ThreadSet;
enum class ContainerControl;
enum class ContainerClass;
enum class ContainerState;

// This class encapsulates private knowledge about how to manage containers.
// For example, how to query data about them, start them, stop them, etc.
// This is the interface that must be implemented in src/backends/... for
// various container types, such as Lxc, or others.

class ContainerPrivate {
public:
    ContainerPrivate(MainWindow& mainWindow, ContainerModel& model, const ContainerId& id);

    virtual ~ContainerPrivate() { }

    // Update model's data for this container, at row given by index.  If force==true,
    // overwrite any cached low-frequency information.
    virtual void updateData(const QModelIndex& index, bool force) = 0;

    // Set data(!) for the container.  Return true for success.  This changes the real,
    // underlying cgroup or container data, and is only supported for some ContainerDatas.
    virtual bool set(ModelType item, const QVariant& value) = 0;

    // return false for non-containers, such as headers, etc.
    virtual bool isContainer() const = 0;

    // Container operations: start, stop etc.
    virtual bool control(const QModelIndex& idx, ContainerControl) = 0;

    // Execute command in container asynchronously, as defined by progstr and
    // the argv vector.  cmdline is for reference/display purposes.  If timeout
    // is -1, use a default.  Ditto threadSet and nullptr.
    virtual bool execute(const QModelIndex& idx, const QByteArray& cmdline,
                         const QByteArray& progstr, const QVector<QByteArray>& argvs,
                         int timeout = -1, ThreadSet* threadSet = nullptr) = 0;

    // See if this is a given thing.
    virtual bool is(const QByteArray& name, const QByteArray& path, ContainerClass cclass) const {
         return id().directory() == path && id().name() == name && id().getClass() == cclass;
    }

    const ContainerId& id() const { return m_id; }

    ContainerModel& model() { return m_model; }

protected:
    // Set the associated transition state for this control command (e.g, start -> starting)
    void setTransitionState(const QModelIndex& idx, ContainerControl ctrl);

    MainWindow&          mainWindow;
    ContainerModel&      m_model;
    const ContainerId    m_id;
    QAtomicInteger<bool> m_stateInTransition;  // true while starting/etc.

private:
    ContainerPrivate(const ContainerPrivate&) = delete;
    ContainerPrivate& operator=(const ContainerPrivate&) = delete;
};

#endif // CONTAINERPRIVATE_H
