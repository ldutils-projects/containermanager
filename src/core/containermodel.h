/*
    Copyright 2018-2023 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERMODEL_H
#define CONTAINERMODEL_H

#include <cassert>
#include <limits>
#include <functional>

#include <QtCore>
#include <QModelIndex>
#include <QVariant>

#include <src/core/treemodel.h>
#include <src/core/modelmetadata.h>
#include "containeritem.h"
#include "containerprivate.h"

class ContainerPrivate;
class ThreadSet;
enum class ContainerState;
enum class ContainerControl;

class ContainerModel : public TreeModel, public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First = 0,
        Name  = _First,   // container name
        Class,            // class from ContainerClass enum
        State,            // running, etc
        Uptime,           // uptime, sec
        Load,             // load average, float
        CpuUse,           // CPU use (TODO: units)
        CpuShares,        // CPU shares
        CfsQuota,         // CFS quota
        CfsPeriod,        // CFS period
        Memory,           // memory use
        MemoryLimit,      // memory limit
        MemPct,           // memory % usage
        KMem,             // kernel memory
        Unprivileged,     // true if unprivileged
        Ephemeral,        // true if ephemeral container
        Processes,        // process count
        Control,          // can we control it?
        HLink,            // host network link
        CLink,            // container network link
        IPV4,             // IPV4 addrs
        IPV6,             // IPV5 addrs
        TxBytes,          // transmitted bytes
        RxBytes,          // received bytes
        TxRxBytes,        // tx+rx bytes
        TxSpeed,          // transmitted bytes per second
        RxSpeed,          // received bytes per second
        TxRxSpeed,        // tx+rx bytes per second
        HWAddress,        // hardware address (MAC)
    #if USEIO
        ReadBytes,        // block read bytes
        WriteBytes,       // block write bytes
        ReadSpeed,        // block read bytes per second
        WriteSpeed,       // block write bytes per second
    #endif
        OS,               // OS type
        Kernel,           // kernel
        Machine,          // machine type (e.g, x64_64)
        Distro,           // linux distro
        Release,          // distro release
        Codename,         // distro codename
        ConfigFile,       // config file

        _Count,
    };

    ContainerModel(QObject *parent = nullptr);

    void updateData(bool force = false); // update model from external container data
    void updateList();                   // update container list if needed

    using TreeModel::findRow;
    QModelIndex findRow(const QModelIndex& parent,
                        const QVariant& data, ModelType column, int role = Qt::DisplayRole,
                        int maxDepth = std::numeric_limits<int>::max()) const override;

    bool setPrivateData(const QModelIndex &index, ContainerPrivate* data);
    ContainerPrivate* privateData(const QModelIndex& index) const;
    const ContainerPrivate* privateConstData(const QModelIndex& index) const;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    const Units& units(const QModelIndex& idx) const override;

    // return false for non-containers, such as headers, etc.
    bool isContainer(const QModelIndex& idx) const;

    // return true if the container at idx is in the given ContainerState
    bool isState(const QModelIndex& idx, ContainerState cs) const;

    using TreeModel::data;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    using TreeModel::apply;
    // Apply method to all elements of list using given args
    template <typename METHOD, typename... ARG>
    int apply(const METHOD& method, const QModelIndexList &list, const ARG&... arg) const;

    int control(ContainerControl cc, const QModelIndexList &list) const {
        return apply(&ContainerPrivate::control, list, cc);
    }

    template <typename... ARG>
    int execute(const QModelIndexList &list, const QByteArray& cmdline,
                const QByteArray& progstr, const QVector<QByteArray>& argvs,
                int timeout = -1, ThreadSet* threadSet = nullptr) const
    {
        return apply(&ContainerPrivate::execute, list, cmdline, progstr, argvs,
                     timeout, threadSet);
    }

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static bool           mdIsRate(ModelType);
    static const QString& mdRateSuffix(ModelType);
    static const Units&   mdUnits(ModelType, bool autoTime = false);
    // *** end ModelMetaData API

    static const char* cgroupController(ModelType);
    static const char* cgroupFile(ModelType);

private:
    inline ContainerItem* getItem(const QModelIndex&) const;

    // Flags for makeTooltip
    enum TTInfo {
        None,
        CgroupCpu,
        CgroupMem,
        CgroupBlkIo,
        AsyncGet,
    };

    static QString makeTooltip(const QString& description, bool editable, TTInfo = TTInfo::None);

    inline const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        return idx;
    }

    // Wraps TreeModel::apply, but calls functions on container private data
    void applyToPrivate(const std::function<bool (ContainerPrivate&, const QModelIndex&)>& fn,
                        const QModelIndex& parent = QModelIndex()) const;

    ContainerModel(const ContainerModel&) = delete;
    ContainerModel& operator=(const ContainerModel&) = delete;

    static void expandDirName(QByteArray& out, const QByteArray& in);

    void setupHeaders(); // common constructor code
};

template <typename METHOD, typename... ARG>
int ContainerModel::apply(const METHOD& method, const QModelIndexList &list, const ARG&... arg) const
{
    int successes = 0;
    for (const auto& idx : list)
        if (ContainerPrivate* privData = privateData(idx))
            successes += (privData->*method)(idx, arg...) ? 1 : 0;

    return successes;
}

#endif // CONTAINERMODEL_H
