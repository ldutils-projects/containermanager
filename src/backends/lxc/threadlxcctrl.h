/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef THREADLXCCTRL_H
#define THREADLXCCTRL_H

#include <QModelIndex>
#include <QPersistentModelIndex>

#include <src/core/modelmetadata.h>
#include "src/core/threadbase.h"


class ContainerPrivateLxc;
class MainWindow;
enum class ContainerControl;
enum class ContainerData;

class ThreadLxcCtrl : public ThreadBase
{
public:
    ThreadLxcCtrl(MainWindow& mainWindow,  ContainerPrivateLxc& privateLxc,
                  ContainerControl ctrl,
                  const QPersistentModelIndex& idx);

    bool setsThreadData(const QModelIndex& i, const QVector<ModelType>&) const {
        return idx == i;
    }

protected:
    void run() final;

private:
    // execute lxc command if the container struct is open.
    template <typename FN, typename... T>
    bool execIfLxc(const FN& fn, const T&... args);

    ContainerPrivateLxc&  privateLxc;
    ContainerControl      ctrl;
    QPersistentModelIndex idx;  // just for comparisons
};

#endif // THREADLXCCTRL_H
