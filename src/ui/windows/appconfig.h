/*
    Copyright 2018-2023 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <QDialog>
#include <QVector>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QByteArray>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QVariant>

#include <src/ui/windows/appconfigbase.h>
#include <src/ui/widgets/colordelegate.h>
#include <src/core/colorizermodel.h>
#include "src/core/cfgdata.h"
#include "src/ui/widgets/containerclassdelegate.h"

namespace Ui {
class AppConfig;
} // namespace Ui

class QAbstractButton;
class MainWindow;
class AppConfig : public AppConfigBase
{
    Q_OBJECT

public:
    explicit AppConfig(MainWindow*);
    ~AppConfig() override;

private slots:
    void on_cfgDirsAdd_clicked();
    void on_cfgDirsDelete_clicked();
    void on_cfgDirsInsert_clicked();
    void on_cfgContainerDirsListView_doubleClicked(const QModelIndex &index);
    void on_cfgContainerButtons_clicked(QAbstractButton*);
    void on_action_Next_Tab_triggered();
    void on_action_Prev_Tab_triggered();

private:
    static const int colorListName  = 0;
    static const int colorListFG    = 1;
    static const int colorListEnd   = 2;

    void setup() override;
    void setupActionIcons();
    void setupDirView();
    void setupUIColors();
    void setupUnitsInputs();
    void setupContainerColorizerEditor();
    void setupGraphColorizerEditor();
    void resizeDirColumns();
    void updateUIFromCfg() override;
    void updateCfgFromUI() override;
    const CfgData& prevCfgData() const override { return m_prevCfgData; }
    CfgData& prevCfgData() override { return m_prevCfgData; }

    void dirViewInsertAt(int row);

    QString dirViewDialog();

    CfgData                 m_prevCfgData; // initial values, for later rejection

    QHeaderView             dirHeader;
    QStandardItemModel      dirModel;

    ContainerClassDelegate  containerClassDelegate;
    ColorDelegate           colorDelegate;

    QVBoxLayout             mainLayout;

    ColorizerModel          containerColorizer;
    ColorizerModel          graphColorizer;

    MainWindow&             mainWindow;
    Ui::AppConfig*          ui;
};

#endif // APPCONFIG_H
