/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/reverseadapter.h>

#include "areachartpane.h"
#include "ui_chartbase.h"

AreaChartPane::AreaChartPane(MainWindow& mainWindow, QWidget* parent) :
    LineChartBase(mainWindow, parent)
{
    setupSignals();
    processModelReset();     // seed initial container data

    setChartData(ContainerModel::Memory); // default
}

AreaChartPane::~AreaChartPane()
{
    // The view has taken ownership of the chart, so we don't delete it
    deleteUI(ui);
}

void AreaChartPane::setupSignals()
{
    // refresh axis a short while after filter updates stop
    axisUpdateTimer.setSingleShot(true);
    connect(&axisUpdateTimer, &QTimer::timeout, this, &AreaChartPane::recalcAxes);

    // let main window know about selection changes
    connect(this, &AreaChartPane::dataSelectionChanged, &mainWindow(), &MainWindow::selectionChanged);
    connect(this, &AreaChartPane::paneCurrentContainerChanged, &mainWindow(), &MainWindow::currentChanged);

    // master data update: update all our container data at once
    connect(&mainWindow(), &MainWindow::dataUpdated, this, &AreaChartPane::dataUpdated);

    // mouse tracking
    connect(&chartView, &ChartViewZoom::mousePan,    this, &AreaChartPane::pan);
    connect(&chartView, &ChartViewZoom::mouseEndPan, this, &AreaChartPane::endPan);
}

void AreaChartPane::insert(const QModelIndex& idx, int pos)
{
    const Data* data = LineChartBase::insertImpl(idx, pos);

    if (data != nullptr) {
        connect(data,  &Data::hovered, this,
                static_cast<void(AreaChartPane::*)(const QPointF &point, bool state)>(&AreaChartPane::hoverData),
                Qt::UniqueConnection);
        connect(data,  &Data::clicked, this, 
                static_cast<void(AreaChartPane::*)(const QPointF &point)>(&AreaChartPane::selectData),
                Qt::UniqueConnection);
    }
}

void AreaChartPane::hoverData(Data* data, bool state)
{
    if (data == nullptr)
        return;

    QPen pen = data->pen();
    pen.setWidth(state ? 3 : 2);
    pen.setStyle(state ? Qt::DotLine : Qt::SolidLine);
    data->setPen(pen);

    // Increase alpha on hover.
    setAreaColor(data, state);

    // Send signal that current container changed
    if (state)
        emit paneCurrentContainerChanged(dataToIndex(data));

    setLabelColor(data, state);
}

void AreaChartPane::hoverData(const QPointF&, bool state)
{
    hoverData(qobject_cast<Data*>(QObject::sender()), state);
}

void AreaChartPane::selectData(const QPointF&)
{
    selectData(qobject_cast<Data*>(QObject::sender()));
}

AreaChartPane::Data* AreaChartPane::dataFactory() const
{
    if (Data* newData = new Data()) {
        newData->setUpperSeries(new LowerData());
        return newData;
    }

    return nullptr;
}

void AreaChartPane::visibilityChange()
{
    LineChartBase::visibilityChange();

    visibilityChange(chart->series());
}

void AreaChartPane::setSelected(Data *data, bool select)
{
    LineChartBase::setSelected(data, select);

    setAreaColor(data, false);
}

void AreaChartPane::setAreaColor(Data *data, bool hovered) const
{
    const QColor& color = data->pen().color();
    const bool selected = isSelected(data);

    int alpha = std::min(int(0xff * (float(cfgData().graphAreaOpacity) / 100.0) +
                             (hovered ? 0x50 : 0) + (selected ? 0x30 : 0)),
                         0xff);

    data->setBrush(QBrush(QColor(color.red(), color.green(), color.blue(), alpha),
                          selected ? Qt::DiagCrossPattern : Qt::SolidPattern));
}

void AreaChartPane::visibilityChange(const QList<QAbstractSeries*>& entries)
{
    if (entries.size() <= 0)
        return;

    // Each visible chart's lower series is the next lower visible chart's upper
    // series.  Note the series are in reverse order due to chart series draw
    // order limitations, so we go backwards through the list.
    LowerData* prevUpperSeries = nullptr;

    for (auto* d : Util::reverse_adapter(entries)) {
        if (auto* data = qobject_cast<Data*>(d); data != nullptr && data->isVisible()) {
            data->setLowerSeries(prevUpperSeries);
            prevUpperSeries = data->upperSeries();
        }
    }
}

// Adjust the lower series for each entry.
void AreaChartPane::updateChart()
{
    LineChartBase::updateChart();

    const auto entries = chart->series();

    // Fill color
    for (const auto& entry : entries)
        setAreaColor(dynamic_cast<Data*>(entry), false);

    visibilityChange(entries);
}
