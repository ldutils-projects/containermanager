/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <csignal>
#include <sys/wait.h>

#include <QMutexLocker>

#include "threadbase.h"
#include "src/ui/windows/mainwindow.h"

ThreadBase::ThreadBase(MainWindow &mainWindow) :
    m_mainWindow(mainWindow),
    m_threadSet(nullptr),
    m_running(0),
    m_doCancel(0),
    m_rc(0)
{
    // Hook thread up to the main window so it can report status messages
    connect(this, &ThreadBase::statusMessage, &m_mainWindow, &MainWindow::statusMessage);
}

ThreadBase::~ThreadBase()
{
    cancel();
}

void ThreadBase::cancel()
{
    if (!running())  // nothing to do if not running
        return;

    const timespec ts = { 0, 250000000 };  // wait 1/4 second
    static const int maxTries = 50;

    // We won't try forever.
    for (int tries = 0; tries < maxTries && running(); ++tries) {
        requestCancel();  // set cancel flag: run() loop must detect and abide.
        nanosleep(&ts, nullptr);
    }

    if (running())
        emit statusMessage(UiType::Error, "Unable to cancel thread");
}

bool ThreadBase::killPid(pid_t pid)
{
    int signal = SIGTERM;

    for (int tries = 0; tries < killTries; ++tries) {
        if (tries % 5 == 0)
            kill(pid, signal);

        if (tries > killTries/2)  // try a bigger hammer
            signal = SIGKILL;

        int waitrc, status;
        if ((waitrc = waitpid(pid, &status, WNOHANG)) == -1)
            return false;

        if (waitrc == pid)
            return true;

        const timespec ts = { 0, timeout_Killing };
        nanosleep(&ts, nullptr);
    }

    // If we get here, we failed to kill the process.  Shrug and return.
    return false;
}

ThreadBase::Runner::Runner(ThreadBase* thread) :
    m_thread(thread)
{
    m_thread->cancel();       // cancel any existing one.

    m_thread->m_running  = 1; // using atomicInt as a bool: 1->true
    m_thread->m_doCancel = 0; // using atomicInt as a bool: 0->false
}

ThreadBase::Runner::~Runner()
{
    m_thread->m_running = 0;

    if (m_thread->m_threadSet != nullptr)
        m_thread->m_threadSet->remove(m_thread);
}

bool ThreadSet::start(ThreadBase* thread)
{
    QMutexLocker lock(&m_mutex);

    // setup before we kick off the thread.
    if (!thread->prepareToRun())
        return false;

    thread->m_threadSet = this;
    thread->setAutoDelete(false);
    m_threads.insert(thread);

    emit threadAboutToStart(thread);
    m_threadPool.start(thread);
    emit threadStarted(thread);

    return true;
}

bool ThreadSet::cancelAll()
{
    int count = 0;

    while (!m_threads.empty()) {
        for (const auto& thread : m_threads)
            thread->requestCancel();

        if (++count > 50)
            break;

        const timespec ts = { 0, 250000000 };
        nanosleep(&ts, nullptr);
    }

    return m_threads.empty();
}

ThreadSet::~ThreadSet()
{
    for (const auto& thread : m_threads) {
        thread->m_threadSet = nullptr; // don't recursively delete
        delete thread;
    }
}

void ThreadSet::remove(ThreadBase *thread)
{
    QMutexLocker lock(&m_mutex);
    thread->m_threadSet = nullptr; // don't recursively delete
    m_threads.remove(thread);

    emit threadFinished(thread->rc());

    delete thread;
}

ThreadBase* ThreadSet::contains(const std::function<bool(const ThreadBase*)>& pred) const
{
    for (const auto& thread : m_threads)
        if (pred(thread))
            return thread;

    return nullptr;
}

