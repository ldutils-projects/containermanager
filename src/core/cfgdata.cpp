/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <pwd.h>
#include <unistd.h>
#include <sys/types.h>
#include <cassert>

#include <QGuiApplication>
#include <QThread>  // just for the idealThreadCount static fn

#include <src/util/util.h>
#include <src/util/roles.h>
#include <src/util/icons.h>

#include "app.h"
#include "containermodel.h"
#include "containerclass.h"

CfgDir::CfgDir(const QString &directory, ContainerClass containerClass, const QString &name, const QByteArray &uid, const QByteArray &gid) :
    directory(directory),
    displayName(name.size() == 0 ? genDisplayName(directory, containerClass) : name),
    containerClass(containerClass),
    uid(uid),
    gid(gid)
{ }

CfgDir::CfgDir(const QStandardItemModel &m, int row) :
    CfgDir(SIM::data(m, row, 0).toString(),       // path
           ContainerClassBase::find(SIM::data(m, row, 1).toString())->containerClass,
           SIM::data(m, row, 2).toString(),       // display name
           SIM::data(m, row, 3).toByteArray(),    // UID
           SIM::data(m, row, 4).toByteArray())    // GID
{
}

void CfgDir::set(QStandardItemModel &m, int row) const
{
    int col = 0;

    m.setItem(row, col++, new QStandardItem(directory));
    m.setItem(row, col++, new QStandardItem(ContainerClassBase::find(containerClass)->name));
    m.setItem(row, col++, new QStandardItem(displayName));
    m.setItem(row, col++, new QStandardItem(QString(uid)));
    m.setItem(row, col++, new QStandardItem(QString(gid)));
}

QByteArray CfgDir::expandedDir() const
{
    return Util::ExpandDirName(directory);
}

// Create friendly display name
QString CfgDir::genDisplayName(const QString& directory, ContainerClass containerClass)
{
    const auto userTypeName = [containerClass](struct passwd *pw) {
        assert(pw != nullptr);  // checked by caller
        return QString(pw->pw_name) + " " + ContainerClassBase::find(containerClass)->name;
    };

    // this user
    if (directory.startsWith("$HOME/") || directory.startsWith("${HOME}/") || directory.startsWith("~/")) {
        struct passwd *pw = getpwuid(geteuid());
        if (pw != nullptr)
            return userTypeName(pw);
    }

    // some other user
    if (directory.startsWith("~")) {
        const int slashPos = directory.indexOf('/');
        if (slashPos > 1) {
            struct passwd* pw = getpwnam(directory.mid(1, slashPos-1).toUtf8().data());
            if (pw != nullptr)
                return userTypeName(pw);
        }
    }

    // moderate length path
    if (directory.length() < 30)
        return directory;

    // squeeze middle out of path, replace with "/.../"
    const int firstSlash = directory.indexOf('/');
    const int lastSlash  = directory.lastIndexOf('/');

    if (firstSlash > 0 && lastSlash > firstSlash)
        return directory.mid(0, firstSlash+1) + "..." + directory.mid(lastSlash);

    if (firstSlash == 0 && lastSlash > firstSlash) {
        const int secondSlash = directory.indexOf('/', firstSlash+1);
        if (lastSlash > secondSlash)
            return directory.mid(0, secondSlash+1) + "..." + directory.mid(lastSlash);
    }

    // We didn't know what to do.  Just return it.
    return directory;
}

void CfgDir::save(QSettings& settings) const
{
    MemberSave(settings, directory);
    MemberSave(settings, displayName);
    MemberSave(settings, containerClass);
    MemberSave(settings, uid);
    MemberSave(settings, gid);
}

void CfgDir::load(QSettings& settings)
{
    MemberLoad(settings, directory);
    MemberLoad(settings, displayName);
    MemberLoad(settings, containerClass);
    MemberLoad(settings, uid);
    MemberLoad(settings, gid);
}

CfgData::CfgData() :
    CfgDataBase(currentCfgDataVersion),
    shells({
        { "direct",  "" },
        { "sh",      "sh -c %s" },
        { "ash",     "ash -c %s" },
        { "bash",    "bash -c %s" },
        { "csh",     "csh -c %s" },
        { "dash",    "dash -c %s" },
        { "ksh",     "ksh -c %s" },
        { "pdksh",   "pdksh -c %s" },
        { "tcsh",    "tcsh -c %s" },
        { "zsh",     "zsh -c %s" } }),
    dataUpdateInterval(2),
    listUpdateInterval(15),
    dataThreadTimeout(15),
    cmdThreadTimeout(60),
    viewAsTree(true),
    copyOnlyContainers(true),
    ctrlThreadCount(QThread::idealThreadCount()),
    dataThreadCount(QThread::idealThreadCount()),
    cmdThreadCount(QThread::idealThreadCount()),
    directCgroups(true),
    unitsNetwork(Format::AutoBinary, 2, false),
    unitsMemory(Format::AutoBinary,  2, false),
    unitsUptime(Format::DurDHMS,     0, false),
    unitsCpuUse(Format::DurDHMS,     0, false),
    unitsPercent(Format::Percent,    2, false),
    graphDataPoints(600),
    graphLineWidth(2),
    graphAreaOpacity(15),
    defaultGraphMin(2),
    defaultGraphSec(0),
    dataInGraphLabels(true),
    consoleHistoryLines(128),
    commandHistoryLines(32),
    consoleFont("DejaVu Sans Mono", 8),
    iconDir(Icons::get("folder")),
    iconCheck(Icons::get("checkmark")),
    containerColorizer(&app().containerModel()),
    graphColorizer(&app().containerModel())
{
    defaultContainerColorizers();
    defaultGraphColorizers();
    uniquifyDirectories();
}

void CfgData::defaultContainerColorizers()
{
    const auto addLogo = [&](const QString& name, ModelType data = ContainerModel::Distro,
            const QString& pattern = "",
            const QString& iconPrefix = ":art/logos/distros")
    {
        const QString path = iconPrefix + "/" + name + "_Logo-64x64.png";

        containerColorizer.appendRow({ data,
                                       ContainerModel::mdName(data) + " == " + (pattern.isEmpty() ? name : pattern),
                                       QVariant(), QVariant(),
                                       path });
    };

    const auto addLogos = [this, addLogo](const QStringList& names) {
        for (const auto& name : names)
            addLogo(name);
    };

    const auto addLoad = [&](const QColor& fg, const float load) {
        containerColorizer.appendRow({ ContainerModel::Load,
                                       (load > 0) ? (QString("Load < ") + QString::number(load)) :
                                                    "Load >= 7.0",
                                       fg });
    };

    // Default custom colors for various states
    for (ContainerState s = ContainerState::_Begin; s < ContainerState::_Count; Util::inc(s))
        containerColorizer.appendRow({ ContainerModel::State,
                                       QString("State == ") + ContainerStateInfo::text(s),
                                       ContainerStateInfo::color(s),
                                       QVariant(), // BG Color
                                       QVariant(), // icon
                                       true });    // case

    // Default custom colors for container loads
    addLoad(QColor::fromRgb(0x20, 0x80, 0x20), 0.3);
    addLoad(QColor::fromRgb(0x50, 0xa0, 0x20), 0.6);
    addLoad(QColor::fromRgb(0xa0, 0xd0, 0x20), 1.0);
    addLoad(QColor::fromRgb(0xd0, 0xd0, 0x20), 3.0);
    addLoad(QColor::fromRgb(0xd0, 0xa0, 0x20), 5.0);
    addLoad(QColor::fromRgb(0xd0, 0x50, 0x20), 7.0);
    addLoad(QColor::fromRgb(0xd0, 0x00, 0x20), -1);

    addLogos({ "Alpine", "Arch", "Bodhi", "CentOS", "Debian", "Devuan", "Elementary", "Fedora", "Gentoo", "Kali",
               "KateOS", "Knoppix", "Mint", "Musix", "OpenSUSE", "PCLinuxOS", "Pear", "PostmarketOS",
               "Puppy", "Qubes", "Redhat", "Sabayon", "Slackware", "Sparky", "Ubuntu", "Ututo", "Viperr",
               "Whonix", "Zorin" });

    addLogo("Tux", ContainerModel::OS, "Linux", ":art/logos/projects");
}

void CfgData::defaultGraphColorizers()
{
    // Default custom graph colors
    for (int hue = 0xee; hue >= 0x00; hue -= 0x22)
        graphColorizer.appendRow({ ContainerModel::Name, "",
                                   QColor::fromHsv(hue, 0xc0, 0xe0),
                                   QColor::fromHsv(hue, 0xc0, 0x40) });
}

void CfgData::uniquifyDirectories()
{
    std::sort(m_directories.begin(), m_directories.end());

    m_directories.erase(std::unique(m_directories.begin(), m_directories.end(), CfgDir::dirNameEqual),
                        m_directories.end());
}

const QList<CfgDir>&CfgData::directories() const
{
    if (m_directories.isEmpty())
        // Let each container class add its default container dirs
        for (int cc=0; cc < int(ContainerClass::_Count); ++cc)
            ContainerClassBase::find(ContainerClass(cc))->defaultDirs(m_directories);

    return m_directories;
}

void CfgData::updateDirectories(const QStandardItemModel& dirModel)
{
    m_directories.clear();
    for (int row = 0; row < dirModel.rowCount(); ++row)
        m_directories.append(CfgDir(dirModel, row));
    uniquifyDirectories();
}

const Units& CfgData::units(ModelType mt, bool autoTime)
{
    return ContainerModel::mdUnits(mt, autoTime);
}

void CfgData::reset()
{
    *this = CfgData();
}

void CfgData::save(QSettings& settings) const
{
    SL::Save(settings, versionKey, version());

    CfgDataBase::save(settings);

    SL::Save(settings, "directories", m_directories);

    // MemberSave(settings, shells);  // TODO: ...
    MemberSave(settings, containerColorizer);
    MemberSave(settings, graphColorizer);
    MemberSave(settings, dataUpdateInterval);
    MemberSave(settings, listUpdateInterval);
    MemberSave(settings, dataThreadTimeout);
    MemberSave(settings, cmdThreadTimeout);
    MemberSave(settings, viewAsTree);
    MemberSave(settings, copyOnlyContainers);
    MemberSave(settings, ctrlThreadCount);
    MemberSave(settings, dataThreadCount);
    MemberSave(settings, cmdThreadCount);
    MemberSave(settings, directCgroups);

    MemberSave(settings, unitsNetwork);
    MemberSave(settings, unitsMemory);
    MemberSave(settings, unitsUptime);
    MemberSave(settings, unitsCpuUse);
    MemberSave(settings, unitsPercent);

    MemberSave(settings, graphDataPoints);
    MemberSave(settings, graphLineWidth);
    MemberSave(settings, graphAreaOpacity);
    MemberSave(settings, defaultGraphMin);
    MemberSave(settings, defaultGraphSec);
    MemberSave(settings, dataInGraphLabels);

    MemberSave(settings, consoleHistoryLines);
    MemberSave(settings, commandHistoryLines);

    MemberSave(settings, consoleFont);
}

void CfgData::load(QSettings& settings)
{
    reset();  // defaults for things not read

    // Don't load incompatible versions
    if (SL::Load(settings, versionKey, 0) != Settings::version())
        return;

    CfgDataBase::load(settings);
    
    SL::Load(settings, "directories", m_directories);

    // MemberLoad(settings, shells);  // TODO: ...
    MemberLoad(settings, containerColorizer);
    MemberLoad(settings, graphColorizer);
    MemberLoad(settings, dataUpdateInterval);
    MemberLoad(settings, listUpdateInterval);
    MemberLoad(settings, dataThreadTimeout);
    MemberLoad(settings, cmdThreadTimeout);
    MemberLoad(settings, viewAsTree);
    MemberLoad(settings, copyOnlyContainers);
    MemberLoad(settings, ctrlThreadCount);
    MemberLoad(settings, dataThreadCount);
    MemberLoad(settings, cmdThreadCount);
    MemberLoad(settings, directCgroups);

    MemberLoad(settings, unitsNetwork);
    MemberLoad(settings, unitsMemory);
    MemberLoad(settings, unitsUptime);
    MemberLoad(settings, unitsCpuUse);
    MemberLoad(settings, unitsPercent);

    MemberLoad(settings, graphDataPoints);
    MemberLoad(settings, graphLineWidth);
    MemberLoad(settings, graphAreaOpacity);
    MemberLoad(settings, defaultGraphMin);
    MemberLoad(settings, defaultGraphSec);
    MemberLoad(settings, dataInGraphLabels);

    MemberLoad(settings, consoleHistoryLines);
    MemberLoad(settings, commandHistoryLines);

    graphDataPoints     = Util::Clamp(graphDataPoints, 4, 65536);
    consoleHistoryLines = Util::Clamp(consoleHistoryLines, 4, 4096);
    commandHistoryLines = Util::Clamp(commandHistoryLines, 4, 256);

    if (settings.contains("consoleFont"))
        MemberLoad(settings, consoleFont);

    updateFormat();
}

void CfgShell::save(QSettings& settings) const
{
    MemberSave(settings, name);
    MemberSave(settings, exec);
}

void CfgShell::load(QSettings& settings)
{
    MemberLoad(settings, name);
    MemberLoad(settings, exec);
}

// If the CfgData loaded was old, we might want to update some values.
void CfgData::updateFormat()
{
    priorCfgDataVersion = cfgDataVersion; // save for other classes to look at on load

    if (cfgDataVersion == currentCfgDataVersion)  // up to date
        return;

    // Format updating here if necessary.

    cfgDataVersion = currentCfgDataVersion;
}
