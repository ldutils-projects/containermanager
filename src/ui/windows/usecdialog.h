/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef USECDIALOG_H
#define USECDIALOG_H

#include <QDialog>

namespace Ui {
class USecDialog;
} // namespace Ui

class USecDialog : public QDialog
{
    Q_OBJECT

public:
    explicit USecDialog(int initAbs, int absMax, bool hideRel, QWidget *parent = nullptr);
    ~USecDialog() override;

    int value() const { return currentValue; }

public slots:
    void setValue(int absValue);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void relValueChange(int);
    void absValueChange(int);
    void modeChange(bool);

private:
    Ui::USecDialog *ui;

    int absToRel(int abs) const { return float(abs) * 100.0 / float(absMax); }
    int relToAbs(int rel) const { return float(rel) * float(absMax) / 100.0; }

    const int absMax;
    const int initAbs;
    int currentValue; // -1 for unlimited, else absolute
};

#endif // USECDIALOG_H
