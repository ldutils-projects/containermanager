/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <csignal>
#include <array>

#include <src/core/uicolormodel.h>
#include <src/util/util.h>
#include "src/core/containermodel.h"
#include "src/util/time.h"
#include "threaddata.h"

ThreadData::ThreadData(MainWindow& mainWindow, ContainerPrivate* privateData,
                       int timeout, const QByteArray &progStr, const QVector<QByteArray>& argvStr) :
    ThreadBase(mainWindow),
    m_finishCommand(FinishCommand::None),
    m_progStr(progStr),
    m_argvStr(argvStr),
    m_timeout(timeout),
    m_output(int(Out::_Count)),
    m_privateData(privateData)
{
    m_argv.reserve(argvStr.size()+1);

    // Set up pointers to make C interfaces happy
    for (const auto& a : m_argvStr)
        m_argv.push_back(a.data());

    m_argv.append(nullptr);  // C API wants trailing null on arg vector
}

// Perform any requested completion command
ThreadData::~ThreadData()
{
    switch (m_finishCommand) {
    case FinishCommand::Data:    [[fallthrough]];
    case FinishCommand::Words:   [[fallthrough]];
    case FinishCommand::EnvFile:
        if (m_rc != 0 || m_columns.isEmpty())  // nothing to do
            return;
    default:
        break;
    }

    switch (m_finishCommand) {
    case FinishCommand::Data:     setData();    break;
    case FinishCommand::Words:    setWords();   break;
    case FinishCommand::EnvFile:  setEnvFile(); break;
    case FinishCommand::Log:      [[fallthrough]];
    case FinishCommand::LogAsync: setLog();     break;
    default: break;
    }

    if (requestingCancel())
        emit cancelled(m_index);
}

void ThreadData::setData()
{
    ContainerModel& model = m_privateData->model();

    if (m_columns.empty())
        model.setDataFromThread(m_index, Util::RemoveNewline(m_output[Out::Stdout]), Util::RawDataRole);
    else
        model.setDataFromThread(model.rowSibling(m_columns[0], m_index), Util::RemoveNewline(m_output[Out::Stdout]),
                Util::RawDataRole);
}

void ThreadData::setWords()
{
    ContainerModel& model = m_privateData->model();

    // break out words
    Util::RemoveNewline(m_output[Out::Stdout]);
    const QList<QByteArray> words = m_output[Out::Stdout].simplified().split(' ');
    int word = 0;
    for (ModelType s : m_columns)
        if (word < words.size())
            model.setDataFromThread(model.rowSibling(s, m_index), words[word++], Util::RawDataRole);
}

void ThreadData::setEnvFile()
{
    ContainerModel& model = m_privateData->model();

    // look for env-like key=value or key="value" entries matching envKeys
    const QList<QByteArray> lines = m_output[Out::Stdout].split('\n');

    int keyEnd;   // end of key in line
    int valStart; // stat of value
    int valEnd;   // end of value

    for (const auto& line : lines) {
        if (line.size() >= 3) {  // minimum for one char key = one char val
            for (keyEnd = 0;
                 keyEnd < line.size() && !bool(isspace(line[keyEnd])) && line[keyEnd] != '=';
                 ++keyEnd);

            // TODO: can probably refactor this little key=val parser with
            // LxcUtils::readConfig or similar

            for (int k = 0; k < m_envKeys.size(); ++k) { // no range-for, since we want the index
                if (k >= m_columns.size())  // not enough ContainerDatas
                    break;

                if (m_envKeys[k].indexIn(line) < 0)  // doesn't match regexp
                    continue;

                if (m_envKeys[k].matchedLength() != keyEnd) // doesn't match whole key
                    continue;

                for (valStart = keyEnd + 1; bool(isspace(line[valStart])); ++valStart);
                for (valEnd = line.size() - 1; bool(isspace(line[valEnd])); --valEnd);

                if (line[valStart] == '"') {
                    ++valStart; // skip the quote
                    valEnd = line.lastIndexOf('"') - 1;
                }

                if (valEnd < 0 || valStart > valEnd) // malformed
                    continue;

                model.setDataFromThread(model.rowSibling(m_columns[k], m_index),
                                        line.mid(valStart, valEnd-valStart+1),
                                        Util::RawDataRole);

                break;  // don't keep searching through keys
            }
        }
    }
}

void ThreadData::setLog()
{
    // Log any output remainder, and the result code.
    emit logMessage(m_index, m_rc, m_output);
}

ThreadData* ThreadData::requestData(const QModelIndex &idx,
                                    const decltype(ThreadData::m_columns)& cols)
{
    m_finishCommand = FinishCommand::Data;
    m_index         = idx;
    m_columns       = cols;
    return this;
}

ThreadData* ThreadData::requestWords(const QModelIndex &idx,
                                     const decltype(ThreadData::m_columns)& cols)
{
    m_finishCommand = FinishCommand::Words;
    m_index         = idx;
    m_columns       = cols;
    return this;
}

ThreadData* ThreadData::requestEnvFile(const QModelIndex &idx,
                                       const decltype(ThreadData::m_envKeys)& keys,
                                       const decltype(ThreadData::m_columns)& cols)
{
    m_finishCommand = FinishCommand::EnvFile;
    m_index         = idx;
    m_envKeys       = keys;
    m_columns       = cols;
    return this;
}

ThreadData* ThreadData::requestLog(const QModelIndex& idx, const QByteArray& cmdline)
{
    m_finishCommand = FinishCommand::Log;
    m_index         = idx;
    m_output[Out::Cmdline] = cmdline;
    return this;
}

ThreadData* ThreadData::requestLogAsync(const QModelIndex& idx, const QByteArray& cmdline)
{
    m_finishCommand = FinishCommand::LogAsync;
    m_index         = idx;
    m_output[Out::Cmdline] = cmdline;
    return this;
}

bool ThreadData::setsThreadData(const QModelIndex& idx,
                                const decltype(ThreadData::m_columns)& cols) const
{
    switch (m_finishCommand) {
    case FinishCommand::Data:
        return m_index == idx;

    case FinishCommand::Words:
    case FinishCommand::EnvFile:
        {
            for (ModelType s : m_columns)
                if (cols.contains(s))
                    return true;

            return false;
        }

    default:
        return false;
    }
}

// Convenience: capture partial output up to last newline (for signalling),
// and restore any post-trailing-newline data on destruction.
class PartialOutput {
public:
    PartialOutput(QByteArray& output) : output(output) {
        if (!output.isEmpty()) {
            const int lastNl = output.lastIndexOf('\n');
            if (lastNl >= 0) {
                partial = output.mid(lastNl+1);
                output.resize(lastNl+1);
            }
        }
    }

    ~PartialOutput() { output.swap(partial); }

private:
    QByteArray& output;
    QByteArray  partial;
};

void ThreadData::sendPartialOutput()
{
    const PartialOutput partOut(m_output[Out::Stdout]);
    const PartialOutput partErr(m_output[Out::Stderr]);

    // Make deep copy of data to hand partial result to other threads.
    decltype(m_output) newOutput;
    newOutput.resize(m_output.size());
    for (int x = 0; x < m_output.size(); ++x)
        newOutput[x] = m_output[x].data();

    emit logMessage(m_index, partialResultRc, newOutput);
}

// In LXC 5.0, there are difficulties running attach() from a thread. This did not
// used to be the case in earlier LXC's. To work around the issue, we launch the
// LXC attached process from our main thread, and only read the data from a
// sub-thread. This function starts the attach() from our main thread.
bool ThreadData::prepareToRun()
{
    // Reset
    m_output[Out::Stdout].clear();
    m_output[Out::Stderr].clear();
    m_rc = -1;

    if (!m_stdoutfd.ok() || !m_stderrfd.ok())
        return false;

    if (!startContainerPid(m_stdoutfd, m_stderrfd, m_pid)) {
        emit statusMessage(UiType::Error, "Failed to start container process");
        m_rc = 5;
        return false;
    }

    return true;
}

void ThreadData::run()
{
    const Time timeout_Limit(m_timeout, 0);  // total time before task is killed

    const Runner runner(this);  // This manages the thread on the threadset

    const Time startTime = Time::now();  // we'll destroy the thread if it doesn't complete after a while.

    std::array<char, 1024> buf;

    fd_set rfds;
    FD_ZERO(&rfds);
    bool exited = false;

    emit logMessage(m_index, startingRc, m_output);

    // Loop to process output and wait for PID to terminate
    while (true) {
        // Someone requested cancellation. Kill the PID and return.
        if (requestingCancel()) {
            killPid(m_pid); // If this fails, nothing we can really do about that.
            return;
        }

        FD_SET(m_stdoutfd[0], &rfds);
        FD_SET(m_stderrfd[0], &rfds);

        struct timeval timeout = { 0, decltype(timespec().tv_nsec)(exited ? timeout_Done : timeout_Running) };

        const int fdcount = select(std::max(m_stdoutfd[0], m_stderrfd[0]) + 1, &rfds, nullptr,
                                   nullptr, &timeout);

        if (fdcount < 0) {
            // just try again.
        }

        if (fdcount > 0) {
            // read stdout
            if (FD_ISSET(m_stdoutfd[0], &rfds)) {
                const int bytes = read(m_stdoutfd[0], buf.data(), sizeof(buf));
                m_output[Out::Stdout].append(buf.data(), bytes);
            }

            // read stderr
            if (FD_ISSET(m_stderrfd[0], &rfds)) {
                const int bytes = read(m_stderrfd[0], buf.data(), sizeof(buf));
                m_output[Out::Stderr].append(buf.data(), bytes);
            }

            // Asynchronous output: send as much as we've collected, but only in whole-line chunks.
            if (m_finishCommand == FinishCommand::LogAsync)
                sendPartialOutput();
        }
        // Once process is done, read test of output without re-checking waitpid
        if (!exited) {
            int status, waitrc;

            waitrc = waitpid(m_pid, &status, WNOHANG);

            if (waitrc == m_pid && WIFEXITED(status)) {
                exited = true;
                m_rc = WEXITSTATUS(status); // remember rc, since we may have more data to read
            }

            // It's had long enough.  If it's still running, cancel it.
            if ((Time::now() - startTime) > timeout_Limit) {
                emit statusMessage(UiType::Warning, "Data thread timed out while waiting for container reply");
                requestCancel();    // TODO: count these, maybe stop trying if too many
            }
        }

        // Primary exit, if process is done and we've read all the output.
        if (exited && fdcount <= 0)
            return;
    }
}

