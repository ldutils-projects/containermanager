/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>

#include <src/util/ui.h>

#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    AboutBase(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    setupPrevNext(ui->aboutTabs, ui->action_Next_Tab, ui->action_Prev_Tab);
    setupAppTitle();
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::setupAppTitle()
{
    Util::SetupWhatsThis(this);

    ui->aboutCMTitle->setText(QApplication::applicationDisplayName());
    ui->aboutCMVersion->setText(QApplication::applicationVersion());
}
