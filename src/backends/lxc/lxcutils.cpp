/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "lxcutils.h"

// Find value for 'key = val' config item, return pointer to the val, or end of line of not found.
const char *LxcUtils::configLineValue(const char *cfgLine)
{
    if (cfgLine == nullptr)
        return "";

    while (*cfgLine != '\0' && *cfgLine != '=')
        ++cfgLine;

    if (*cfgLine == '=')
        ++cfgLine;

    while (*cfgLine == ' ')
        ++cfgLine;

    return cfgLine;
}
