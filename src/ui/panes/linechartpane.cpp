/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "linechartpane.h"
#include "ui_chartbase.h"

LineChartPane::LineChartPane(MainWindow& mainWindow, QWidget* parent) :
    LineChartBase(mainWindow, parent)
{
    setupSignals();
    processModelReset();     // seed initial container data

    setChartData(ContainerModel::Memory); // default
}

LineChartPane::~LineChartPane()
{
    // The view has taken ownership of the chart, so we don't delete it
    deleteUI(ui);
}

void LineChartPane::setupSignals()
{
    // refresh axis a short while after filter updates stop
    axisUpdateTimer.setSingleShot(true);
    connect(&axisUpdateTimer, &QTimer::timeout, this, &LineChartPane::recalcAxes);

    // let main window know about selection changes
    connect(this, &LineChartPane::dataSelectionChanged, &mainWindow(), &MainWindow::selectionChanged);
    connect(this, &LineChartPane::paneCurrentContainerChanged, &mainWindow(), &MainWindow::currentChanged);

    // master data update: update all our container data at once
    connect(&mainWindow(), &MainWindow::dataUpdated, this, &LineChartPane::dataUpdated);

    // mouse tracking
    connect(&chartView, &ChartViewZoom::mousePan,    this, &LineChartPane::pan);
    connect(&chartView, &ChartViewZoom::mouseEndPan, this, &LineChartPane::endPan);
}

void LineChartPane::hoverData(Data *data, bool state)
{
    if (data == nullptr)
        return;

    QPen pen = data->pen();
    pen.setWidth(state ? 3 : 2);
    pen.setStyle(state ? Qt::DotLine : Qt::SolidLine);
    data->setPen(pen);

    // Send signal that current container changed
    if (state)
        emit paneCurrentContainerChanged(dataToIndex(data));

    setLabelColor(data, state);
}

