/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERDELEGATEBASE_H
#define CONTAINERDELEGATEBASE_H

#include <functional>

#include <QStyledItemDelegate>
#include <QDialog>

class QItemSelectionModel;

class ContainerDelegateBase : public QStyledItemDelegate
{
public:
    ContainerDelegateBase(QObject* parent = nullptr, bool popupEditor = true);

    void setSelector(const QItemSelectionModel* s) { selector = s; }

protected:
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override = 0;
    void setEditorData(QWidget */*editor*/, const QModelIndex &/*index*/) const override { }
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override = 0;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    // Convenience function to set popup dialog flags
    static QDialog* setPopup(QDialog* editor);

    void updateResultFromEditor(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index,
                                const std::function<QVariant(bool& accepted)>& fetchFromEditor) const;

    // Convenience function to set given column of all selected rows
    template <typename DIALOG>
    void updateResultsFromDialog(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

    template <typename WIDGET>
    void updateResultsFromWidget(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

protected:
    const QItemSelectionModel* selector;

private:
    const bool popupEditor;
};

template <typename DIALOG>
inline void ContainerDelegateBase::updateResultsFromDialog(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    const DIALOG* dialog = dynamic_cast<DIALOG*>(editor);

    if (dialog == nullptr)
        return;

    updateResultFromEditor(editor, model, index, [dialog](bool& accepted) {
        accepted = dialog->result() == QDialog::Accepted;
        return dialog->value();
    });
}

template <typename WIDGET>
void ContainerDelegateBase::updateResultsFromWidget(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    const WIDGET* widget = dynamic_cast<WIDGET*>(editor);

    if (widget == nullptr)
        return;

    updateResultFromEditor(editor, model, index, [widget](bool& accepted) {
        accepted = true;
        return widget->value();
    });
}


#endif // CONTAINERDELEGATEBASE_H
