/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QGuiApplication>
#include <QPen>
#include <QPalette>

#include "src/util/util.h"
#include "containerstate.h"

QString ContainerStateInfo::text(ContainerState s)
{
    switch (s) {
    case ContainerState::Unknown:   return QObject::tr("Unknown");
    case ContainerState::Running:   return QObject::tr("Running");
    case ContainerState::Stopped:   return QObject::tr("Stopped");
    case ContainerState::Frozen:    return QObject::tr("Frozen");
    case ContainerState::Defined:   return QObject::tr("Defined");
    case ContainerState::Starting:  return QObject::tr("Starting");
    case ContainerState::Stopping:  return QObject::tr("Stopping");
    case ContainerState::Rebooting: return QObject::tr("Rebooting");
    case ContainerState::Freezing:  return QObject::tr("Freezing");
    case ContainerState::Thawing:   return QObject::tr("Thawing");
    case ContainerState::Executing: return QObject::tr("Executing Cmd");
    case ContainerState::Creating:  return QObject::tr("Creating");
    case ContainerState::Removing:  return QObject::tr("Removing");
    case ContainerState::_Count:    break;
    }

    assert(0 && "Unknown ContainerState value");
    return QObject::tr("Unknown");
}

QColor ContainerStateInfo::color(ContainerState s)
{
    switch (s) {
    case ContainerState::Unknown:   return QColor::fromRgb(120, 80, 0);
    case ContainerState::Running:   return QColor::fromRgb(50, 200, 50);
    case ContainerState::Stopped:   return QColor::fromRgb(200, 50, 50);
    case ContainerState::Frozen:    return QColor::fromRgb(50, 120, 255);
    case ContainerState::Starting:  return QColor::fromRgb(70, 128, 20);
    case ContainerState::Stopping:  return QColor::fromRgb(200, 100, 20);
    case ContainerState::Rebooting: return QColor::fromRgb(200, 200, 40);
    case ContainerState::Freezing:  return QColor::fromRgb(100, 50, 200);
    case ContainerState::Thawing:   return QColor::fromRgb(20, 200, 200);
    case ContainerState::Executing: return QColor::fromRgb(70, 255, 80);
    case ContainerState::Creating:  return QColor::fromRgb(80, 255, 80);
    case ContainerState::Removing:  return QColor::fromRgb(255, 0, 0);
    case ContainerState::Defined:   return QGuiApplication::palette().text().color();
    case ContainerState::_Count:    break;
    }

    assert(0 && "Unknown ContainerState value");
    return QGuiApplication::palette().text().color();
}

