/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <lxc/lxccontainer.h>

#include "src/ui/windows/mainwindow.h"
#include "src/util/pipe.h"
#include "src/util/setid.h"
#include "containerprivatelxc.h"
#include "threadlxcdata.h"

bool ThreadLxcData::startContainerPid(const Pipe& stdoutfd, const Pipe& stderrfd, pid_t& pid)
{
    lxc_attach_options_t attach_options = LXC_ATTACH_OPTIONS_DEFAULT;

    // remove const to match C api :(
    lxc_attach_command_t command = {
        const_cast<char*>(m_progStr.data()),
        const_cast<char**>(m_argv.data())
    };

    attach_options.stdout_fd = stdoutfd[1];
    attach_options.stderr_fd = stderrfd[1];

    if (auto* privateLxc = dynamic_cast<ContainerPrivateLxc*>(m_privateData)) {
        if (!privateLxc->lxc()->may_control(privateLxc->lxc()))
            return false;

        const SetId setId(privateLxc->uid, privateLxc->gid, m_mainWindow);
        return privateLxc->lxc()->attach(privateLxc->lxc(),
                                         lxc_attach_run_command,
                                         &command,
                                         &attach_options,
                                         &pid) == 0;
    }

    return false;
}
