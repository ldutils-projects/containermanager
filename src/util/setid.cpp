/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <cstdio>
#include <cerrno>
#include <QtGlobal>
#include <QMessageBox>
#include "src/ui/windows/mainwindow.h"
#include "src/util/setid.h"
#include "src/util/util.h"
#include "src/util/resources.h"

const char* SetId::homeVar = "HOME";
const char* SetId::userVar = "USER";

int SetId::nestCount = 0;

SetId::SetId(MainWindow& mainWindow) :
    mainWindow(mainWindow),
    success(true),
    prevUid(getuid()),
    prevGid(getgid()),
    prevHome(qgetenv(homeVar)),
    prevUser(qgetenv(userVar))
{
}

SetId::SetId(uid_t uid, gid_t gid, MainWindow& mainWindow) :
    SetId(mainWindow)
{
    init(uid, gid);
}

SetId::SetId(const QByteArray &uid_s, const QByteArray &gid_s, MainWindow& mainWindow) :
    SetId(mainWindow)
{
    const uid_t uid = Util::GetUid(uid_s);
    const gid_t gid = Util::GetGid(gid_s);

    if (uid == uid_t(-1) || gid == gid_t(-1))
        success = false;

    init(uid, gid);
}

void SetId::init(uid_t uid, gid_t gid)
{
    nestCount++;

    if (success) {
        success = success && (prevHome.size() > 0);

        if (uid != uid_t(-1) && uid != prevUid)
            success = success && (setuid(uid) == 0);

        if (gid != gid_t(-1) && gid != prevGid)
            success = success && (setgid(gid) == 0);

        if (success) {
            struct passwd *pw = getpwuid(geteuid());
            if (pw != nullptr) {
                success = success && (setenv(homeVar, pw->pw_dir, 1) == 0);
                success = success && (setenv(userVar, pw->pw_name, 1) == 0);
            }
        }
    }
}

void SetId::setFailure(const char* caller, const char* fn)
{
    QMessageBox::critical(&mainWindow, "Fatal error: aborting",
                          QString("Failure to restore environment in: SetId::") + caller + "\n" +
                          "API: " + fn,
                          QMessageBox::Abort);

    throw Exit(5);
}

SetId::~SetId()
{
    --nestCount;

    try { // avoid exceptions from destructor
        // TODO: check and shutdown if failed
        if (prevUid != getuid())
            if (setuid(prevUid) != 0)
                setFailure(static_cast<const char*>(__func__), "setuid");

        if (prevGid != getgid())
            if (setgid(prevGid) != 0)
                setFailure(static_cast<const char*>(__func__), "setgid");

        if (prevHome.size() > 0) {
            if (setenv(homeVar, prevHome.data(), 1) != 0)
                setFailure(static_cast<const char*>(__func__), "setenv");
            if (setenv(userVar, prevUser.data(), 1) != 0)
                setFailure(static_cast<const char*>(__func__), "setenv");
        }
    } catch (...) { }
}
