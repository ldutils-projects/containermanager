/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <lxc/lxccontainer.h>

#include <cassert>
#include <tuple>
#include <QSet>

#include "src/ui/windows/mainwindow.h"
#include "containerclasslxc.h"
#include "containerprivatelxc.h"
#include "src/util/util.h"
#include "src/core/cfgdata.h"
#include "src/util/setid.h"
#include "lxcutils.h"

ContainerClassLxc::ContainerClassLxc(ContainerClass cclass, const QString &name, MainWindow& mainWindow) :
    ContainerClassBase(cclass, name, mainWindow),
    classIcon(Util::ReadIcon(":art/logos/projects/LXC_logo-"))
{ }

void ContainerClassLxc::collectRootFs(ContainerModel& model, char** names, const char* path, int count,
                                      RootFsData& rootfs, const CfgDir& cfgDir) const
{
    rootfs.reserve(count);

    for (int c = 0; c < count; ++c) {
        QByteArray cacheKey = path;
        cacheKey += ':';
        cacheKey += names[c];

        const auto it = rootfsCache.find(cacheKey);
        if (it != rootfsCache.end()) {
            rootfs.push_back(std::make_tuple(*it, names[c]));
            continue;
        }

        rootfs.push_back(std::make_tuple(QByteArray(), names[c]));
        ContainerPrivateLxc lxc(mainWindow, model, names[c], path, cfgDir.uid, cfgDir.gid);

        const int size = lxc.lxc()->get_config_item(lxc.lxc(), "lxc.rootfs.path", nullptr, 0);

        auto& rootPath = std::get<0>(rootfs.back());
        rootPath.resize(size+1);  // +1 for trailing '\0'

        lxc.lxc()->get_config_item(lxc.lxc(), "lxc.rootfs.path", rootPath.data(), size+1);
        rootfsCache.insert(cacheKey, rootPath);
    }

    // Sort root paths, which will generate a nesting, since identical prefixes sort identically.
    std::sort(rootfs.begin(), rootfs.end());
}

void ContainerClassLxc::updateList(ContainerModel& model, const QModelIndex& parent, const CfgDir& cfgDir,
                                   QSet<const ContainerPrivate*>& updated) const
{  
    // TODO: look into encodeName/decodeName
    const QByteArray path = cfgDir.expandedDir();
    char** names = nullptr;
    const int count = list_all_containers(path.data(), &names, nullptr);

    QList<QModelIndex> roots({parent});
    const auto setCommonData = [&](QModelIndex& index, const QString& name, const char* privName) {
        if (!index.isValid()) {
            model.appendRow(roots.back());
            index = model.lastChild(roots.back());

            if (privName != nullptr)
                model.setPrivateData(index,
                                     new ContainerPrivateLxc(mainWindow, model, privName, path.data(),
                                                             cfgDir.uid, cfgDir.gid));

            model.setData(ContainerModel::Name, index, name);
            model.setData(ContainerModel::Class, index, int(containerClass), Util::RawDataRole);
        }

        if (model.privateData(index) != nullptr)
            updated.insert(model.privateData(index));
    };

    if (count <= 0) {
        // TODO: -1 on error: should we do anything about that, or just ignore? set a GUI error flag maybe?
    } else {
        // Set IDs if possible.  Will auto-reset in destructor.
        const SetId setId(cfgDir.uid, cfgDir.gid, mainWindow);

        RootFsData rootfs;

        // We must build a tree out of the flat list, so we will make two passes: the first to collect
        // parent-child relationships, and the second to populate the model.
        collectRootFs(model, names, path.data(), count, rootfs, cfgDir);

        // don't use names any more: process in root-sorted order.
        Util::FreeCArray(&names, count);  // Plain old C api: no destructor

        // Add title
        {
            rootfs.reserve(count);

            QModelIndex index = model.findRow(roots.back(), cfgDir.directory, ContainerModel::ConfigFile, Util::RawDataRole, 1);

            setCommonData(index, cfgDir.displayName, nullptr);
            roots.push_back(index);
            model.setData(ContainerModel::ConfigFile, index, cfgDir.directory, Util::RawDataRole);  // important for later matching, as just above in findRow
        }

        for (int c=0; c<count; ++c) {
            const QByteArray& name = std::get<1>(rootfs[c]);

            QModelIndex index = model.findRow(parent, [&](const QModelIndex& idx) {
                    const auto* privData = static_cast<const ContainerPrivateLxc*>(model.privateData(idx));
                    return privData != nullptr && privData->is(name, path, containerClass);
                });

            setCommonData(index, name, name.data());

            // Handle container nesting.
            {
                const int relativeIndent = (c < (count-1)) ? 
                    (std::get<0>(rootfs[c+1]).count(':') - std::get<0>(rootfs[c]).count(':')) : 0;

                if (relativeIndent > 0) {
                    roots.push_back(index);       // We're more nested: add indent level
                    assert(relativeIndent == 1);  // better just be one level at a time
                } else {
                    // We're equally or less nested: pop until we're back.
                    for (int r = 0; r < -relativeIndent; ++r)
                        roots.pop_back();
                }
            }
        }

        roots.pop_back();
    }
}

// Provide default container directories
void ContainerClassLxc::defaultDirs(QList<CfgDir>& dirs) const
{  
    dirs.push_back(CfgDir("~/.local/share/lxc", containerClass));

    const QStringList fromConf =
            LxcUtils::readConfig<QStringList>("/etc/lxc/lxc.conf", "lxc.lxcpath") +
            LxcUtils::readConfig<QStringList>("~/.config/lxc/default.conf", "lxc.lxcpath");

    for (const auto& dir : fromConf)
        dirs.push_back(CfgDir(dir, containerClass));
}

