/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONDIALOG_H
#define ICONDIALOG_H

#include <QDialog>
#include <QString>
#include <QIcon>

class MainWindow;

namespace Ui {
class IconDialog;
} // namespace Ui

class IconDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IconDialog(const MainWindow& mainWindow, QWidget *parent = nullptr);
    ~IconDialog() override;

    const QIcon& value() const { return selectedIcon; }

private slots:
    void on_openFromFile_clicked();
    void iconDoubleClicked(int row, int col);

private:
    void setupResourceIcons();

    Ui::IconDialog *ui;

    const MainWindow&  mainWindow;
    QIcon              selectedIcon;
};

#endif // ICONDIALOG_H
