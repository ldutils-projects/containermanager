/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QAction>
#include "containerpane.h"
#include "ui_containerpane.h"

#include <src/util/icons.h>
#include <src/util/roles.h>
#include <src/util/ui.h>
#include <src/util/util.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "src/core/containercontrol.h"

ContainerPane::ContainerPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::Container, parent),
    ui(new Ui::ContainerPane),
    uSecDelegate(this),
    sizeDelegate(20, 42, this),
    cpuSharesDelegate(this),
    typingIdle(this)
{
    ui->setupUi(this);

    setupView(ui->viewContainers, &app().containerModel());
    setWidgets<ContainerModel>(defColumnView(),
                               ui->filterContainers, ui->filterColumns, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    setupContainerView();
    setupDelegates();
    setupContextMenus();
    setupFilters();
    showAll();
    setupSignals();
    Util::SetupWhatsThis(this);
}

ContainerPane::~ContainerPane()
{
    deleteUI(ui);
}

void ContainerPane::setupContextMenus()
{
    // View context menus
    QTreeView& view = *ui->viewContainers;

    mainWindow().setupContainerActionContextMenu(paneMenu);
    setupActionContextMenu(paneMenu);
    // container action context menu
    view.setContextMenuPolicy(Qt::CustomContextMenu);
    connect(&view, &QTreeView::customContextMenuRequested, this, &ContainerPane::showActionContextMenu);
}

void ContainerPane::setupContainerView()
{
    typingIdle.setSingleShot(true);
    connect(&typingIdle, &QTimer::timeout, this, &ContainerPane::expandAll);

    setFocusProxy(ui->viewContainers);
    disableToolTipsFor(ui->viewContainers);

    static const auto flattenPredicate = [](const QAbstractItemModel* model, const QModelIndex& idx)
    {
        const auto* containerModel = dynamic_cast<const ContainerModel*>(Util::MapDown(model));
        return containerModel != nullptr && containerModel->isContainer(Util::MapDown(idx));
    };

    setFlattenPredicate(flattenPredicate);
}

void ContainerPane::setupDelegates()
{
    QTreeView& view = *ui->viewContainers;

    // Alas, this can't be in the constructors, since it requires ui->setupUi above.
    uSecDelegate.setSelector(selectionModel());
    sizeDelegate.setSelector(selectionModel());
    cpuSharesDelegate.setSelector(selectionModel());

    view.setItemDelegateForColumn(int(ContainerModel::CfsQuota),    &uSecDelegate);
    view.setItemDelegateForColumn(int(ContainerModel::CfsPeriod),   &uSecDelegate);
    view.setItemDelegateForColumn(int(ContainerModel::CpuShares),   &cpuSharesDelegate);
    view.setItemDelegateForColumn(int(ContainerModel::MemoryLimit), &sizeDelegate);
}

void ContainerPane::setupFilters()
{
    viewAsTree(cfgData().viewAsTree);
}

void ContainerPane::setupSignals()
{
    DataColumnPaneBase::setupSignals();
}

const ContainerPane::DefColumns& ContainerPane::defColumnView() const
{
    static const DefColumns cols = {
        { ContainerModel::Name,         true  },
        { ContainerModel::Class,        false },
        { ContainerModel::State,        true  },
        { ContainerModel::Uptime,       true  },
        { ContainerModel::Load,         true  },
        { ContainerModel::CpuUse,       true  },
        { ContainerModel::CpuShares,    false },
        { ContainerModel::CfsQuota,     false },
        { ContainerModel::CfsPeriod,    false },
        { ContainerModel::Memory,       true  },
        { ContainerModel::MemoryLimit,  false },
        { ContainerModel::MemPct,       true  },
        { ContainerModel::KMem,         false },
        { ContainerModel::Unprivileged, false },
        { ContainerModel::Ephemeral,    false },
        { ContainerModel::Processes,    false },
        { ContainerModel::Control,      false },
        { ContainerModel::HLink,        false },
        { ContainerModel::CLink,        false },
        { ContainerModel::IPV4,         false },
        { ContainerModel::IPV6,         false },
        { ContainerModel::TxBytes,      false },
        { ContainerModel::RxBytes,      false },
        { ContainerModel::TxRxBytes,    false },
        { ContainerModel::TxSpeed,      false },
        { ContainerModel::RxSpeed,      false },
        { ContainerModel::TxRxSpeed,    false },
        { ContainerModel::HWAddress,    false },
    #if USEIO
        { ContainerModel::ReadBytes,    false },
        { ContainerModel::WriteBytes,   false },
        { ContainerModel::ReadSpeed,    false },
        { ContainerModel::WriteSpeed,   false },
    #endif
        { ContainerModel::OS,           false },
        { ContainerModel::Kernel,       false },
        { ContainerModel::Machine,      false },
        { ContainerModel::Distro,       true  },
        { ContainerModel::Release,      false },
        { ContainerModel::Codename,     false },
        { ContainerModel::ConfigFile,   false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = ContainerModel::_First; md < ContainerModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

void ContainerPane::filterTextChanged(const QString& regex)
{
    DataColumnPaneBase::filterTextChanged(regex);

    using namespace std::chrono_literals;
    typingIdle.start(500ms);  // expand the tree after a little bit of idle time
}

void ContainerPane::showActionContextMenu(const QPoint& pos)
{
    paneMenu.exec(ui->viewContainers->mapToGlobal(pos));
}
