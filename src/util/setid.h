/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SETID_H
#define SETID_H

#include <sys/types.h>
#include <QByteArray>

class MainWindow;

// This class sets the process UID and $HOME in its constructor, and reverts in the
// destructor.  It is not a failure for the attempt to fail: it merely means the
// owning process lacked permission to do so.
//
// IDs can be names, or numeric ids prefixed with '#'.
class SetId
{
public:
    SetId(MainWindow& mainWindow);
    SetId(uid_t uid, gid_t gid, MainWindow& mainWindow);
    SetId(const QByteArray& uid_s, const QByteArray& gid_s, MainWindow& mainWindow);
    ~SetId();

    bool succeeded() const { return success; }

private:
    void setFailure(const char* caller, const char* fn);

    SetId(MainWindow* mainWindow);
    void init(uid_t uid, gid_t gid);

    static const char* homeVar;
    static const char* userVar;

    MainWindow& mainWindow;

    bool        success;

    uid_t       prevUid;   // values to restore
    gid_t       prevGid;
    QByteArray  prevHome;
    QByteArray  prevUser;

    static int  nestCount; // so we can nest these
};

#endif // SETID_H
