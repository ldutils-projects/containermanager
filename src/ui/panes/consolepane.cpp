/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <array>

#include <QPlainTextEdit>
#include <QFontMetrics>
#include <QToolButton>

#include "src/core/app.h"
#include "src/core/containermodel.h"
#include "src/ui/windows/mainwindow.h"
#include <src/util/icons.h>
#include "src/util/executor.h"
#include "consolepane.h"
#include "ui_logbase.h"

ConsolePane::ConsolePane(MainWindow& mainWindow, QWidget* parent) :
    LogBase(mainWindow, PaneClass::Console, parent),
    cmdThreadPool(this),
    cmdThreads(cmdThreadPool),
    commandHistory(new QComboBox()),
    commandEdit(new ConsoleInput(*this, commandHistory)),
    shells(new QComboBox())
{
    nameColumn = int(Columns::Name);

    setTitle("Console");
    setupInputUi();        // input editor, shell selectors, etc.
    setupTooltip();        // pane tooltip
    setupShellComboBox();  // populate shell combobox
    setupTreeView();       // tree headers, modes, etc.
    setupColumnWidths();   // default column sizes
    setupContextMenus();   // our custom context menus
    setupSignals();        // setup signals
    setupActionIcons();    // default icons if there's no theme icon
    processModelReset();   // seed initial model data
}

ConsolePane::~ConsolePane()
{
    cmdThreadPool.clear();  // clear pending but not started threads
    deleteUI(ui);
}

void ConsolePane::setupSignals()
{
    connect(&cmdThreads, &ThreadSet::threadFinished, this, &ConsolePane::threadFinished);
    connect(&cmdThreads, &ThreadSet::threadAboutToStart, this, &ConsolePane::threadStarting);
}

void ConsolePane::setupInputUi()
{
    if (ui == nullptr)
        return;

    auto* hboxCommand = new QHBoxLayout();

    commandEdit->setPlaceholderText(tr("Command"));
    commandEdit->setFocus();
    commandEdit->setClearButtonEnabled(true);
    commandHistory->setLineEdit(commandEdit);
    commandHistory->setMaxCount(cfgData().commandHistoryLines);
    commandHistory->setMaxVisibleItems(16);
    commandHistory->setInsertPolicy(QComboBox::InsertAtBottom);

    setFocusProxy(commandEdit);

    cancelButton = new QToolButton();
    cancelButton->setIcon(Icons::get("process-stop"));
    cancelButton->setEnabled(false);

    hboxCommand->insertWidget(-1, commandHistory, 100);
    hboxCommand->insertWidget(-1, cancelButton, 1);
    hboxCommand->insertWidget(-1, createClearButton(), 1);
    hboxCommand->insertWidget(-1, shells, 20);

    ui->verticalLayout->insertLayout(-1, hboxCommand);

    connect(cancelButton, &QToolButton::pressed, this, &ConsolePane::cancelAll);

    static const QString tooltipCommandEdit =
            tr("<html><head/><body><p>"
               "Commands input here will be passed to each selected container each time the "
               "<i>return</i> key is pressed.  This will happen in a process per container. "
               "If the process launched in the container does not terminate after a period of "
               "time, it will be killed.  The return code from the process will be displayed "
               "next to each container name in the console display above."
               "</p><p>"
               "Several customn key sequences are available:<br>"
               "&nbsp;&nbsp;&nbsp;<b>Ctrl+C</b> or <b>Esc</b> - Interrupt pending processes.<br>"
               "&nbsp;&nbsp;&nbsp;<b>Ctrl+L</b> - Clear scrollback buffers.<br>"
               "&nbsp;&nbsp;&nbsp;<b>Ctrl+Home</b> - Collapse tree view.<br>"
               "&nbsp;&nbsp;&nbsp;<b>Ctrl+End</b> - Expand tree view.<br>"
               "</p></body></html>");
    commandEdit->setToolTip(tooltipCommandEdit);
    commandEdit->setWhatsThis(tooltipCommandEdit);

    static const QString tooltipShell =
            tr("<html><head/><body><p>"
               "This selects the command interpreter to be used for the commands executed in "
               "each container.  There is no guarantee that any given container has the selected "
               "interpreter installed, so an error will result if it does not.  The command "
               "interpreter is not kept running in the container: it terminates after the command "
               "being executed finishes.  Each command uses a new execution of the selected "
               "interpreter."
               "</p><p>"
               "The <b>direct</b> option executes the command directly, without using a "
               "command interpreter.  This is light weight, but does not provide access to "
               "shell features such as pipes and control structures."
               "</p></body></html>");
    shells->setToolTip(tooltipShell);
    shells->setWhatsThis(tooltipShell);

    static const QString tooltipCancel =
            tr("<html><head/><body><p>"
               "Pressing this will attempt to cancel any active container processes started "
               "by this <b>Console Pane</b>.  It may or may not be possible to cancel them."
               "</p></body></html>");
    cancelButton->setToolTip(tooltipCancel);
    cancelButton->setWhatsThis(tooltipCancel);
}

void ConsolePane::setupTooltip()
{
    static const QString tooltipPane =
            tr("<html><head/><body><p align=\"center\"><span style=\" font-weight:600; text-decoration: underline;\">Console Pane</span></p>"
               "<p>Commands entered into the command line at the bottom of this pane will be broadcast to all selected (checked) containers, "
               "and the results displayed in the output area for each.  The return code (RC) for each command will be displayed next to "
               "the container name, so even if the consoles are collapsed, an overview of the result can easily be seen.  The shell "
               "to use can be selected next to the input area at the bottom.  The <i>direct</i> option will invoke processes directly "
               "rather than use a command interpreter.</p>"
               "<p>The filter box at the top will expand only containers matching the given expression.  To restrict execution to only "
               "those containers, use the <b>Select Expanded</b> context menu.</p>"
               "<p>Only a limited number of processes are launched at a time, and they will time out after a while.  The thread count "
               "and the timeout can be set in the program configuration under <i>Command Threads</i> and <i>Command Task Timeout.</i></p>"
               "<p>Processes are run asynchronously.  The interrupt button next to the command input line will cancel running processes.</p>"
               "</body></html>");

    setToolTip(tooltipPane);
    setWhatsThis(tooltipPane);
}

void ConsolePane::setupShellComboBox()
{
    const QString current = shells->currentText();

    shells->clear();
    for (const auto& shell : cfgData().shells)
        shells->insertItem(shells->count(), shell.name);

    // attempt to restore old setting
    shells->setCurrentText(current);
}

void ConsolePane::setupTreeView()
{
    ui->logTree->setHeaderHidden(false);
    ui->logTree->setHeaderLabels({"RC", "Container"});
    ui->logTree->setSortingEnabled(true);
    ui->logTree->sortByColumn(int(Columns::Name), Qt::AscendingOrder);
}

void ConsolePane::setupColumnWidths()
{
    const int rcColWidth = QFontMetrics(ui->logTree->font()).boundingRect("000").width();

    ui->logTree->setColumnWidth(int(Columns::Rc), rcColWidth);
}

void ConsolePane::setupContextMenus()
{
    LogBase::setupContextMenus();

    // Must be here, not in our base class, because it's called from the constructor and uses virtual
    // methods.
    mainWindow().setupContainerActionContextMenu(paneMenu, true);
    setupActionContextMenu(paneMenu);
}

void ConsolePane::newConfig()
{
    LogBase::newConfig();

    setupShellComboBox();  // reset shell options

    cmdThreadPool.setMaxThreadCount(cfgData().cmdThreadCount);

    applyToWidgets<QPlainTextEdit>(&QPlainTextEdit::setMaximumBlockCount,
                                   cfgData().consoleHistoryLines);

    commandHistory->setMaxCount(cfgData().commandHistoryLines);
}

// Clear all logs
void ConsolePane::clearLogs()
{
    applyToWidgets<QPlainTextEdit>(&QPlainTextEdit::clear);
}

// Append some output to the proper log, at end of command when RC is known.
void ConsolePane::append(const QModelIndex& idx, int rc, const QVector<QByteArray>& output)
{
    auto*            pte    = indexToWidget<QPlainTextEdit>(idx);
    QTreeWidgetItem* parent = indexToParentItem(idx);
    if (pte == nullptr)
        return;

    if (rc == ThreadBase::startingRc) {
        appendStr(pte, UiType::Cmdline, output, Out::Cmdline, "# ");
        parent->setData(int(Columns::Rc), Qt::DisplayRole, "");
        return;
    }

    const QColor& rcColor =
            (rc == 0)   ? cfgData().uiColor[UiType::Success] :
            (rc == 255) ? cfgData().uiColor[UiType::Critical] :
            cfgData().uiColor[UiType::Error];

    // Set RC in column header
    if (parent != nullptr && rc >= 0) {
        parent->setData(int(Columns::Rc), Qt::DisplayRole, rc);
        parent->setData(int(Columns::Rc), Qt::ForegroundRole, rcColor);
    }

    // Add RC to console output if it's not negative or zero
    if (rc > 0) {
        std::array<char, 64> rcBuf;
        sprintf(rcBuf.data(), "[rc=%d]", rc);
        appendStr(pte, rcColor, rcBuf.data());
    }

    appendStr(pte, UiType::Stderr, output, Out::Stderr);
    appendStr(pte, UiType::Stdout, output, Out::Stdout);

    pte->moveCursor(QTextCursor::End);
}

void ConsolePane::cancelled(const QModelIndex &idx)
{
    auto*            pte    = indexToWidget<QPlainTextEdit>(idx);
    QTreeWidgetItem* parent = indexToParentItem(idx);
    if (pte == nullptr)
        return;

    const QColor& cancelColor = cfgData().uiColor[UiType::Warning];

    // Set cancel notification in tree view
    if (parent != nullptr) {
        parent->setData(int(Columns::Rc), Qt::DisplayRole, "^C");
        parent->setData(int(Columns::Rc), Qt::ForegroundRole, cancelColor);
    }

    appendStr(pte, cancelColor, "^C");
}

void ConsolePane::threadFinished(int rc) const
{
    cancelButton->setEnabled(cmdThreads.count() > 0);

    const_cast<ConsolePane*>(this)->mainWindow().finishedBGTask(rc);
}

void ConsolePane::threadStarting(ThreadBase* thread) const
{
    if (thread == nullptr)
        return;

    const_cast<ConsolePane*>(this)->mainWindow().startedBGTask(thread);

    thread->connect(thread, &ThreadBase::logMessage, this, &LogBase::append);
    thread->connect(thread, &ThreadBase::cancelled, this, &LogBase::cancelled);
}

void ConsolePane::execute(const QByteArray& cmdLine)
{
    if (ui == nullptr)
        return;

    if (cmdLine.isEmpty())  // nothing to do
        return;

    static const CfgShell fallbackShell("direct", "");  // if there were none in the config.

    const CfgShell& shell = (shells->currentIndex() < cfgData().shells.size()) ?
                             cfgData().shells[shells->currentIndex()] : fallbackShell;

    const Executor executor(cmdLine, shell);

    if (executor.parseError()) {
        mainWindow().statusMessage(UiType::Warning, tr("Command line parse error"));
        return;
    }

    containerModel.execute(getSelections(), cmdLine,
                           executor.progname(), executor.argv(),
                           cfgData().cmdThreadTimeout, &cmdThreads);

    cancelButton->setEnabled(cmdThreads.count() > 0);
}

void ConsolePane::save(QSettings& settings) const
{
    if (ui == nullptr)
        return;

    LogBase::save(settings);

    MemberSave(settings, commandHistory);
    MemberSave(settings, commandEditLE);

    SL::Save(settings, "shell", shells->currentText());
}

void ConsolePane::load(QSettings& settings)
{
    if (ui == nullptr)
        return;

    LogBase::load(settings);

    MemberLoad(settings, commandHistory);
    MemberLoad(settings, commandEditLE);

    shells->setCurrentText(SL::Load<QString>(settings, "shell", "sh"));
}

void ConsolePane::ConsoleInput::keyPressEvent(QKeyEvent *event)
{
    if (console.ui == nullptr)
        return;

    // Override a few keys to do our evil bidding.
    if (event->key() == Qt::Key_Escape) {
        console.cancelAll();
        return;
    }

    // Custom return key behavior to make this behave a little more like a console.
    if (event->key() == Qt::Key_Return) {
        const QString command = text();  // to avoid the widgets changing it behind our back
        console.execute(command);        // execute it

        // Insert item if it isn't in the history already, or if it's control-return
        if (history->findText(command) == -1 || event->modifiers() == Qt::ControlModifier) {
            if (history->count() > 0)
                if (history->itemText(history->count() - 1).isEmpty())
                    history->removeItem(history->count() - 1);

            while (history->count() >= (history->maxCount()-1))  // remove from top
                history->removeItem(0);

            history->addItem(command);
            history->addItem("");  // add newly empty item, so up arrow acts as expected.
        }

        history->setCurrentIndex(history->count()-1);
        clear();
        return;
    }

    // More bending the line editor to our bidding... muahahaaaa.
    // TODO: get from configuration.
    if (event->modifiers() == Qt::ControlModifier) {
        switch (event->key()) {
        case Qt::Key_C:    console.cancelAll();   return;
        case Qt::Key_L:    console.clearLogs();   return;
        case Qt::Key_Home: console.collapseAll(); return;
        case Qt::Key_End:  console.expandAll();   return;
        default: break;
        }
    }

    return QLineEdit::keyPressEvent(event);
}

void ConsolePane::setupActionIcons()
{
    Icons::defaultIcon(ui->action_AddAllRunning,           "list-add");
    Icons::defaultIcon(ui->action_SelectExpanded,          "checkmark");
    Icons::defaultIcon(ui->action_RemoveUnselected,        "remove");
}

