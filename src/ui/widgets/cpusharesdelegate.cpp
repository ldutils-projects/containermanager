/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <limits>
#include <QSpinBox>

#include <src/util/roles.h>
#include "cpusharesdelegate.h"

CpuSharesDelegate::CpuSharesDelegate(QObject* parent) :
    ContainerDelegateBase(parent, false)
{
}

QWidget *CpuSharesDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex& index) const
{
    const int value = index.model()->data(index, Util::RawDataRole).toInt();

    auto *editor = new QSpinBox(parent);

    editor->setMinimum(1);
    editor->setMaximum(std::numeric_limits<int>::max());
    editor->setValue(value);
    editor->setSingleStep(16);
    editor->setFrame(false);
    editor->setAutoFillBackground(true);  // otherwise it ends up transparent
    editor->show();

    return editor;
}

void CpuSharesDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    updateResultsFromWidget<QSpinBox>(editor, model, index);
}
