/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERDATADELEGATE_H
#define CONTAINERDATADELEGATE_H

#include <src/ui/widgets/comboboxdelegate.h>

class ContainerDataDelegate : public ComboBoxDelegate
{
public:
    ContainerDataDelegate(QObject* parent = nullptr);
};

#endif // CONTAINERDATADELEGATE_H
