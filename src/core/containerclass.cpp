/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include "containerclass.h"
#include "containermodel.h"
#include "src/ui/windows/mainwindow.h"
#include "src/backends/lxc/containerclasslxc.h"

ContainerClassBase::ContainerClassBase(ContainerClass cclass, const QString& name,
                                       MainWindow& mainWindow) :
    containerClass(cclass),
    name(name),
    mainWindow(mainWindow)
{ }

decltype(ContainerClassBase::ContainerClasses) ContainerClassBase::ContainerClasses;

void ContainerClassBase::initContainerTypes(MainWindow& mainWindow)
{
    if (!ContainerClasses.empty())
        return;

    ContainerClasses[ContainerClass::None] = new ContainerClassNone(ContainerClass::None, "None", mainWindow);
    ContainerClasses[ContainerClass::Lxc]  = new ContainerClassLxc(ContainerClass::Lxc, "Lxc", mainWindow);
}

const ContainerClassBase* ContainerClassBase::find(const QString &name)
{
    for (const auto& cclass : ContainerClasses)
        if (cclass->name == name)
            return cclass;

    return nullptr;
}

const ContainerClassBase* ContainerClassBase::find(const ContainerClass type)
{
    return ContainerClasses[type];
}

QDataStream& operator<<(QDataStream& out, const ContainerClass& cc)
{
    return out << std::underlying_type<ContainerClass>::type(cc);
}

QDataStream& operator>>(QDataStream& in, ContainerClass& cc)
{
    return in >> (std::underlying_type<ContainerClass>::type&)(cc);
}

