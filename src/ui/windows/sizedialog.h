/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIZEDIALOG_H
#define SIZEDIALOG_H

#include <QDialog>

namespace Ui {
class SizeDialog;
} // namespace Ui

enum class Format;

class SizeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SizeDialog(size_t initialValue, int lowestPow2 = 20, int highestPow2 = 46, QWidget *parent = nullptr);
    ~SizeDialog() override;

    qulonglong value() const { return currentValue; }

public slots:
    void setValue(uint64_t);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_sizeUnits_currentIndexChanged(int index);
    void on_sizeSlider_valueChanged(int value);
    void on_sizeSpinBox_valueChanged(int value);

private:
    void   setupUnits();
    int    indexFor(Format) const;
    size_t currentUnitMultiplier() const;  // for current combobox value

    static size_t sliderToVal(int sliderVal);
    static int    valToSlider(size_t value);

    Ui::SizeDialog *ui;

    size_t currentValue;
    const size_t initialValue;
    const int    lowestPow2;
    const int    highestPow2;
};

#endif // SIZEDIALOG_H
