/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sizedelegate.h"
#include <src/util/roles.h>
#include "src/ui/windows/sizedialog.h"

SizeDelegate::SizeDelegate(int minPow2, int maxPow2, QObject* parent) :
    ContainerDelegateBase(parent),
    minPow2(minPow2),
    maxPow2(maxPow2)
{
}

QWidget *SizeDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex& index) const
{
    const size_t value = index.model()->data(index, Util::RawDataRole).toULongLong();

    return setPopup(new SizeDialog(value, minPow2, maxPow2, parent));
}

void SizeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    updateResultsFromDialog<SizeDialog>(editor, model, index);
}
