/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERCONTROL_H
#define CONTAINERCONTROL_H

#include <QString>

enum class ContainerState;

// Things we can do with containers.  These are verbs.
enum class ContainerControl {
    _First = 0,
    Start = _First,
    Stop,
    Freeze,
    Thaw,
    Reboot,
    ExecuteIn,
    Create,
    Remove,
    _Count,
};

class ContainerControlInfo
{
public:
    static QString name(ContainerControl);
    static ContainerState transitionState(ContainerControl);
    static ContainerState finalState(ContainerControl);
    static bool separatorAfter(ContainerControl);
    static bool needsSelection(ContainerControl);
};

#endif // CONTAINERCONTROL_H
