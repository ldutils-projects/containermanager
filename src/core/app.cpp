/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>

#include <QVector>
#include <QString>
#include <QDataStream>

#include "src/ui/panes/pane.h"
#include "src/util/ui.h"
#include "src/util/cmdline.h"
#include "src/core/containerclass.h"
#include "app.h"

App::App(int &argc, char **argv, const CmdLine& cmdLine) :
    AppBase(argc, argv, cmdLine)
{
    setupTranslators();
    m_rc = m_cmdLine.processArgsPostApp();
}


App::~App()
{
    haltThreads();
}

void App::haltThreads()
{
    // Clear pending but not yet started threads, to avoid having to wait for them.
    dataPool().clear();
    ctrlPool().clear();
}

App::InitStatic::InitStatic()
{
    setupResources();
    setupIcons();
    registerTypes();
}

void App::setupResources()
{
    const QString iconsTheme = Util::IsLightTheme() ? "icons-light.rcc" : "icons-dark.rcc";

    for (const QString& file : { { "art.rcc" },
                                 iconsTheme } )
        loadResourceFile(file);
}

void App::setupIcons()
{
    QApplication::setWindowIcon(QIcon(":art/logos/projects/ContainerManager-64x64.png"));
}

// Allow conversion to QVariant
QDataStream& operator<<(QDataStream& out, const Qt::SortOrder& s) {
    return out << int(s);
}

QDataStream& operator>>(QDataStream& in, Qt::SortOrder& s) {
    return in >> (int &)(s);
}

void App::registerTypes()
{
    // Allow passing these things between threads
    qRegisterMetaType<QString>("QString&");
    qRegisterMetaType<QVector<int>>("QVector<int>&");
    qRegisterMetaType<UiType>("UiType");
    qRegisterMetaType<ContainerClass>("ContainerClass");
    qRegisterMetaType<PaneClass>("PaneClass");
    qRegisterMetaType<QVector<QByteArray>>("QVector<QByteArray>&");

    qRegisterMetaTypeStreamOperators<ContainerClass>("ContainerClass");
    qRegisterMetaTypeStreamOperators<PaneClass>("PaneClass");
    qRegisterMetaTypeStreamOperators<Qt::SortOrder>("Qt::SortOrder");
}
