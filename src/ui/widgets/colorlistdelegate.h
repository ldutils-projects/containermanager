/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORLISTDELEGATE_H
#define COLORLISTDELEGATE_H

#include <QList>
#include <QModelIndex>
#include <QAbstractItemModel>

#include <src/ui/widgets/colordelegate.h>

// Custom delegate to set font colors, etc.
class ColorListDelegate : public ColorDelegate {
public:
    ColorListDelegate(const QList<int>& fgCol, const QList<int>& bgCol, QObject* parent) :
        ColorDelegate(parent), fgCol(fgCol), bgCol(bgCol), enabled(true) { }

    ColorListDelegate(int fgCol, int bgCol, QObject* parent) :
        ColorDelegate(parent), fgCol({fgCol}), bgCol({bgCol}), enabled(true) { }

    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

    void setEnabled(bool e) { enabled = e; }

private:
    QList<int> fgCol;
    QList<int> bgCol;
    bool       enabled;
};

#endif // COLORLISTDELEGATE_H
