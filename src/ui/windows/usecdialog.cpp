/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QThread>
#include <src/util/ui.h>

#include "usecdialog.h"
#include "ui_usecdialog.h"

USecDialog::USecDialog(int initAbs, int absMax, bool hideRel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::USecDialog),
    absMax(absMax),
    initAbs(initAbs)
{
    ui->setupUi(this);

    connect(ui->usRelSlide, &QSlider::valueChanged,  this, &USecDialog::relValueChange);
    connect(ui->usRelSpin,  (void (QSpinBox::*)(int)) &QSpinBox::valueChanged,
            this, &USecDialog::relValueChange);

    connect(ui->usAbsSpin, (void (QSpinBox::*)(int)) &QSpinBox::valueChanged,
            this, &USecDialog::absValueChange);

    connect(ui->usUnlimited, &QRadioButton::toggled, this, &USecDialog::modeChange);

    if (hideRel) {
        ui->usModeBox->hide();
        ui->usRelSpin->hide();
    }

    // We don't know these values in advance, so set them here.
    ui->usRelSpin->setMaximum(QThread::idealThreadCount() * 100);
    ui->usRelSlide->setMaximum(QThread::idealThreadCount() * 100);
    ui->usRelSlide->setTickInterval(50);
    ui->usAbsSpin->setMaximum(absMax * QThread::idealThreadCount());
    ui->usAbsSpin->setSingleStep(absMax / 25);

    modeChange(initAbs < 0);
    setValue(initAbs);

    Util::SetupWhatsThis(this);
}

USecDialog::~USecDialog()
{
    delete ui;
}

void USecDialog::modeChange(bool unlimited)
{
    if (unlimited)
        setValue(-1);
    else if (value() < 0)
        setValue(ui->usAbsSpin->value());  // start with whatever was in the widget

    ui->usValGroup->setDisabled(unlimited);
    ui->usUnlimited->setChecked(unlimited);
}

void USecDialog::setValue(int absVal)
{
    currentValue = absVal;

    if (absVal >= 0) {
        ui->usAbsSpin->setValue(absVal);

        int relVal = absToRel(absVal);
        ui->usRelSpin->setValue(relVal);
        ui->usRelSlide->setValue(relVal);
    }
}

void USecDialog::on_buttonBox_accepted()
{
    emit accepted();
}

void USecDialog::on_buttonBox_rejected()
{
    currentValue = initAbs;  // restore
    emit rejected();
}

void USecDialog::relValueChange(int val)
{
    setValue(relToAbs(val));
}

void USecDialog::absValueChange(int val)
{
    setValue(val);
}
