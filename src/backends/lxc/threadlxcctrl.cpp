/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QtGlobal>

#include <cstdlib>
#include <sys/prctl.h>
#include <lxc/lxccontainer.h>

#include "threadlxcctrl.h"
#include "src/version.h"
#include "src/core/containercontrol.h"
#include "src/util/setid.h"
#include "containerprivatelxc.h"

ThreadLxcCtrl::ThreadLxcCtrl(MainWindow& mainWindow, ContainerPrivateLxc& privateLxc,
                             ContainerControl ctrl,
                             const QPersistentModelIndex& idx) :
    ThreadBase(mainWindow),
    privateLxc(privateLxc),
    ctrl(ctrl),
    idx(idx)
{ }

template <typename FN, typename... T>
bool ThreadLxcCtrl::execIfLxc(const FN& fn, const T&... args)
{
    const SetId setId(privateLxc.uid, privateLxc.gid, m_mainWindow);

    lxc_container* lxc = privateLxc.lxc();

    if (lxc == nullptr)
        return false;

    // TODO: check for permission to access container root via access()

    if (!lxc->may_control(lxc)) {
        return false;  // TODO: count insufficient privileges
    }

    if (lxc->lxc_conf == nullptr) {
        return false;
    }

    // We can't cancel any of these operations into liblxc, so they better
    // finish on their own.
    return (lxc->*fn)(lxc, args...);
}

void ThreadLxcCtrl::run()
{
    const Runner runner(this);  // This manages the thread on the threadset

    const timespec rebootDelay = { 0, 250000000 };
    bool success = false;

    // lxc doesn't correctly parse the /proc/self/stat file if it contains spaces, which
    // QT adds to the thread name by default, so we'll rename ourselves to avoid that bug.
    // We don't check for errors, because there's nothing to be done about it anyway.
    prctl(PR_SET_NAME, Appname, 0, 0, 0);

    srand(size_t(this) & 0xffffffff); // seed based on thread id

    // Small random delay, to stagger the spam a little.
    const int64_t startDelay = (float(rand()) / float(RAND_MAX)) * 500000000;

    const timespec ts = { 0, decltype(timespec().tv_nsec)(startDelay) };
    nanosleep(&ts, nullptr);

    // Turn one of the control commands into and lxc_container function
    switch (ctrl) {
    case ContainerControl::Start:
        // Daemonize it
        if (privateLxc.lxc() != nullptr)
            privateLxc.lxc()->want_daemonize(privateLxc.lxc(), true);

        success = execIfLxc(&lxc_container::start, false, nullptr);
        break;

    case ContainerControl::Stop:
        success = execIfLxc(&lxc_container::stop);
        break;

    case ContainerControl::Freeze:
        success = execIfLxc(&lxc_container::freeze);
        break;

    case ContainerControl::Thaw:
        success = execIfLxc(&lxc_container::unfreeze);
        break;

    case ContainerControl::Reboot:
        // success = execIfLxc(&lxc_container::reboot);
        // For some reason liblxc's reboot has trouble with many parallel
        // reboots, so we instead stop and then start after a small delay.
        success = execIfLxc(&lxc_container::stop);
        nanosleep(&rebootDelay, nullptr);
        success = success && execIfLxc(&lxc_container::start, false, nullptr);
        break;

    case ContainerControl::Remove:
        success = execIfLxc(&lxc_container::destroy);
        break;

    default:
        break;
    }

    privateLxc.m_stateInTransition = false;

    m_rc = (success ? 0 : 1);
}
