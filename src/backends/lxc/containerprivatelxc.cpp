/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstring>
#include <algorithm>
#include <array>
#include <unistd.h>
#include <lxc/lxccontainer.h>

#include "src/ui/windows/mainwindow.h"
#include "src/util/setid.h"
#include <src/util/roles.h>
#include "src/util/util.h"
#include "src/core/app.h"
#include "src/util/cgroups.h"
#include "src/core/cfgdata.h"
#include "src/core/containerstate.h"
#include "src/core/containercontrol.h"
#include "containerprivatelxc.h"
#include "threadlxcctrl.h"
#include "containerclasslxc.h"
#include "lxcutils.h"

class MainWindow;

ContainerPrivateLxc::ContainerPrivateLxc(MainWindow& mainWindow,
                                         ContainerModel& model, const char* name, const char* configPath,
                                         uid_t uid, gid_t gid) :
    ContainerPrivate(mainWindow, model, ContainerId(name, configPath, ContainerClass::Lxc)),
    prevRunning(false),
    prevCpuUse(0),
    prevUptime(0),
    prevReadBytes(0),
    prevWriteBytes(0),
    uid(uid),
    gid(gid),
    bootTimeInTcks(0),
    ctrlThreads(app().ctrlPool()),
    dataThreads(app().dataPool()),
    dataTries(int(ContainerModel::_Count), 0),
    lxc_data(lxc_container_new(name, configPath))
{
    setupSignals();
    elapsed.restart();
}

ContainerPrivateLxc::ContainerPrivateLxc(MainWindow& mainWindow,
                                         ContainerModel &model, const char *name, const char *configPath,
                                         const QByteArray &uid_s, const QByteArray &gid_s) :
    ContainerPrivateLxc(mainWindow, model, name, configPath, Util::GetUid(uid_s), Util::GetGid(gid_s))
{
}

ContainerPrivateLxc::~ContainerPrivateLxc()
{
    if (lxc_data != nullptr)
        lxc_container_put(lxc_data);  // release container reference
}

void ContainerPrivateLxc::setupSignals()
{
    ctrlThreads.connect(&ctrlThreads, &ThreadSet::threadStarted, &mainWindow, &MainWindow::startedBGTask);
    ctrlThreads.connect(&ctrlThreads, &ThreadSet::threadFinished, &mainWindow, &MainWindow::finishedBGTask);
}

// See if thraedset contains a thread of class THREAD, referencing idx
template <typename THREAD>
THREAD* ContainerPrivateLxc::threadsetContains(const ThreadSet& threadSet, const QModelIndex& idx,
                                               const QVector<ModelType>& cols) const
{
    ThreadBase* thread = threadSet.contains([&](const ThreadBase* thread) {
        const auto* t = dynamic_cast<const THREAD*>(thread);
        return t != nullptr && t->setsThreadData(idx, cols);
    });

    return static_cast<THREAD*>(thread);
}

// Collect some data asynchronously and set it in the model
template <typename... T>
bool ContainerPrivateLxc::asyncSet(const QModelIndex& idx,
                                   const QVector<QRegExp>& keys,
                                   const QVector<ModelType>& cols,
                                   ThreadLxcData::FinishCommand finishCommand,
                                   const QByteArray& progStr, const T&... t)
{
    // If ANY of the requested data exists, simply return.  Some data may not
    // be available from some containers, so we consider any of it to be "good enough".
    if (std::any_of(cols.begin(), cols.end(),
                    [&](ModelType mt) { return !model().data(mt, idx, Util::RawDataRole).isNull(); })) {
        for (const auto d : cols)
            dataTries[int(d)] = 0;
        return true;
    }

    // If we've tried too many times, just give up.
    for (auto d : cols)
        if (++dataTries[int(d)] > maxDataFailures)
            return false;

    // Don't re-launch if there's an exist thread trying to do this piece of
    // work already.
    if (threadsetContains<ThreadLxcData>(dataThreads, idx, cols) != nullptr)
        return true;

    auto* thread = new ThreadLxcData(mainWindow, this, cfgData().dataThreadTimeout, progStr, t...);
    if (thread == nullptr)
        return false;

    // TODO: this whole mechanism is clunky and could stand improvement.
    switch (finishCommand) {
    case ThreadLxcData::FinishCommand::Data:
        return dataThreads.start(thread->requestData(idx, cols));
    case ThreadLxcData::FinishCommand::Words:
        return dataThreads.start(thread->requestWords(idx, cols));
    case ThreadLxcData::FinishCommand::EnvFile:
        return dataThreads.start(thread->requestEnvFile(idx, keys, cols));
    default:
        delete thread;
        return false;
    }
}

template <typename T>
T ContainerPrivateLxc::getCgroupItem(pid_t pid, const char* item, int buflen,
                                     const char* controller) const
{
    return cgroups.read<T>(pid, item, buflen, controller);
}

template <>
int64_t ContainerPrivateLxc::getCgroupItem<int64_t>(pid_t pid, const char* item,
                                                    int buflen, const char* controller) const
{
    if (cfgData().directCgroups) {
        // If our process has setuid'ed itself, the lxc->get_cgroup_item will fail since it wants write
        // access to the cgroup for /proc/self.  As a backup plan, we can try to read the cgroup ourselves.
        return cgroups.read<QByteArray>(pid, item, buflen, controller).toLongLong();
    }

    std::array<char, 64> buf;
    buf[0] = '\0';

    assert(unsigned(buflen) <= sizeof(buf));

    const int ret = item != nullptr ? lxc()->get_cgroup_item(lxc(), item, buf.data(), sizeof(buf)) : -1;

    if (ret > 0 && ret < int(sizeof(buf)))
        return strtoll(buf.data(), nullptr, 0);

    return 0;
}

template <>
uint64_t ContainerPrivateLxc::getCgroupItem<uint64_t>(pid_t pid, const char* item,
                                                      int buflen, const char* controller) const
{
    return uint64_t(getCgroupItem<int64_t>(pid, item, buflen, controller));
}

template<typename T>
T ContainerPrivateLxc::getCgroupItem(pid_t pid, int buflen, ModelType mt) const
{
    assert(ContainerModel::cgroupController(mt) != nullptr);
    return getCgroupItem<T>(pid, ContainerModel::cgroupFile(mt), buflen, ContainerModel::cgroupController(mt));
}

template <typename T>
bool ContainerPrivateLxc::setCgroupItem(pid_t pid, const T& value, ModelType mt)
{
    if (cfgData().directCgroups)
        if (cgroups.write(pid, ContainerModel::cgroupFile(mt), value.toByteArray(),
                               ContainerModel::cgroupController(mt)))
            return true;


   return lxc()->set_cgroup_item(lxc(), ContainerModel::cgroupFile(mt), value.toByteArray().data());
}


void ContainerPrivateLxc::updateData(const QModelIndex &index, bool force)
{
    static const char* na = "n/a";

    if (lxc() == nullptr)
        return;

    // If asked, clear out data and reseed.  The range here is magic: sorry.
    if (force) {
        dataTries.fill(0);  // reset failure count

        for (ModelType mt = ContainerModel::State; mt < ContainerModel::ConfigFile; ++mt)
            model().clearData(mt, index, Util::RawDataRole);
    }

    // Set IDs if possible.  Will auto-reset in destructor.
    const SetId setId(uid, gid, mainWindow);

    const ContainerState cstate = state();
    const bool   running        = lxc()->is_running(lxc());
    const pid_t  pid            = lxc()->init_pid(lxc());

    // don't clobber transitional states
    if (!m_stateInTransition)
        model().setData(ContainerModel::State,  index, int(cstate), Util::RawDataRole);

    if (running != prevRunning || force) {
        // Config file
        {
            const char*  configFile     = lxc()->config_file_name(lxc());
            model().setData(ContainerModel::ConfigFile, index, (configFile != nullptr) ? configFile : na, Util::RawDataRole);
            free((void*)configFile);
        }

        // Control
        {
            const bool   mayControl     = lxc()->may_control(lxc());
            model().setData(ContainerModel::Control,index, mayControl, Util::RawDataRole);
        }
    }

    if (running) {
        // Gather data.  TODO: reduce update interval on some that don't change often.
        const quint64  memory        = getCgroupItem<uint64_t>(pid, 64, ContainerModel::Memory);
        const quint64  kMem          = getCgroupItem<uint64_t>(pid, 64, ContainerModel::KMem);
        const quint64  memoryLimit   = getCgroupItem<uint64_t>(pid, 64, ContainerModel::MemoryLimit);
        const quint64  uptime        = procUptime(pid);
        const quint64  cpuUse        = getCgroupItem<uint64_t>(pid, 64, ContainerModel::CpuUse);
        const quint32  processes     = cgroups.lineCount(pid, "cgroup.procs", "memory");
        const quint32  cpuShares     = getCgroupItem<uint64_t>(pid, 64, ContainerModel::CpuShares);
        const qint64   cfsQuota      = getCgroupItem<int64_t>(pid, 64, ContainerModel::CfsQuota);
        const qint64   cfsPeriod     = getCgroupItem<int64_t>(pid, 64, ContainerModel::CfsPeriod);
        const float    memPct        = (memoryLimit) == 0 ? 0.0 : (float(memory) / float(memoryLimit));
        const float    dTime         = float(elapsed.restart()) / 1000.0;
        const float    loadAvg       = std::min((dTime > 0.5 && prevCpuUse != 0) ?
                                                (float(cpuUse - prevCpuUse) / float(dTime) / 1e9) : 0.0, 32.0);

        // It would be better to store the tx/rx data as uint64_t, but QVariant doesn't natively
        // hold containers of those, so, meh... this is easy for the moment.
        QStringList hlink, clink, hwAddr, ipv4, ipv6;
        SizeList txBytes, rxBytes, txrxBytes, txSpeed, rxSpeed, txrxSpeed;
        quint64 txBytesTotal, rxBytesTotal, txrxBytesTotal, txSpeedTotal, rxSpeedTotal, txrxSpeedTotal;

        interfaces(hlink, hwAddr, txBytes, rxBytes, txrxBytes, txBytesTotal, rxBytesTotal, txrxBytesTotal);
        addresses(ipv4, ipv6);
        rates(dTime, txBytes, rxBytes,
              txSpeed, rxSpeed, txrxSpeed,
              txSpeedTotal, rxSpeedTotal, txrxSpeedTotal);

#if USEIO
        quint64 readBytes, writeBytes, readSpeed, writeSpeed;
        blockIo(pid, dTime, readBytes, writeBytes, readSpeed, writeSpeed);
#endif

        // Also add new items to offline clear set below
        // TODO: add convenience function to supply proper role
        model().setData(ContainerModel::Uptime,       index, uptime,             Util::RawDataRole);
        model().setData(ContainerModel::CpuUse,       index, cpuUse,             Util::RawDataRole);
        model().setData(ContainerModel::CpuShares,    index, cpuShares,          Util::RawDataRole);
        model().setData(ContainerModel::CfsQuota,     index, cfsQuota,           Util::RawDataRole);
        model().setData(ContainerModel::CfsPeriod,    index, cfsPeriod,          Util::RawDataRole);
        model().setData(ContainerModel::Load,         index, loadAvg,            Util::RawDataRole);
        model().setData(ContainerModel::Memory,       index, memory,             Util::RawDataRole);
        model().setData(ContainerModel::MemoryLimit,  index, memoryLimit,        Util::RawDataRole);
        model().setData(ContainerModel::MemPct,       index, memPct,             Util::RawDataRole);
        model().setData(ContainerModel::KMem,         index, kMem,               Util::RawDataRole);
        model().setData(ContainerModel::Processes,    index, processes,          Util::RawDataRole);
        model().setData(ContainerModel::HLink,        index, hlink.join('\n'),   Util::RawDataRole);
        model().setData(ContainerModel::TxBytes,      index, txBytes,            Util::RawDataRole);
        model().setData(ContainerModel::TxBytes,      index, txBytesTotal,       Util::RawDataRole);
        model().setData(ContainerModel::RxBytes,      index, rxBytes,            Util::RawDataRole);
        model().setData(ContainerModel::RxBytes,      index, rxBytesTotal,       Util::RawDataRole);
        model().setData(ContainerModel::TxRxBytes,    index, txrxBytes,          Util::RawDataRole);
        model().setData(ContainerModel::TxRxBytes,    index, txrxBytesTotal,     Util::RawDataRole);
        model().setData(ContainerModel::TxSpeed,      index, txSpeed,            Util::RawDataRole);
        model().setData(ContainerModel::TxSpeed,      index, txSpeedTotal,       Util::RawDataRole);
        model().setData(ContainerModel::RxSpeed,      index, rxSpeed,            Util::RawDataRole);
        model().setData(ContainerModel::RxSpeed,      index, rxSpeedTotal,       Util::RawDataRole);
        model().setData(ContainerModel::TxRxSpeed,    index, txrxSpeed,          Util::RawDataRole);
        model().setData(ContainerModel::TxRxSpeed,    index, txrxSpeedTotal,     Util::RawDataRole);
        model().setData(ContainerModel::HWAddress,    index, hwAddr.join('\n'),  Util::RawDataRole);
#if USEIO
        model().setData(ContainerModel::ReadBytes,    index, readBytes,          Util::RawDataRole);
        model().setData(ContainerModel::WriteBytes,   index, writeBytes,         Util::RawDataRole);
        model().setData(ContainerModel::ReadSpeed,    index, readSpeed,          Util::RawDataRole);
        model().setData(ContainerModel::WriteSpeed,   index, writeSpeed,         Util::RawDataRole);
#endif
        model().setData(ContainerModel::IPV4,         index, ipv4.join('\n'),    Util::RawDataRole);
        model().setData(ContainerModel::IPV6,         index, ipv6.join('\n'),    Util::RawDataRole);

        if (!prevRunning || force) {
            // Ephemeral
            {
                const char*    cfgEphemeral  = lxc()->get_running_config_item(lxc(), "lxc.ephemeral");
                const bool     ephemeral     = cfgEphemeral != nullptr && cfgEphemeral[0] == '1';
                model().setData(ContainerModel::Ephemeral,    index, ephemeral, Util::RawDataRole);
                free((void *)cfgEphemeral);
            }

            // Unprivileged
            {
                const char*    cfgIdMap      = lxc()->get_running_config_item(lxc(), "lxc.idmap");
                const bool     unprivileged  = (cfgIdMap != nullptr);

                model().setData(ContainerModel::Unprivileged, index, unprivileged, Util::RawDataRole);
                free((void *)cfgIdMap);
            }
        }

        prevCpuUse     = cpuUse;  // to simulate load avg
        prevUptime     = uptime;
        prevRxBytes    = rxBytes; // for transmission rates
        prevTxBytes    = txBytes; // ...
#if USEIO
        prevReadBytes  = readBytes;
        prevWriteBytes = writeBytes;
#endif

        static const QVector<QRegExp> osReleaseData =
                { QRegExp("ID"),         QRegExp("VERSION_ID"),  QRegExp("VERSION_CODENAME") };

        asyncSet(index, { },
                 { ContainerModel::Kernel, ContainerModel::Machine, ContainerModel::OS },
                 ThreadLxcData::FinishCommand::Words,
                 "/bin/uname", "-orm");

        asyncSet(index, osReleaseData,
                 { ContainerModel::Distro, ContainerModel::Release, ContainerModel::Codename },
                 ThreadLxcData::FinishCommand::EnvFile,
                 "/bin/cat", "/etc/os-release");

        asyncSet(index, { }, { ContainerModel::CLink },
                 ThreadLxcData::FinishCommand::Data,
                 "/bin/ls", "-C", "/sys/class/net");
    } else {
        if (prevRunning || force) {
            // This data can be gathered from non-running containers
            {
                const char*  configFile     = lxc()->config_file_name(lxc());

                // Unprivileged
                {
                    const bool unprivileged = (lxc()->get_config_item(lxc(), "lxc.idmap", nullptr, 0) != 0);
                    model().setData(ContainerModel::Unprivileged, index, unprivileged, Util::RawDataRole);
                }
                // Memory limit
                {
                    const uint64_t memoryLimit  = LxcUtils::readConfig<uint64_t>(configFile, "lxc.cgroup.memory.limit_in_bytes");
                    model().setData(ContainerModel::MemoryLimit, index, quint64(memoryLimit), Util::RawDataRole);
                }

                free((void*)configFile);
            }

            // reset other stuff. TODO: automate this somehow, while avoiding the few we want to preserve
            model().clearData(ContainerModel::Uptime,     index, Util::RawDataRole);
            model().clearData(ContainerModel::CpuUse,     index, Util::RawDataRole);
            model().clearData(ContainerModel::CpuShares,  index, Util::RawDataRole);
            model().clearData(ContainerModel::CfsQuota,   index, Util::RawDataRole);
            model().clearData(ContainerModel::CfsPeriod,  index, Util::RawDataRole);
            model().clearData(ContainerModel::Load,       index, Util::RawDataRole);
            model().clearData(ContainerModel::Memory,     index, Util::RawDataRole);
            model().clearData(ContainerModel::MemPct,     index, Util::RawDataRole);
            model().clearData(ContainerModel::KMem,       index, Util::RawDataRole);
            model().clearData(ContainerModel::Ephemeral,  index, Util::RawDataRole);
            model().clearData(ContainerModel::Processes,  index, Util::RawDataRole);
            model().clearData(ContainerModel::HLink,      index, Util::RawDataRole);
            model().clearData(ContainerModel::CLink,      index, Util::RawDataRole);
            model().clearData(ContainerModel::TxBytes,    index, Util::RawDataRole);
            model().clearData(ContainerModel::TxBytes,    index, Util::RawDataRole);
            model().clearData(ContainerModel::RxBytes,    index, Util::RawDataRole);
            model().clearData(ContainerModel::RxBytes,    index, Util::RawDataRole);
            model().clearData(ContainerModel::TxRxBytes,  index, Util::RawDataRole);
            model().clearData(ContainerModel::TxRxBytes,  index, Util::RawDataRole);
            model().clearData(ContainerModel::TxSpeed,    index, Util::RawDataRole);
            model().clearData(ContainerModel::TxSpeed,    index, Util::RawDataRole);
            model().clearData(ContainerModel::RxSpeed,    index, Util::RawDataRole);
            model().clearData(ContainerModel::RxSpeed,    index, Util::RawDataRole);
            model().clearData(ContainerModel::TxRxSpeed,  index, Util::RawDataRole);
            model().clearData(ContainerModel::TxRxSpeed,  index, Util::RawDataRole);
            model().clearData(ContainerModel::HWAddress,  index, Util::RawDataRole);
#if USEIO
            model().clearData(ContainerModel::ReadBytes,  index, Util::RawDataRole);
            model().clearData(ContainerModel::WriteBytes, index, Util::RawDataRole);
            model().clearData(ContainerModel::ReadSpeed,  index, Util::RawDataRole);
            model().clearData(ContainerModel::WriteSpeed, index, Util::RawDataRole);
#endif
            model().clearData(ContainerModel::IPV4,       index, Util::RawDataRole);
            model().clearData(ContainerModel::IPV6,       index, Util::RawDataRole);
        }
    }

    prevRunning = running;
}

bool ContainerPrivateLxc::set(ModelType mt, const QVariant& value)
{
    const pid_t pid     = lxc()->init_pid(lxc());
    const bool  running = lxc()->is_running(lxc());

    switch (mt) {
    case ContainerModel::MemoryLimit:
    case ContainerModel::CpuShares:
    case ContainerModel::CfsQuota:
    case ContainerModel::CfsPeriod:
        if (!running)
            return false;
        return setCgroupItem(pid, value, mt);

    default: return false;
    }
}

template <typename... T>
bool ContainerPrivateLxc::asyncControl(const QModelIndex& idx, ContainerControl ctrl, const T&... t)
{
    const SetId setId(uid, gid, mainWindow);

    if (lxc() == nullptr)
        return false;

    // only start thread if there's not already one attempting to do the same thing.
    if (threadsetContains<ThreadLxcCtrl>(ctrlThreads, idx) != nullptr)
        return false;
    
    // Set transitional state.  It'll update with the real state next display update.
    // This happens before kicking off the thread.
    setTransitionState(idx, ctrl);
    bootTimeInTcks = 0; // reset boot time cache, so it refreshes next time

    // Try to stop all UI update threads that might be using that container.
    // TODO encapsulate thread cancel
    dataThreads.cancelAll();

    // Since it is a thread pol with limited threads going at once, we will
    // increment the task count before the threads actually start.
    ctrlThreads.start(new ThreadLxcCtrl(mainWindow, *this, ctrl, idx, t...));

    return true;
}


bool ContainerPrivateLxc::control(const QModelIndex& idx, ContainerControl cc)
{
    m_stateInTransition = true;

    if (lxc() == nullptr)
        return false;

    // Auto-succeed if we're in the requested final state already.
    if (ContainerState(model().data(ContainerModel::State, idx, Util::RawDataRole).toInt()) ==
            ContainerControlInfo::finalState(cc))
        return true;

    switch (cc) {
    // launch these in a background thread to avoid blocking UI
    case ContainerControl::Start:    [[fallthrough]];
    case ContainerControl::Stop:     [[fallthrough]];
    case ContainerControl::Freeze:   [[fallthrough]];
    case ContainerControl::Thaw:     [[fallthrough]];
    case ContainerControl::Reboot:   [[fallthrough]];
    case ContainerControl::Remove:
        return asyncControl(idx, cc);

    case ContainerControl::ExecuteIn: return false; // done via execute()
    case ContainerControl::Create:    // TODO: ...
    default:
        return false;
    }
}

bool ContainerPrivateLxc::execute(const QModelIndex& idx, const QByteArray& cmdline,
                                  const QByteArray& progstr, const QVector<QByteArray>& argvs,
                                  int timeout, ThreadSet* threadSet)
{
    if (timeout < 0)
        timeout = cfgData().dataThreadTimeout;

    if (threadSet == nullptr)
        threadSet = &dataThreads;

    ThreadData* thread = (new ThreadLxcData(mainWindow, this, timeout, progstr, argvs))->requestLogAsync(idx, cmdline);

    if (thread == nullptr)
        return false;

    return threadSet->start(thread);
}

// in nanoseconds
quint64 ContainerPrivateLxc::procUptime(pid_t pid) const
{
    if (bootTimeInTcks == 0) {
        const auto stat = Util::ReadFilev<QByteArray>(1024, "/proc/%d/stat", pid);

        int pos = stat.indexOf(") ");

        for (int i = 0; i < 20 && pos > 0; ++i)
            pos = stat.indexOf(' ', pos+1);

        if (pos <= 0)
            return 0;

        bootTimeInTcks = strtoull(stat.data() + pos, nullptr, 10);
    }

    auto uptimeStr = Util::ReadFile<QByteArray>("/proc/uptime", 128);

    uptimeStr.truncate(uptimeStr.indexOf(' '));
    const double uptimeInTcks = uptimeStr.toDouble() * sysconf(_SC_CLK_TCK);

    // Convert return to nanoseconds
    return (uptimeInTcks - bootTimeInTcks) * (1e9 / sysconf(_SC_CLK_TCK));
}

void ContainerPrivateLxc::rates(float dTime, const SizeList& txBytes, const SizeList& rxBytes,
                                SizeList& txSpeed, SizeList& rxSpeed, SizeList& txrxSpeed,
                                quint64& txSpeedTotal, quint64& rxSpeedTotal, quint64& txrxSpeedTotal) const
{
    txSpeedTotal = 0;
    rxSpeedTotal = 0;
    txrxSpeedTotal = 0;

    // Calculate tx/rx speeds
    if (prevTxBytes.size() >= txBytes.size()) {
        for (int s = 0; s < rxBytes.size(); ++s) {
            const quint64 txRate = float(txBytes[s].toULongLong() - prevTxBytes[s].toULongLong()) / dTime;
            const quint64 rxRate = float(rxBytes[s].toULongLong() - prevRxBytes[s].toULongLong()) / dTime;
            const quint64 txrxRate = txRate + rxRate;

            txSpeed.push_back(txRate);
            rxSpeed.push_back(rxRate);
            txrxSpeed.push_back(txrxRate);
            txSpeedTotal   += txRate;
            rxSpeedTotal   += rxRate;
            txrxSpeedTotal += txrxRate;
        }
    }
}

void ContainerPrivateLxc::interfaces(QStringList& interfaces, QStringList& hwAddr,
                                     SizeList& tx, SizeList& rx, SizeList& txrx,
                                     quint64& txTotal, quint64& rxTotal, quint64& txrxTotal) const
{
    std::array<char, PATH_MAX> buf;

    txTotal = 0;
    rxTotal = 0;

    for (int num = 0; ;num++) {
        snprintf(buf.data(), sizeof(buf), "lxc.net.%d.type", num);

        const char* type = lxc()->get_running_config_item(lxc(), buf.data());
        if (type == nullptr)
            break;

        if (strcmp(type, "veth") == 0)
            snprintf(buf.data(), sizeof(buf), "lxc.net.%d.veth.pair", num);
        else
            snprintf(buf.data(), sizeof(buf), "lxc.net.%d.link", num);

        free((void *)type);

        const char* ifname = lxc()->get_running_config_item(lxc(), buf.data());
        if (ifname == nullptr)
            return;

        interfaces.push_back(ifname);

         // tx/rx reversed from container's perspective)    
        const auto txBytes = Util::ReadFilev<quint64>(64, "/sys/class/net/%s/statistics/tx_bytes", ifname);
        const auto rxBytes = Util::ReadFilev<quint64>(64, "/sys/class/net/%s/statistics/rx_bytes", ifname);

        tx.push_back(txBytes);
        rx.push_back(rxBytes);
        txrx.push_back(txBytes + rxBytes);
        txTotal += txBytes;
        rxTotal += rxBytes;
        txrxTotal += txBytes + rxBytes;

        auto address = Util::ReadFilev<QByteArray>(64, "/sys/class/net/%s/address", ifname);
        hwAddr.push_back(Util::RemoveNewline(address));
    }
}

void ContainerPrivateLxc::addresses(QStringList &ipv4, QStringList &ipv6) const
{
    const auto getIps = [this](QStringList& out, const char* family) {
        char **ips = lxc()->get_ips(lxc(), nullptr, family, 0);
        if (ips != nullptr) {
            int i;
            for (i = 0; ips[i] != nullptr; ++i)
                out.push_back(ips[i]);

            Util::FreeCArray(&ips, i);
        }
    };

    getIps(ipv4, "inet");
    getIps(ipv6, "inet6");
}

#if USEIO
void ContainerPrivateLxc::blockIo(pid_t pid, float dTime,
                                  quint64& readBytes, quint64& writeBytes,
                                  quint64& readSpeed, quint64& writeSpeed) const
{
    readBytes = writeBytes = readSpeed = writeSpeed = 0;

    // This is marked ReadBytes, but write data comes from the same source.
    const auto lines = getCgroupItem<QList<QByteArray>>(pid, 4096, ContainerModel::ReadBytes);

    if (lines.empty())
        return;

    for (const auto& line : lines) {
        const int space1 = line.indexOf(' ');
        const int space2 = line.indexOf(' ', space1);

        if (space2 < 0)
            continue;

        const char* value = line.data() + space2 + 1;

        if (strncmp(line.data() + space1, "Read", 4) == 0)
            readBytes += strtoull(value, nullptr, 10);
        if (strncmp(line.data() + space1, "Write", 4) == 0)
            writeBytes += strtoull(value, nullptr, 10);
    }

    readSpeed  = float(readBytes - prevReadBytes) / float(dTime);
    writeSpeed = float(writeBytes - prevWriteBytes) / float(dTime);
}
#endif

ContainerState ContainerPrivateLxc::state(lxc_container* lxc)
{
    // We are not supposed to free this.  liblxc owns it.
    const char*  stateStr = lxc->state(lxc);

    if (strcasecmp(stateStr, "RUNNING") == 0)
        return ContainerState::Running;
    if (strcasecmp(stateStr, "STOPPED") == 0)
        return ContainerState::Stopped;
    if (strcasecmp(stateStr, "Frozen") == 0)
        return ContainerState::Frozen;
    if (strcasecmp(stateStr, "Defined") == 0)
        return ContainerState::Defined;

    return ContainerState::Unknown;

}

ContainerState ContainerPrivateLxc::state() const
{
    return state(lxc());
}
