/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CFGDATA_H
#define CFGDATA_H

#include <QString>
#include <QByteArray>
#include <QColor>
#include <QHash>
#include <QFont>
#include <QGuiApplication>
#include <QPalette>

#include <src/core/containermodel.h>
#include <src/core/cfgdatabase.h>
#include <src/core/colorizermodel.h>
#include <src/util/units.h>
#include "containerclass.h"
#include "containerstate.h"

class QStandardItemModel;

// container directory configuration data
struct CfgDir : public Settings
{
    explicit CfgDir() : containerClass(ContainerClass::None) { }

    explicit CfgDir(const QString& directory, ContainerClass containerClass, const QString& name = "",
       const QByteArray& uid = "", const QByteArray& gid = "");

    explicit CfgDir(const QStandardItemModel& m, int row);

    void set(QStandardItemModel& m, int row) const;

    bool operator<(const CfgDir& other) const { return directory < other.directory; }
    static bool dirNameEqual(const CfgDir& d0, const CfgDir& d1) { return d0.expandedDir() == d1.expandedDir(); }

    QByteArray     expandedDir() const;

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    QString        directory;
    QString        displayName;
    ContainerClass containerClass;
    QByteArray     uid;
    QByteArray     gid;

private:
    static QString genDisplayName(const QString& directory, ContainerClass containerClass);
};

// TODO: these should be configurable in the GUI
struct CfgShell : public Settings
{
    CfgShell(const QString& name, const QByteArray& exec) : name(name), exec(exec) { }

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    QString       name;
    QByteArray    exec;
};

//// Data tracked by dialog
struct CfgData : public CfgDataBase
{
    explicit CfgData();

    QVector<CfgShell> shells;

    int           dataUpdateInterval;   // container display update interval
    int           listUpdateInterval;   // container list update interval
    int           dataThreadTimeout;    // data thread timeout in seconds
    int           cmdThreadTimeout;     // command (console) thread timeout
    bool          viewAsTree;           // default container view
    bool          copyOnlyContainers;   // omit non-containers when copying to clipboard
    int           ctrlThreadCount;      // # of threads for container cmds (start/stop/etc)
    int           dataThreadCount;      // # of threads for querying async data from containers
    int           cmdThreadCount;       // # of threads for interactive shell commands
    bool          directCgroups;        // directly set cgroup data, bypassing liblxc

    Units         unitsNetwork;         // units for network size and rates
    Units         unitsMemory;          // units for memory display
    Units         unitsUptime;          // units for uptime display
    Units         unitsCpuUse;          // units for CPU use
    Units         unitsPercent;         // units for percentages

    int           graphDataPoints;      // how many data points to keep for linear graphs
    int           graphLineWidth;       // line width for graph pens
    int           graphAreaOpacity;     // opactiy of area graphs
    int           defaultGraphMin;      // default graph minutes
    int           defaultGraphSec;      // default graph seconds
    bool          dataInGraphLabels;    // true to add data to graph labels

    int           consoleHistoryLines;  // console history lines
    int           commandHistoryLines;  // command history lines

    QFont         consoleFont;          // font for consoles/fixed-width things

    QIcon         iconDir;              // icon to use for directories
    QIcon         iconCheck;            // icon to use for checkmarks

    ColorizerModel containerColorizer;  // container text colorization
    ColorizerModel graphColorizer;      // line/etc graph colorization

    int defaultGraphSeconds() const { return std::max(defaultGraphMin * 60 + defaultGraphSec, 15); }

    void uniquifyDirectories();
    const QList<CfgDir>& directories() const;
    void updateDirectories(const QStandardItemModel&);

    static const Units& units(ModelType, bool autoTime = false);

    static const int msPerUpdateTick = 1000;

    void reset() override; // reset to defaults

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

private:
    CfgData& operator=(const CfgDataBase& rhs) override {
        return *this = reinterpret_cast<const CfgData&>(rhs);
    }

    mutable QList<CfgDir> m_directories;  // directories for finding containers

    // This is the compile-time current cfg data version.  On load, older save versions will be
    // updated to this version via updateFormat().
    static const constexpr uint32_t currentCfgDataVersion = 1;

    void defaultContainerColorizers();
    void defaultGraphColorizers();
    void updateFormat();           // update old save formats to latest format
};

#endif // CFGDATA_H
