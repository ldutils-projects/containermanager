/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERSTATE_H
#define CONTAINERSTATE_H

#include <QString>
#include <QColor>

enum class ContainerState {
    _Begin = 0,
    Unknown = _Begin,
    _FirstKnownState,
    Running = _FirstKnownState,
    Stopped,
    Frozen,
    Defined,
    _NumStaticStates,

    // Entries from here on are transition states
    Starting = _NumStaticStates,
    Stopping,                 // stop triggered, but not stopped
    Rebooting,                // rebooting (hopefully...)
    Freezing,                 // getting colder
    Thawing,                  // getting warmer
    Executing,                // executing command
    _NumUIStates,             // last one in the color UI, for now
    Creating = _NumUIStates,  // being created
    Removing,                 // being removed
    _Count,
};

class ContainerStateInfo {
public:
    static QString text(ContainerState s);
    static QColor color(ContainerState s);  // seed for preferences

private:
    ContainerStateInfo(const ContainerStateInfo&) = delete;
    ContainerStateInfo& operator=(const ContainerStateInfo&) = delete;
};

#endif // CONTAINERSTATE_H
