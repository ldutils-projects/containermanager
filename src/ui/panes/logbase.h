/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOGBASE_H
#define LOGBASE_H

#include <QHash>
#include <QSet>
#include <QMenu>
#include <QTreeWidgetItem>
#include <QStandardItemModel>
#include <QModelIndexList>

#include "src/core/outputs.h"
#include "mappedbase.h"

namespace Ui {
class LogBasePane;
} // namespace Ui

class QPlainTextEdit;
class QToolButton;
enum class UiType;

class LogBase : public MappedBase
{
    Q_OBJECT

public:
    LogBase(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent);
    ~LogBase() override;

    void setContainerSet(const QModelIndexList&);

    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

public slots:
    // Send some data into the log, for the container belonging to idx.
    virtual void append(const QModelIndex& idx, int rc, const QVector<QByteArray>& output) = 0;
    virtual void cancelled(const QModelIndex& idx) = 0;

protected slots:
    virtual void setLogDisplayLines(int count) const;
    void showActionContextMenu(const QPoint& pos);

private slots:
    void on_LogBasePane_toggled(bool checked) { paneToggled(checked); }
    void on_action_AddAllRunning_triggered();
    void on_action_SelectExpanded_triggered();
    void on_action_RemoveUnselected_triggered();

protected:
    void     expandAll() override { setExpanded(true); }
    void     collapseAll() override { setExpanded(false); }
    void     selectAll() override;
    void     selectNone() override;
    bool     hasSelection() const override;
    bool     isSelected(const QModelIndex& idx) const override;
    QModelIndexList getSelections() const override;
    bool     supportsAction(PaneAction pa) const override;
    void     newConfig() override;

    QToolButton* createClearButton() const;
    void     setQueryColumnUi(int column);
    void     setQuery(const QString& pattern) override;
    void     processModelReset() override;
    bool     accept(const QModelIndex& idx) const override;
    void     remove(const QModelIndex& idx) override;
    void     insert(const QModelIndex& idx, int pos) override;
    bool     contains(const QModelIndex& idx) const override;
    void     show(const QModelIndex& idx, int pos = -1) override;
    void     hide(const QModelIndex& idx) override;
    bool     shown(const QModelIndex& idx) const override;
    void     clear() override;
    virtual void clearLogs() = 0;

    void setTitle(const QString& title);
    void setupContextMenus(); // must call from subclass constructors

    template <typename WT>
    WT*              indexToWidget(const QModelIndex& idx) const;
    QTreeWidgetItem* indexToTreeItem(const QModelIndex& idx) const;
    QTreeWidgetItem* indexToParentItem(const QModelIndex& idx) const;

    static QTreeWidgetItem* widgetToParentItem(const QWidget* widget);
    static QTreeWidgetItem* widgetToItem(const QWidget* widget);
    static QModelIndex widgetToIndex(const QWidget* widget);

    static void appendStr(QPlainTextEdit* pte, const QColor&, const char* text);
    static void appendStr(QPlainTextEdit* pte, const QColor&, const QVector<QByteArray>& output,
                          Out outIndex, const char* prefix = nullptr);

    static QWidget* logWidgetfactory();

    static void appendStr(QPlainTextEdit* pte, UiType color, const QVector<QByteArray>& output,
                          Out outIndex, const char* prefix = nullptr);

    template <typename WT, typename METHOD, typename... T>
    void applyToWidgets(const METHOD& method, const T&... t);

    template <typename METHOD, typename... T>
    void applyToParents(const METHOD& method, const T&... t);

    Ui::LogBasePane* ui;
    int          nameColumn;   // column in which to display container names
    QHash<QPersistentModelIndex, QWidget*> logMap; // cache for efficiency

private:
    bool canHide() const override { return true; }
    void setupSignals();
    void setupSubsetComboBox();
    void setExpanded(bool);

    static void setLogDisplayLines(QPlainTextEdit* pte, int count);

    QStandardItemModel          filterColumnModel;
    QSet<QPersistentModelIndex> containerSet; // view log for these containers

    static const constexpr char* indexProperty = "lb-index";
    static const constexpr char* widgetItemProperty = "lb-widgetitem";
};

template <typename WT>
WT* LogBase::indexToWidget(const QModelIndex& idx) const
{
    const auto it = logMap.find(idx);
    return (it == logMap.end()) ? nullptr : dynamic_cast<WT*>(*it);
}

template <typename WT, typename METHOD, typename... T>
void LogBase::applyToWidgets(const METHOD& method, const T&... t)
{
    for (auto& widget : logMap)
        if (WT* wt = dynamic_cast<WT*>(widget))
            (wt->*method)(t...);
}

template <typename METHOD, typename... T>
void LogBase::applyToParents(const METHOD& method, const T&... t)
{
    for (auto& widget : logMap)
        if (QTreeWidgetItem* parent = widgetToParentItem(widget))
            (parent->*method)(t...);
}

#endif // LOGBASE_H
