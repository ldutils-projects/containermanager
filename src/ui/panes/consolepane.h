/*
    Copyright 2018-2020 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef OUTPUTPANE_H
#define OUTPUTPANE_H

#include <QThreadPool>
#include <QLineEdit>

#include "src/core/threadbase.h"
#include "logbase.h"

class QLineEdit;
class QComboBox;
class QToolButton;

class ConsolePane final : public LogBase
{
    Q_OBJECT
public:
    ConsolePane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~ConsolePane() override;

public slots:
    void append(const QModelIndex& idx, int rc, const QVector<QByteArray>& output) override;
    void cancelled(const QModelIndex& idx) override;
    void threadFinished(int rc) const;
    void threadStarting(ThreadBase*) const;
    void execute(const QByteArray& cmdLine);
    void execute(const QString& cmdLine) { execute(cmdLine.toUtf8()); }
    void cancelAll() { cmdThreads.cancelAll(); }

protected:
    void newConfig() override;

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    class ConsoleInput : public QLineEdit {
    public:
        ConsoleInput(ConsolePane& console, QComboBox* history, QWidget* parent = nullptr) :
            QLineEdit(parent), console(console), history(history) { }
    private:
        void keyPressEvent(QKeyEvent *event) override;

        ConsolePane& console;
        QComboBox*   history;
    };

    enum class Columns {
        Rc = 0,
        Name,
    };

    QThreadPool   cmdThreadPool;
    ThreadSet     cmdThreads;

    QComboBox*    commandHistory;

    union { // union for settings support
        ConsoleInput* commandEdit;
        QLineEdit*    commandEditLE;
    };

    QComboBox*    shells;
    QToolButton*  cancelButton;

    void clearLogs() override;
    void setupInputUi();
    void setupTooltip();
    void setupShellComboBox();
    void setupTreeView();
    void setupColumnWidths();
    void setupContextMenus();
    void setupSignals();
    void setupActionIcons();
};

#endif // OUTPUTPANE_H
