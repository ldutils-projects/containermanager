/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINERCLASSLXC_H
#define CONTAINERCLASSLXC_H

#include <QString>
#include <QByteArray>
#include <QVector>
#include <QSet>
#include <QHash>
#include <QIcon>

#include "src/core/containerclass.h"

struct lxc_container;
class CfgData;
class ContainerPrivate;

class ContainerClassLxc : public ContainerClassBase
{
public:
    ContainerClassLxc(ContainerClass cclass, const QString& name, MainWindow& mainWindow);

    void updateList(ContainerModel& model, const QModelIndex& parent, const CfgDir& cfgDir,
                    QSet<const ContainerPrivate*>& updated) const final;

    void defaultDirs(QList<CfgDir>& dirs) const final;

    const QIcon& icon() const override { return classIcon; }

protected:
    using RootFsData = QVector<std::tuple<QByteArray, QByteArray>>;

    void collectRootFs(ContainerModel& mode, char** names, const char* path, int count,
                       RootFsData& rootfs, const CfgDir& cfgDir) const;

private:
    // cache to avoid re-reading lxc.rootfs.path each time, to avoid liblxc memory leak
    mutable QHash<QByteArray, QByteArray> rootfsCache;

    QIcon classIcon;
};

#endif // CONTAINERCLASSLXC_H
