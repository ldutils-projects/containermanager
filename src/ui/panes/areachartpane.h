/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef AREACHARTPANE_H
#define AREACHARTPANE_H

#include <QtCharts/QLineSeries>
#include <QtCharts/QAreaSeries>
#include <QPoint>
#include "linechartbase.h"

class AreaChartPane : public LineChartBase<QLineSeries, QAreaSeries>
{
    Q_OBJECT
    using Data      = QAreaSeries;
    using LowerData = QLineSeries;

public:
    AreaChartPane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~AreaChartPane() override;

private slots:
    // These must live here because they're Qt slots, which can't exist in a temlate method.
    void dataUpdated() override { LineChartBase::dataUpdated(); }
    void recalcAxes() override { LineChartBase::recalcAxes(); }
    void pan(QMouseEvent* event, const QPoint& rel) override { LineChartBase::pan(event, rel); }
    void endPan() override { LineChartBase::endPan(); }
    void hoverLegend(bool state) override { ChartBaseData::hoverLegend(QObject::sender(), state); }
    void selectLegend() override { ChartBaseData::selectLegend(QObject::sender()); }
    void hoverData(AreaChartPane::Data*, bool state) override;
    void hoverData(const QPointF& point, bool state);
    void selectData(const QPointF& point);
    void selectData(AreaChartPane::Data* data) override { ChartBaseData::selectData(data, [this]() { return chart->series(); }); }
    void setStacked(bool stacked) override { LineChartBase::setStacked(stacked); }

private:
    void insert(const QModelIndex& idx, int pos) override;
    Data* dataFactory() const override;
    bool isStacked() const override { return true; }
    void setupSignals();
    void updateChart() override;
    void visibilityChange() override;
    void setSelected(Data* data, bool select) override;
    void setAreaColor(Data *data, bool hovered) const;

    static void visibilityChange(const QList<QAbstractSeries*>&);

    LowerData* lower(Data* data) const override { return data->upperSeries(); }
    LowerData* lower(QAbstractSeries* data) const override {
        return lower(static_cast<Data*>(data));
    }
};

#endif // AREACHARTPANE_H
