/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QColorDialog>
#include <algorithm>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/ui.inl.h>
#include <src/util/icons.h>
#include <src/core/modelmetadata.inl.h>

#include "src/core/app.h"
#include "src/core/containerclass.h"
#include "src/core/containerstate.h"
#include "src/ui/widgets/colorizereditor.h"
#include "src/ui/windows/icondialog.h"

#include "ui_appconfig.h"
#include "mainwindow.h"
#include "appconfig.h"

AppConfig::AppConfig(MainWindow* mainWindow) :
    AppConfigBase(mainWindow),
    dirHeader(Qt::Horizontal, this),
    dirModel(this),
    containerClassDelegate(this),
    colorDelegate(this),
    containerColorizer(&app().containerModel()),
    graphColorizer(&app().containerModel()),
    mainWindow(*mainWindow),
    ui(nullptr)
{
}

AppConfig::~AppConfig()
{
    delete ui;
}

void AppConfig::setup()
{
    if (ui != nullptr)
        return;

    ui = new Ui::AppConfig;
    ui->setupUi(this);

    setLayout(&mainLayout);
    setupDirView();
    setupUIColors();
    setupUnitsInputs();
    setupContainerColorizerEditor();
    setupGraphColorizerEditor();
    setupActionIcons();
    Util::SetupWhatsThis(this);

    addAction(ui->action_Next_Tab);
    addAction(ui->action_Prev_Tab);
}

void AppConfig::setupActionIcons()
{
    int tab = 0;
    Icons::defaultIcon(ui->appCfgTabs, tab++, "configure");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "folder");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "clock");
    Icons::defaultIcon(ui->appCfgTabs, tab++, "measure");
}

void AppConfig::setupDirView()
{
    QTreeView& view = *ui->cfgContainerDirsListView;

    const QStringList header = { tr("Directory"),
                                 tr("Type"),
                                 tr("Display Name"),
                                 tr("UID"),
                                 tr("GID"),
                               };

    dirModel.setHorizontalHeaderLabels(header);

    view.setModel(&dirModel);
    view.setHeader(&dirHeader);
    view.setSortingEnabled(true);
    view.sortByColumn(0, Qt::AscendingOrder);
    view.setAlternatingRowColors(true);
    view.setSelectionBehavior(QAbstractItemView::SelectItems);
    view.setSelectionMode(QAbstractItemView::SingleSelection);
    view.setRootIsDecorated(false);  // act like a list

    view.setItemDelegateForColumn(1, &containerClassDelegate);
}

void AppConfig::setupUIColors()
{
    QTreeView& view = *ui->cfgUiColors;

     view.setModel(&cfgDataWritable().uiColor);
     view.setItemDelegateForColumn(UiColorModel::Color, &colorDelegate);

     Util::ResizeViewForData(view);
}

void AppConfig::setupUnitsInputs()
{
    cfgData().unitsUptime.addToComboBox(ui->cfgUptimeFormat);
    cfgData().unitsCpuUse.addToComboBox(ui->cfgCpuUseFormat);
    cfgData().unitsMemory.addToComboBox(ui->cfgMemoryFormat);
    cfgData().unitsNetwork.addToComboBox(ui->cfgNetworkFormat);
    cfgData().unitsPercent.addToComboBox(ui->cfgPercentFormat);
}

void AppConfig::setupContainerColorizerEditor()
{
    auto* colorizerEditor = new ColorizerEditor(containerColorizer, ContainerModel::headersList<ContainerModel>());

    ui->appCfgTabs->addTab(colorizerEditor, Icons::get("color-profile"), "Colorizer");
}

void AppConfig::setupGraphColorizerEditor()
{
    const QString htmlBegin = "<html><head/><body>";

    const QString graphIntro =
            tr("<p>This data allows customization of graph colors.  It maps containers to "
               "the graph colors used to draw them.  If containers are matched by more than "
               "one pattern, they will be assigned a color from one of the ranges in a round "
               "robin manner.  Multiple containers matched by a single pattern will be assigned "
               "an interpolated color between the <b>Fg Color</b> and <b>Bg Color</b>.</p>");

    const QString htmlEnd = "</body></html>";
    const QString graphTooltip = htmlBegin + graphIntro + htmlEnd;

    auto* colorizerEditor = new ColorizerEditor(graphColorizer, ContainerModel::headersList<ContainerModel>());

    colorizerEditor->treeView()->setToolTip(graphTooltip);
    colorizerEditor->treeView()->setWhatsThis(graphTooltip);
    colorizerEditor->treeView()->setColumnHidden(ColorizerModel::Icon, true);
    colorizerEditor->treeView()->setColumnHidden(ColorizerModel::HideText, true);

    ui->appCfgTabs->addTab(colorizerEditor, Icons::get("color-profile"), "Graph Colors");
}

void AppConfig::dirViewInsertAt(int row)
{
    row = std::max(row, 0);

    QTreeView& view = *ui->cfgContainerDirsListView;

    const QString directory = dirViewDialog();
    if (directory.isEmpty())
        return;

    dirModel.insertRow(row);

    // TODO: auto-deduce the ContainerClass by examining directory
    const CfgDir cfgDir(directory, ContainerClass::Lxc); // default to LXC

    const QModelIndex index(dirModel.index(row, 0));
    view.setCurrentIndex(index);    
    cfgDir.set(dirModel, row);
    resizeDirColumns();
}

QString AppConfig::dirViewDialog()
{
    const QFileDialog::Options options =
            QFileDialog::DontResolveSymlinks |
            QFileDialog::ReadOnly |
            QFileDialog::ShowDirsOnly;

    return QFileDialog::getExistingDirectory(this, tr("Containers Directory"), "/", options);
}

void AppConfig::on_cfgDirsAdd_clicked()
{
    dirViewInsertAt(dirModel.rowCount());
}

void AppConfig::on_cfgDirsDelete_clicked()
{
    QTreeView& view = *ui->cfgContainerDirsListView;

    dirModel.removeRow(view.currentIndex().row());
}

void AppConfig::on_cfgDirsInsert_clicked()
{
    QTreeView& view = *ui->cfgContainerDirsListView;

    dirViewInsertAt(view.currentIndex().row());
}

void AppConfig::on_cfgContainerDirsListView_doubleClicked(const QModelIndex &index)
{
    if (index.column() == 0) {
        // edit directory
        const QString directory = dirViewDialog();
        if (directory.isEmpty())
            return;

        const CfgDir cfgDir(directory, ContainerClass::Lxc); // default to LXC
        cfgDir.set(dirModel, index.row());

        ui->cfgContainerDirsListView->resizeColumnToContents(index.column());
    } else if (index.column() == 1) {
        // Will edit via combo box
    }
}

void AppConfig::updateUIFromCfg()
{
    if (ui == nullptr)
        return;

    // Update directories
    dirModel.removeRows(0, dirModel.rowCount());  // can't use clear(): that removes headers too
    for (const auto& dir : cfgData().directories())
        dir.set(dirModel, dirModel.rowCount());
    resizeDirColumns();

    // Units
    ui->cfgUptimeFormat->setCurrentIndex(cfgData().unitsUptime.rangeIdx());
    ui->cfgCpuUseFormat->setCurrentIndex(cfgData().unitsCpuUse.rangeIdx());
    ui->cfgMemoryFormat->setCurrentIndex(cfgData().unitsMemory.rangeIdx());
    ui->cfgNetworkFormat->setCurrentIndex(cfgData().unitsNetwork.rangeIdx());
    ui->cfgPercentFormat->setCurrentIndex(cfgData().unitsPercent.rangeIdx());

    ui->cfgUptimePrecision->setValue(cfgData().unitsUptime.precision());
    ui->cfgCpuUsePrecision->setValue(cfgData().unitsCpuUse.precision());
    ui->cfgMemoryPrecision->setValue(cfgData().unitsMemory.precision());
    ui->cfgNetworkPrecision->setValue(cfgData().unitsNetwork.precision());
    ui->cfgPercentPrecision->setValue(cfgData().unitsPercent.precision());

    ui->cfgUptimeZeros->setChecked(cfgData().unitsUptime.leadingZeros());
    ui->cfgCpuUseZeros->setChecked(cfgData().unitsCpuUse.leadingZeros());

    // Undo
    ui->maxUndoCount->setValue(cfgData().maxUndoCount);
    ui->maxUndoSizeMiB->setValue(double(cfgData().maxUndoSizeMiB));

    // Intervals
    ui->spinBoxDataRefresh->setValue(cfgData().dataUpdateInterval);
    ui->spinBoxListRefresh->setValue(cfgData().listUpdateInterval);
    ui->spinBoxDataTimeout->setValue(cfgData().dataThreadTimeout);
    ui->spinBoxCmdTimeout->setValue(cfgData().cmdThreadTimeout);

    ui->cfgContainerDirsListView->resizeColumnToContents(0);

    // Cut and paste
    ui->cfgContainerRowSep->setText(cfgData().rowSeparator);
    ui->cfgContainerColSep->setText(cfgData().colSeparator);
    ui->cfgContainerCopyOnlyC->setChecked(cfgData().copyOnlyContainers);

    // Threads
    ui->cfgContainerCtrlThreads->setValue(cfgData().ctrlThreadCount);
    ui->cfgContainerDataThreads->setValue(cfgData().dataThreadCount);
    ui->cfgContainerCmdThreads->setValue(cfgData().cmdThreadCount);

    // CGroups
    ui->directCgroups->setChecked(cfgData().directCgroups);

    // Filter behavior
    ui->caseSensitiveFilters->setChecked(cfgData().caseSensitiveFilters);
    ui->caseSensitiveSorting->setChecked(cfgData().caseSensitiveSorting);

    // UI stuff
    ui->cfgWarnOnClose->setChecked(cfgData().warnOnClose);
    ui->cfgWarnOnDelete->setChecked(cfgData().warnOnRemove);
    ui->cfgWarnOnRevert->setChecked(cfgData().warnOnRevert);
    ui->cfgWarnOnExit->setChecked(cfgData().warnOnExit);
    ui->inlineCompletion->setChecked(cfgData().inlineCompletion);
    ui->completionListSize->setValue(cfgData().completionListSize);
    ui->completionListSize->setDisabled(cfgData().inlineCompletion);

    // Graphs
    ui->cfgGraphDataPoints->setValue(cfgData().graphDataPoints);
    ui->cfgGraphLineWidth->setValue(cfgData().graphLineWidth);
    ui->cfgGraphAreaOpacity->setValue(cfgData().graphAreaOpacity);
    ui->cfgGraphMin->setValue(cfgData().defaultGraphMin);
    ui->cfgGraphSec->setValue(cfgData().defaultGraphSec);
    ui->cfgDataInGraphLabels->setChecked(cfgData().dataInGraphLabels);

    // Consoles
    ui->cfgConsoleHistoryLines->setValue(cfgData().consoleHistoryLines);
    ui->cfgCommandHistoryLines->setValue(cfgData().commandHistoryLines);

    // Fonts
    ui->cfgConsoleFont->setCurrentFont(cfgData().consoleFont);
    ui->cfgConsoleFontSize->setValue(cfgData().consoleFont.pointSize());

    // Backups and files
    ui->backupUICount->setValue(cfgData().backupUICount);
    ui->dataAutosaveInterval->setValue(cfgData().dataAutosaveInterval);

    // Models
    containerColorizer = cfgData().containerColorizer;
    graphColorizer     = cfgData().graphColorizer;

    cfgDataWritable().applyGlobal();
}

void AppConfig::updateCfgFromUI()
{
    if (ui == nullptr)
        return;

    CfgData& cfgData = cfgDataWritable();

    // Update directories
    cfgData.updateDirectories(dirModel);

    // Units
    cfgData.unitsUptime.setIdx(ui->cfgUptimeFormat->currentIndex());
    cfgData.unitsCpuUse.setIdx(ui->cfgCpuUseFormat->currentIndex());
    cfgData.unitsMemory.setIdx(ui->cfgMemoryFormat->currentIndex());
    cfgData.unitsNetwork.setIdx(ui->cfgNetworkFormat->currentIndex());
    cfgData.unitsPercent.setIdx(ui->cfgPercentFormat->currentIndex());

    cfgData.unitsUptime.setPrecision(ui->cfgUptimePrecision->value());
    cfgData.unitsCpuUse.setPrecision(ui->cfgCpuUsePrecision->value());
    cfgData.unitsMemory.setPrecision(ui->cfgMemoryPrecision->value());
    cfgData.unitsNetwork.setPrecision(ui->cfgNetworkPrecision->value());
    cfgData.unitsPercent.setPrecision(ui->cfgPercentPrecision->value());

    cfgData.unitsUptime.setLeadingZeros(ui->cfgUptimeZeros->isChecked());
    cfgData.unitsCpuUse.setLeadingZeros(ui->cfgCpuUseZeros->isChecked());

    // Undos
    cfgData.maxUndoCount         = ui->maxUndoCount->value();
    cfgData.maxUndoSizeMiB       = float(ui->maxUndoSizeMiB->value());

    // Intervals
    cfgData.dataUpdateInterval   = ui->spinBoxDataRefresh->value();
    cfgData.listUpdateInterval   = ui->spinBoxListRefresh->value();
    cfgData.dataThreadTimeout    = ui->spinBoxDataTimeout->value();
    cfgData.cmdThreadTimeout     = ui->spinBoxCmdTimeout->value();

    // Cut and paste
    cfgData.rowSeparator         = ui->cfgContainerRowSep->text();
    cfgData.colSeparator         = ui->cfgContainerColSep->text();
    cfgData.copyOnlyContainers   = ui->cfgContainerCopyOnlyC->isChecked();

    // Threads
    cfgData.ctrlThreadCount      = ui->cfgContainerCtrlThreads->value();
    cfgData.dataThreadCount      = ui->cfgContainerDataThreads->value();
    cfgData.cmdThreadCount       = ui->cfgContainerCmdThreads->value();

    // CGroups
    cfgData.directCgroups        = ui->directCgroups->isChecked();

    // Filter behavior
    cfgData.caseSensitiveFilters = ui->caseSensitiveFilters->isChecked();
    cfgData.caseSensitiveSorting = ui->caseSensitiveSorting->isChecked();

    // UI stuff
    cfgData.warnOnClose          = ui->cfgWarnOnClose->isChecked();
    cfgData.warnOnRemove         = ui->cfgWarnOnDelete->isChecked();
    cfgData.warnOnRevert         = ui->cfgWarnOnRevert->isChecked();
    cfgData.warnOnExit           = ui->cfgWarnOnExit->isChecked();
    cfgData.inlineCompletion     = ui->inlineCompletion->isChecked();
    cfgData.completionListSize   = ui->completionListSize->value();

    // Graphs
    cfgData.graphDataPoints      = ui->cfgGraphDataPoints->value();
    cfgData.graphLineWidth       = ui->cfgGraphLineWidth->value();
    cfgData.graphAreaOpacity     = ui->cfgGraphAreaOpacity->value();
    cfgData.defaultGraphMin      = ui->cfgGraphMin->value();
    cfgData.defaultGraphSec      = ui->cfgGraphSec->value();
    cfgData.dataInGraphLabels    = ui->cfgDataInGraphLabels->isChecked();

    // Consoles
    cfgData.consoleHistoryLines  = ui->cfgConsoleHistoryLines->value();
    cfgData.commandHistoryLines  = ui->cfgCommandHistoryLines->value();

    // Fonts
    cfgData.consoleFont          = ui->cfgConsoleFont->currentFont();
    cfgData.consoleFont.setPointSize(ui->cfgConsoleFontSize->value());

    if (cfgData.defaultGraphMin == 0)
        cfgData.defaultGraphSec = std::max(cfgData.defaultGraphSec, 15);

    // Backups and files
    cfgData.backupUICount        = ui->backupUICount->value();
    cfgData.dataAutosaveInterval = ui->dataAutosaveInterval->value();

    // Models
    cfgData.containerColorizer   = containerColorizer;
    cfgData.graphColorizer       = graphColorizer;

    cfgData.applyGlobal();
}

void AppConfig::on_cfgContainerButtons_clicked(QAbstractButton *button)
{
    appCfgButtons(ui->cfgContainerButtons, button);
}

void AppConfig::resizeDirColumns()
{
    QTreeView& view = *ui->cfgContainerDirsListView;

    Util::ResizeViewForData(view);
}

void AppConfig::on_action_Next_Tab_triggered()
{
    Util::NextTab(ui->appCfgTabs);
}

void AppConfig::on_action_Prev_Tab_triggered()
{
    Util::PrevTab(ui->appCfgTabs);
}
