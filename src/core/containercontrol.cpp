/*
    Copyright 2018 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QObject>

#include "containercontrol.h"
#include "containerstate.h"

QString ContainerControlInfo::name(ContainerControl cc)
{
    switch (cc) {
    case ContainerControl::Start:     return QObject::tr("Start");
    case ContainerControl::Stop:      return QObject::tr("Stop");
    case ContainerControl::Freeze:    return QObject::tr("Freeze");
    case ContainerControl::Thaw:      return QObject::tr("Thaw");
    case ContainerControl::Reboot:    return QObject::tr("Reboot");
    case ContainerControl::ExecuteIn: return QObject::tr("Execute In");
    case ContainerControl::Create:    return QObject::tr("Create");
    case ContainerControl::Remove:    return QObject::tr("Remove");
    case ContainerControl::_Count:    break;
    }

    assert(0 && "Unknown ContainerControl value");
    return "";
}

ContainerState ContainerControlInfo::transitionState(ContainerControl cc)
{
    switch (cc) {
    case ContainerControl::Start:     return ContainerState::Starting;
    case ContainerControl::Stop:      return ContainerState::Stopping;
    case ContainerControl::Freeze:    return ContainerState::Frozen;
    case ContainerControl::Thaw:      return ContainerState::Running;
    case ContainerControl::Reboot:    return ContainerState::Rebooting;
    case ContainerControl::ExecuteIn: return ContainerState::Executing;
    case ContainerControl::Create:    return ContainerState::Defined;
    case ContainerControl::Remove:    return ContainerState::Unknown;
    case ContainerControl::_Count:    break;
    }

    assert(0 && "Unknown ContainerControl value");
    return ContainerState::Unknown;
}

ContainerState ContainerControlInfo::finalState(ContainerControl cc)
{
    switch (cc) {
    case ContainerControl::Start:     return ContainerState::Running;
    case ContainerControl::Stop:      return ContainerState::Stopped;
    case ContainerControl::Freeze:    return ContainerState::Freezing;
    case ContainerControl::Thaw:      return ContainerState::Thawing;
    case ContainerControl::Reboot:    return ContainerState::Rebooting;
    case ContainerControl::ExecuteIn: return ContainerState::Executing;
    case ContainerControl::Create:    return ContainerState::Creating;
    case ContainerControl::Remove:    return ContainerState::Removing;
    case ContainerControl::_Count:    break;
    }

    assert(0 && "Unknown ContainerControl value");
    return ContainerState::Unknown;
}

bool ContainerControlInfo::separatorAfter(ContainerControl cc)
{
    switch (cc) {
    case ContainerControl::Stop:
    case ContainerControl::Thaw:   return true;
    default:                       return false;
    }
}

bool ContainerControlInfo::needsSelection(ContainerControl cc)
{
    switch (cc) {
    case ContainerControl::Create: return false;
    default:                       return true;
    }
}
