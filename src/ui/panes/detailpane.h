/*
    Copyright 2018-2022 Loopdawg Software

    ContainerManager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DETAILSPANE_H
#define DETAILSPANE_H

#include <QHeaderView>
#include <QVector>
#include <QMenu>
#include <QTimer>
#include <map>

#include "src/ui/widgets/usecdelegate.h"
#include "src/ui/widgets/sizedelegate.h"
#include "src/ui/widgets/cpusharesdelegate.h"
#include "src/ui/filters/subtreefilter.h"
#include "src/ui/filters/containerdetailsfilter.h"
#include "src/ui/filters/flattenfilter.h"
#include "pane.h"

namespace Ui {
class DetailsPane;
} // namespace Ui

class MainWindow;
class QModelIndex;
class QItemSelectionModel;

class DetailPane final : public Pane
{
    Q_OBJECT

public:
    explicit DetailPane(MainWindow& mainWindow, QWidget* parent = nullptr);
    ~DetailPane() override;

    bool hasSelection() const override;
    bool isSelected(const QModelIndex& idx) const override;
    QModelIndexList getSelections() const override;
    bool viewIsTree() const override;

protected:
    void showAll() override;
    void expandAll() override;
    void collapseAll() override;
    void selectAll() override;
    void selectNone() override;
    void resizeToFit(int defer = -1) override;
    void copySelected() const override;
    void viewAsTree(bool) override;
    void newConfig() override;

private slots:
    void on_DetailsPane_toggled(bool checked) { paneToggled(checked); }
    void showActionContextMenu(const QPoint&);
    void on_lockToCurrentButton_toggled(bool checked);
    void reexpandTree();
    void filterTextChanged(const QString&);
    void currentContainerChanged(const QModelIndex &current);

private:
    // *** begin Settings API
    void save(QSettings& settings) const override;
    void load(QSettings& settings) override;
    // *** end Settings API

    void setupDetailsView();
    void setupRowMap();
    void setupSignals();
    void setupDelegates();
    void setupContextMenus();               // build context menus
    void setupTimers();
    void resizeDeferredHook();
    QItemSelectionModel* selectionModel() const override;

    Ui::DetailsPane*       ui;

    USecDelegate           uSecDelegate;      // edit uSec times
    SizeDelegate           sizeDelegate;      // edit size limits
    CpuSharesDelegate      cpuSharesDelegate; // edit CPU shares

    QHeaderView            detailsHeader;
    SubTreeFilter          subtreeFilter;
    FlattenFilter          flattenFilter;
    ContainerDetailsFilter detailsFilter;

    QTimer                 resizeTimer;
    QTimer                 initialExpandTimer;

    std::map<QString, bool> expandState;  // see C++ comment for reexpandTree
};

#endif // DETAILSPANE_H
